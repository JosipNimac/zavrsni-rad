const LocalStrategy = require('passport-local').Strategy;
const bcrypt = require('bcrypt');
const logger = require('./logger');

const userRepository = require('../database/repositories/user-repo/user-repo-interface');

// Names of the keys that will be sent as json when user tries to log in!
const loginRequestBodyNames = {
    usernameField: 'identificator',
    passwordField: 'password'
};

module.exports = function(passport) {
    
    // Middleware function used to check that user is logged in
    // before accessing a protected endpoint.
    passport.loginCheck = function(req, res, next) {
        if (req.isAuthenticated()) {
            // User is logged in, so we let them continue.
            return next();
        }

        logger.warn('Unauthorized request for -> PATH: ' + req.path + ', METHOD: ' + req.method);
        res.status(401).json({message: 'Unauthorized'});
    }

    passport.serializeUser((user, done) => {
        if (!user || user.id === undefined) {
            done('Empty user');
        }
        
        done(null, user.id);
    });

    passport.deserializeUser((id, done) => {
        userRepository.getUserByID(id, (err, user) => {
            if (err) {
                done(err);
            } else if (!user || user.id === undefined) {
                done('No user with given id.');
            }

            done(null, user);
        });
    });

    passport.use('local', new LocalStrategy(loginRequestBodyNames, (identifier, password, done) => {
        userRepository.getUserByUsername(identifier, (err, user) => {

            if (err || !user || !user.activated) {
                // Either there was a problem with database or user does not exist!
                if (user && !user.activated) {
                    logger.warn(`User with username [${identifier}] has not been activated but tried to log in.`);
                    return done("Your account has not been activated. Check your e-mail from confimation link.");
                } else {
                    return done(null, false);
                }
            }

            bcrypt.compare(password, user.password_hash, (error, same) => {
                if (error) {
                    // Error while checking password!
                    // Maybe log this.
                    return done(err);
                }

                if (!same) {
                    return done(null, false);
                }

                return done(null, user);
            });
        });
    }));
};
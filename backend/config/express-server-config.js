const expressSession = require('express-session');
const bodyParser = require('body-parser');
const logger = require('./logger');
const expressSessionConfig = require('./express-session-config');

// Configuring passport!
const passport = require('passport');
require('./passport-config')(passport);

module.exports = function (app) {

    app.use(bodyParser.json({limit: "2mb"}));
    app.use(bodyParser.urlencoded({extended: false}));

    app.use(expressSession(expressSessionConfig));
    app.use(passport.initialize());
    app.use(passport.session());

    // Allowing requests from our frontend domain.
    app.use((req, res, next) => {
        res.header('Access-Control-Allow-Origin', process.env.FRONTEND || 'http://192.168.1.9:3000');
        res.header('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept');
        res.header('Access-Control-Expose-Headers', 'set-cookie');
        res.header('Access-Control-Allow-Credentials', 'true');
        next();
    });

    // Setting up logging of each request!
    app.use((req, res, next) => {
        logger.info('Got request for -> PATH: ' + req.path + ', METHOD: ' + req.method);
        next();
    });

    // Setting up router!
    app.use('/api', require('../routes/router'));

    // Setting up logging of requests that have no route handlers.
    app.use('*', function(req, res) {
        logger.warn('No request handler for request -> PATH: ' + req.baseUrl + ', METHOD: ' + req.method);
        res.status(404).end();
    });
}
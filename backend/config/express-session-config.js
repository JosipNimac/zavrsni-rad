const db = require('../database/index');


module.exports = {
    secret: process.env.SESSION_SECRET,
    cookie: {
        maxAge: 3 * 1000 * 60 * 60, // 3 hour!
        httpOnly: false
    },
    store: db.getSessionStore(),
    resave: true,
    saveUninitialized: false
}

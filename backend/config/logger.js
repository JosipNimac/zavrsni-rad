const winston = require('winston');
const fs = require('fs');
const path = require('path');

// Creating directory for logs. Runs only on startup and creates directories only once.
const logDirectory = './logs';
if (!fs.existsSync(logDirectory)) {
    fs.mkdirSync(logDirectory);
}

const customLevels = {
    levels: {
        error: 0, // Any unexpected applicaion behaviour.
        warn: 1, // Not strictly errors, but still unusual things.
        info: 2, // Info statements, used to follow the state of the application.
        debug: 3 // Used for debugging statements.
    },
    colors: { // Represents color that each logging message will be colored in the console depending on the level.
        error: 'red',
        warn: 'yellow',
        info: 'green',
        debug: 'cyan'
    }
};

const customConsoleFormatter = winston.format.combine(
    winston.format.colorize(),
    winston.format.timestamp(),
    winston.format.align(),
    winston.format.printf((info) => {
        const {timestamp, level, message, ...args} = info;

        const ts = new Date(timestamp).toLocaleString(); // Format of the date printed will be dependant on Locale of the environment.
        return `${ts} [${level}]: ${message} ${Object.keys(args).length ? JSON.stringify(args, null, 2) : ''}`;
    }),
);

const customFileFormatter = winston.format.combine(
    winston.format.timestamp(),
    winston.format.printf((info) => {
        const {timestamp, level, message, ...args} = info;
        
        const ts = new Date(timestamp).toLocaleString(); // Format of the date printed will be dependant on Locale of the environment.
        return `${ts} [${level.toUpperCase()}]: ${message} ${Object.keys(args).length ? JSON.stringify(args, null, 2) : ''}`;
}));

const logger = winston.createLogger({
    levels: customLevels.levels,
    format: winston.format.json(),
    transports: [
        new winston.transports.Console({ // Everything is printed to console 
            level: 'debug',
            format: customConsoleFormatter
        }),

        new winston.transports.File({ // Errors and alerts are automaticaly saved to a file 'backend/logs/errors.log'. 
            filename: './logs/errors.log',
            level: 'error',
            format: customFileFormatter
        }),

        new winston.transports.File({ // All logging statements except for debug are additionaly saved to a file 'backend/logs/combined.log'.     
            filename: './logs/info.log',
            level: 'info',
            format: customFileFormatter
        })
    ]
});
winston.addColors(customLevels.colors);

module.exports = logger;

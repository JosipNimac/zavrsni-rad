const GenericTreeNode = require('./GeneticProgramming/GeneticAlgorithm/Nodes/GenericTreeNode');
const Left = require('./GeneticProgramming/GeneticAlgorithm/Nodes/TerminalNodes/Left');
const Right = require('./GeneticProgramming/GeneticAlgorithm/Nodes/TerminalNodes/Right');
const Move = require('./GeneticProgramming/GeneticAlgorithm/Nodes/TerminalNodes/Move');
const Prog2 = require('./GeneticProgramming/GeneticAlgorithm/Nodes/FunctionalNodes/Prog2');
const Prog3 = require('./GeneticProgramming/GeneticAlgorithm/Nodes/FunctionalNodes/Prog3');
const IfFoodAhead = require('./GeneticProgramming/GeneticAlgorithm/Nodes/FunctionalNodes/IfFoodAhead');
const SimpleMutation = require('./GeneticProgramming/GeneticAlgorithm/Operators/Mutations/SimpleMutation');
const OnePointCrossover = require('./GeneticProgramming/GeneticAlgorithm/Operators/Crossovers/OnePointCrossover');
const nTournamentSelection = require('./GeneticProgramming/GeneticAlgorithm/Operators/Selections/nTournamentSelection');
const NodeUtils = require('./GeneticProgramming/GeneticAlgorithm/Nodes/NodeUtils');
const Ant = require('./GeneticProgramming/GeneticAlgorithm/Ant/Ant');
const populationInit = require('./GeneticProgramming/GeneticAlgorithm/Population/PopulationInitializer');
const Population = require('./GeneticProgramming/GeneticAlgorithm/Population/Population');
const ExecutionEngine = require('./GeneticProgramming/ExecutionEngine/ExecutionEngine');
const GeneticRunner = require('./GeneticProgramming/GeneticRunner');

const tree1 = NodeUtils.GenerateTreeWithRoot(3, 10, true);
const ant1 = new Ant(tree1, 0);
const tree2 = NodeUtils.GenerateTreeWithRoot(3, 10, true);
const ant2 = new Ant(tree2, 0);

const {performance} = require('perf_hooks');

/*console.log();
console.log();
console.log("---- Created ant 1 ---- Size -> " + ant1.getRoot().getNodeCount());
console.log(NodeUtils.GetTreeAsString(tree1));
console.log("---- Created ant 2 ---- Size -> " + ant2.getRoot().getNodeCount());
console.log(NodeUtils.GetTreeAsString(tree2));
console.log();

const simpleMutation = new SimpleMutation(3, 10);

simpleMutation.mutate(ant1);
simpleMutation.mutate(ant2);

console.log("---- First ant after mutation ---- Size -> " + ant1.getRoot().getNodeCount());
console.log(ant1.toString());
console.log("---- Second ant after mutation ---- Size -> " + ant2.getRoot().getNodeCount());
console.log(ant2.toString());
console.log();
console.log();

const crossover = new OnePointCrossover(30, 6);

const ant1Clone = ant1.clone();
const ant2Clone = ant2.clone();

crossover.cross(ant1Clone, ant2Clone);

console.log("After crossover: ");
console.log(" -- Ant1 -- Size -> " + ant1Clone.getRoot().getNodeCount());
console.log(ant1Clone.toString());
console.log(" -- Ant2 -- Size -> " + ant2Clone.getRoot().getNodeCount());
console.log(ant2Clone.toString());
console.log();
console.log(" --- Check of original ants --- ");
console.log(" -- Original 1 -- Size -> " + ant1.getRoot().getNodeCount());
console.log(ant1.toString());
console.log(" -- Original 2 -- Size -> " + ant2.getRoot().getNodeCount());
console.log(ant2.toString());

console.log();
console.log();*/

/*const population = populationInit({
    popSize: 100,
    method: "Half_and_half",
    maxNodes: 40,
    desiredDepth: 5
});

for (let i = 0; i < population.length; i++) {
    console.log(`Ant ${i + 1} with size: ${population[i].getRoot().getNodeCount()}`);
    console.log(population[i].toString());
    console.log();
}*/



params = {
    mapParams: {
        antRow: 0,
        antColumn: 0,
        antRotation: "Right",
        fromFile: true,
        filePath: "Maps/Default/SantaFeTrail.txt"
    },
    algorithmParams: {
        popInitParams: {
            popSize: 400,
            desiredDepth: 6,
            maxNodes: 200,
            method: "Grow"
        },
        operators: {
            selection: {
                sizeOfTournament: 7
            },
            mutation: {
                maxNodes: 200,
                maxDepth: 8
            },
            crossover: {
                maxNodes: 250,
                maxEffort: 4
            },
            evaluator: {
                plagirismFactor: 0.98
            }
        },
        generations: 100,
        reproductionRate: 0.01,
        mutationRate: 0.24,
        crossoverRate: 0.75,
        elitist: true,
        maxGensNoProgress: 60,
        mutationInjection: true
    }
}

const initStart = performance.now();
let runner = new GeneticRunner(params);
const initEnd = performance.now();

const trainingStart = performance.now();
runner.startTraining();
const trainingEnd = performance.now();

console.log(`Init [${initEnd - initStart}] Training [${trainingEnd - trainingStart}] Total [${trainingEnd - initStart}]`);

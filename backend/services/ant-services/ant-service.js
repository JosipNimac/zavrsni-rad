const logger = require('../../config/logger');
const antRepository = require('../../database/repositories/ant-repo/ant-repo-interface');
const UtilService = require('../_core/util-service');
const db = require('../../database/index');

function saveNewAnt(antData, algParams, callback) {
    let code = UtilService.generateCode(5, 4, '-');
    const {name, data, userID, trailID} = antData;

    db.getConnection(connection => {
        connection.client.query("BEGIN", err => {
            if (err) {
                logger.error("Could not start transaction to save new ant: ", err);
                connection.done();
                return callback(err);
            }

            antRepository.saveNewAnt(name, code, data, userID, (err, antID) => {
                if (err) {
                    logger.error("Unable to save new ant.");
                    db.rollback(connection, "Could not do rollback for saving a new ant: ");
                    return callback(err);
                }

                antRepository.saveAlgParams(algParams, antID, trailID, (err, paramsID) => {
                    if (err) {
                        logger.error("Unable to save algorithm params");
                        db.rollback(connection, "Could not do rollback for saving algorihtm params: ");
                        return callback(err);
                    }

                    db.commit(connection, "Could not commit transaction for saving ant and algorithm params: ");
                    
                    callback(null, antID);
                }, connection);
            }, connection);
        });
    });
    //antRepository.saveNewAnt(name, code, data, userID, callback);
}

function getAntById(id, callback) {
    antRepository.getAntById(id, (err, ant) => {
        if (err) {
            return callback(err.noAnt ? {notFound: true} : err);
        }

        return callback(null, ant);
    });
}

function getAntByCode(code, callback) {
    antRepository.getAntByCode(code, (err, ant) => {
        if (err) {
            return callback(err.noAnt ? {notFound: true} : err);
        }

        return callback(null, ant);
    });
}

function getRecentAnts(userID, callback) {
    antRepository.getRecentAnts(userID, callback)
}

function getParamsForAnt(antID, callback) {
    antRepository.getParamsForAnt(antID, (err, params) => {
        if (err) {
            if (err.noParams) {
                return callback({notFound: true});
            }

            return callback(err);
        }

        return callback(null, params);
    });
}

function getAntsBySubQuery(subQuery, limit, code, callback) {
    antRepository.getAntsBySubQuery(subQuery, limit, code, callback);
}

function getRandomAdminAnt(trailCode, callback) {
    antRepository.getAdminAntsOnTrail(trailCode, (err, ants) => {
        if (err) {
            return callback(err);
        }

        return callback(null, ants[Math.floor(Math.random() * ants.length)]);
    });
}

function getUserAnts(userID, page, pageSize, callback) {
    antRepository.getUserAnts(userID, (err, ants) => {
        if (err) {
            return callback(err);
        }

        if (page < 1 || page > Math.ceil(ants.length / pageSize)) {
            return callback({illegalPage: true});
        }

        return callback(null, {
            ants: ants.slice((page - 1) * pageSize, page * pageSize),
            page: parseInt(page),
            pageSize: parseInt(pageSize),
            totalPages: Math.ceil(ants.length / pageSize)
        });
    });
}

module.exports = {
    saveNewAnt,
    getAntById,
    getAntByCode,
    getRecentAnts,
    getParamsForAnt,
    getAntsBySubQuery,
    getRandomAdminAnt,
    getUserAnts
}
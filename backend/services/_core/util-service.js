const logger = require('../../config/logger');
const randomString = require('randomstring');

function generateCode(batchSize, batchNumber, batchSeparator) {
    if (batchSize < 1 || batchNumber < 1 || batchSeparator === undefined) {
        throw new Error(`Illegal arguments given! Given was: batchSize [${batchSize}], batchNumber [${batchNumber}], batchSeparator [${batchSeparator}]`);
    }

    let string = "";
    for (let i = 0; i < batchNumber; i++) {
        string += randomString.generate(batchSize).toUpperCase();

        if (i !== batchNumber - 1) {
            string += '-';
        }
    }

    return string;
}

module.exports = {
    generateCode
}
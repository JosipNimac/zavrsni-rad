const fs = require('fs');
const fsPath = require('path');
const logger = require('../../config/logger');
const Constants = require('../../_genetic/GeneticProgramming/Constants');

const trailsRoot = "../../Trails/";

function saveThumbnailImage(base64Data, path, category, username, callback) {
    if (!fs.existsSync(fsPath.resolve(__dirname + "/" + trailsRoot + category + "/Thumbnails/" + username + "/"))) {
        fs.mkdirSync(fsPath.resolve(__dirname + "/" + trailsRoot + category + "/Thumbnails/" + username));
    }

    logger.debug("In save thumbnail image!");
    logger.debug("Saving thumbnail in: " + fsPath.resolve(__dirname + "/" + trailsRoot + category + "/" + path).toString());
    fs.writeFile(fsPath.resolve(__dirname + "/" + trailsRoot + category + "/" + path), base64Data, 'base64', (err) => {
        logger.debug("In callback");
        logger.debug("Error object recieved: " + (err != null));
        callback(err);
    });
}

function getThumbnailImage(path, category, callback) {
    fs.exists(fsPath.resolve(__dirname + "/" + trailsRoot + category + "/" + path), exist => {
        if (!exist) {
            logger.warn(`Thumbnail at path [${path}] does not exist!`);
            return callback({notFound: true});
        }

        fs.readFile(fsPath.resolve(__dirname + "/" + trailsRoot + category + "/" + path), {encoding: "base64"}, (err, data) => {
            if (err) {
                logger.error(`Problem while reading file [${path}]: `, err);
                return callback(err);
            }

            return callback(null, "data:image/png;base64, " + data);
        });
    });
}

function saveDefinitionFromParams(mapParams, path, category, username, callback) {
    // First row of file is of form -> rowxcolumn, eg. -> '32x32'.
    let fileString = mapParams.mapRows + "x" + mapParams.mapColumns + "\r\n";

    // Map data.
    for (let row = 0; row < mapParams.foodLocations.length; row++) {
        let rowData = "";
        for (let column = 0; column < mapParams.foodLocations[row].length; column++) {
            rowData += mapParams.foodLocations[row][column];
        }

        fileString += rowData + "\r\n";
    }

    // Starting ant data.
    fileString += mapParams.antRow + ";" + mapParams.antColumn + ";" + mapParams.antRotation;

    if (!fs.existsSync(fsPath.resolve(__dirname + "/" + trailsRoot + category + "/Definitions/" + username + "/"))) {
        fs.mkdirSync(fsPath.resolve(__dirname + "/" + trailsRoot + category + "/Definitions/" + username));
    }

    fs.writeFile(fsPath.resolve(__dirname + "/" + trailsRoot + category + "/" + path), fileString, "utf-8", callback);
}

function loadParamsFromFile(path, category, callback) {
    if (!fs.existsSync(fsPath.resolve(__dirname + "/" + trailsRoot + category + "/" + path))) {
        logger.warn(`File at path [${path}] with category [${category}] was not found.`);
        return callback({notFound: true});
    }

    fs.readFile(fsPath.resolve(__dirname + "/" + trailsRoot + category + "/" + path), (err, data) => {
        if (err) {
            logger.error(`Unable to read file [${path}]: `, err);
            return callback(err);
        }

        const splitData = data.toString().split(/\r\n|\r|\n/);
        const firstLine = splitData[0].split("x");

        let mapParams = {
            mapRows: parseInt(firstLine[0]),
            mapColumns: parseInt(firstLine[1])
        };

        const foodLocations = [];
        for (let i = 1; i < splitData.length - 1; i++) {
            const row = [];
            for (let j = 0; j < splitData[i].length; j++) {
                row.push(splitData[i].charAt(j));
            }
            foodLocations.push(row);
        }
        mapParams.foodLocations = foodLocations;
        mapParams.foodCount = countFoodOnMap(foodLocations);
        
        const antParams = splitData[splitData.length - 1].split(";");
        mapParams.antRow = parseInt(antParams[0]);
        mapParams.antColumn = parseInt(antParams[1]);
        mapParams.antRotation = antParams[2];

        callback(null, mapParams);
    });
}

function getRandomDefaultTrail(callback) {
    const dir = fsPath.resolve(__dirname + "/" + trailsRoot + "Default/Definitions/"); 
    fs.readdir(dir, (err, users) => {
        if (err) {
            logger.error("Unable to read list of default trail users: ", err);
            return callback(err);
        }

        const user = users[Math.floor(Math.random() * users.length)];
        fs.readdir(dir + "/" + user, (err, files) => {
            if (err) {
                logger.error(`Unable to read list of trails for user ${user}: `, err);
                return callback(err);
            }

            const file = files[Math.floor(Math.random() * files.length)];
            loadParamsFromFile(`Definitions/${user}/${file}`, "Default", (err, data) => {
                if (err) {
                    return callback(err);
                }
                
                data.code = file.substring(file.indexOf("-", 4) + 1, file.indexOf("-", 4) + 24);
                callback(null, data);
            });
        });
    });
}

module.exports = {
    saveThumbnailImage,
    getThumbnailImage,
    saveDefinitionFromParams,
    loadParamsFromFile,
    getRandomDefaultTrail
}

/**
 * 
 * @param {Array[]} foodLocations 
 */
function countFoodOnMap(foodLocations) {
    let amountOfFood = 0;

    for (let i = 0; i < foodLocations.length; i++) {
        for (let j = 0; j < foodLocations[i].length; j++) {
            if (foodLocations[i][j] === Constants.MAP_DATA.FOOD) {
                amountOfFood++;
            }
        }
    }

    return amountOfFood;
}
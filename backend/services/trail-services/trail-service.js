const randomstring = require('randomstring');
const logger = require('../../config/logger');
const trailRepository = require('../../database/repositories/trail-repo/trail-repo-interface');
const fs = require('fs');

const IOService = require('../_core/IOService');

const TrailsRoot = "./Trails/";

function saveNewTrail(mapParams, thumbnailBase64, user, callback) {
    if (!validateMapParams(mapParams)) {
        logger.debug("Validation of params falied!");
        return callback({invalidParams: true});
    }


    let code = generateTrailCode();
    let definitionPath = "Definitions/" + user.username + "/Map-" + mapParams.mapName + "-" + code + ".txt";
    let thumbnailPath = "Thumbnails/" + user.username + "/Map-" + mapParams.mapName + "-" + code + ".png";
    trailRepository.saveNewTrail(mapParams.mapName, code, definitionPath, thumbnailPath, user.id, (err, res) => {
        if (err) {
            logger.error("Unable to save trail to database. Definition file and thumbnail were not saved.");
            return callback(err);
        }

        IOService.saveDefinitionFromParams(mapParams, definitionPath, "User", user.username, () => {
            IOService.saveThumbnailImage(thumbnailBase64.replace(/^data:image\/png;base64,/, ""), thumbnailPath, "User", user.username, callback);
        });
    });
}

function getBasicTrailInfoById(id, callback) {
    trailRepository.getTrailById(id, (err, trail) => {
        if (err) {
            return callback(err.noTrail ? {notFound: true} : err);
        }

        return callback(null, {trailID: trail.id, trailName:trail.name, trailCode:trail.code, path: trail.path_to_thumbnail, user: trail.username, created: trail.timestamp_created});
    });
}

function getBasicTrailInfoByCode(code, callback) {
    trailRepository.getTrailByCode(code, (err, trail) => {
        if (err) {
            return callback(err.noTrail ? {notFound: true} : err);
        }

        return callback(null, {trailID: trail.id, trailName:trail.name, trailCode:trail.code, path: trail.path_to_thumbnail, user: trail.username, created: trail.timestamp_created});
    });
}

function getTrailThumbnail(path, callback) {
    IOService.getThumbnailImage(path, "User", callback);
}

function getAllOffsetTrails(offsetDate, limit, callback) {
    trailRepository.getAllOffsetTrails(offsetDate, limit, (err, trails) => {
        if (err) {
            logger.error("Unable to get trails info.");
            return callback(err);
        }

        trailRepository.getTrailsCount((err, totalTrails) => {
            if (err) {
                logger.error("Unable to get trails count.");
                return callback(err);
            }

            // FIXME: I could/should do some filtering here so that not all trail data is sent to frontend, but just it's subset...
            return callback(null, {totalTrails, trails});
        });
    });
}

function getTrailDetailByCode(code, callback) {
    trailRepository.getTrailByCode(code, (err, trail) => {
        if (err && err.noTrail) {
            logger.error(`Unable to find trail with code: ${code}.`);
            return callback({notFound: true});
        } else if (err) {
            logger.error("Error while getting trail with code: " + code);
            return callback(err);
        }

        IOService.loadParamsFromFile(trail.path_to_definition, "User", callback);
    });
}

function getUserTrails(userID, page, pageSize, callback) {
    trailRepository.getUserTrails(userID, (err, trails) => {
        if (err) {
            return callback(err);
        }

        if (page < 1 || page > Math.ceil(trails.length / pageSize)) {
            return callback({illegalPage: true});
        }

        return callback(null, {
            trails: trails.slice((page - 1) * pageSize, page * pageSize),
            page: parseInt(page),
            pageSize: parseInt(pageSize),
            totalPages: Math.ceil(trails.length / pageSize)
        });
    });
}

function getRandomDefaultTrail(callback) {
    IOService.getRandomDefaultTrail(callback);
}

function searchTrails(searchQuery, callback) {
    trailRepository.searchTrailsByNameOrCode(searchQuery, callback);
}

module.exports = {
    saveNewTrail,
    getBasicTrailInfoById,
    getBasicTrailInfoByCode,
    getTrailThumbnail,
    getAllOffsetTrails,
    getTrailDetailByCode,
    getRandomDefaultTrail,
    getUserTrails,
    searchTrails
}


function validateMapParams(mapParams) {
    if (mapParams == null || mapParams.mapRows === undefined || mapParams.mapColumns === undefined || mapParams.mapName === undefined) {
        return false;
    }

    if (mapParams.antRow === undefined || mapParams.antColumn === undefined || mapParams.antRotation === undefined) {
        return false;
    }

    if (mapParams.foodLocations.length !== 32 || mapParams.foodLocations[0].length !== 32) {
        return false;
    }
    
    return true;
}

function generateTrailCode() {
    let code = "";
    for (let i = 0; i < 4; i++) {
        code += randomstring.generate(5).toUpperCase();
        if (i !== 3) {
            code += "-";
        }
    }

    return code;
}

function saveParamsToFile(mapParams, filepath, username) {
    // Map size info.
    let str = mapParams.mapRows + "x" + mapParams.mapColumns + "\r\n";
    
    // Map data.
    for (let i = 0; i < mapParams.foodLocations.length; ++i) {
        let row = "";
        for (let j = 0; j < mapParams.foodLocations[i].length; ++j) {
            row += mapParams.foodLocations[i][j];
        }
        str += row + "\r\n";
    }

    // Staring ant data.
    str += mapParams.antRow + ";" + mapParams.antColumn + ";" + mapParams.antRotation;

    if (!fs.existsSync(TrailsRoot + "User/Definitions/" + username + "/")) {
        fs.mkdirSync(TrailsRoot + "/User/Definitions/" + username);
    }

    fs.writeFileSync(TrailsRoot + "User/" + filepath, str, "utf-8");
}

function saveThumbnail(base64Data, filepath, username) {
    if (!fs.existsSync(TrailsRoot + "User/Thumbnails/" + username)) {
        fs.mkdirSync(TrailsRoot + "User/Thumbnails/" + username);
    }

    fs.writeFileSync(TrailsRoot + "User/" + filepath, base64Data, 'base64');
}
const tokenRepository = require('../../database/repositories/token-repo/token-repo-interface');
const nodemailer = require('nodemailer');
const logger = require('../../config/logger');

let transporter = nodemailer.createTransport({
    service: "gmail",
    auth: {
        user: process.env.EMAIL_USERNAME,
        pass: process.env.EMAIL_PASSWORD
    },
    tls: {
        rejectUnauthorized: false
    }
});

async function sendTokenViaMail(token, email) {
    let mailOptions = {
        from: process.env.EMAIL_USERNAME,
        to: email,
        subject: "Registration on GeneticProgrammingDemo",
        html: `<p>Hello,<br/>thank you for using GeneticProgrammingDemo.<br/>
            Please click this link to confirm your mail: <a href="http://localhost:3333/api/auth/confirm/${token}">${token}</a></p>
            <p>If you think you got this mail by mistake, reply to this mail adress and we will check it.</p>`
    };
    // TODO: Remove hardcoded link. Should lead to react page that will then send to server but for now it is just for testing.

    transporter.sendMail(mailOptions, (err, info) => {
        if (err) {
            return logger.error("Email token service was unable to send e-mail: ", err);
        }

        logger.info(`Token successfully sent to e-mail [${email}].`);
    });
}

module.exports = {
    sendTokenViaMail
};
const logger = require('../../config/logger');
const userRepository = require('../../database/repositories/user-repo/user-repo-interface');

function checkIfUsernameExists(username, callback) {
    userRepository.getUserByUsername(username, (err, user) => {
        if ((err !== null && err.custom !== undefined) || !user) {
            return callback(false);
        }

        return callback(true);
    });
}

function checkIfEmailExists(email, callback) {
    userRepository.getUserByEmail(email, (err, user) => {
        if (err !== null && err.custom !== undefined || !user) {
            return callback(false);
        }

        return callback(true);
    });
}

module.exports = {
    checkIfUsernameExists,
    checkIfEmailExists
}
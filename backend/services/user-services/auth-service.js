const randomstring = require('randomstring');
const bcrypt = require('bcrypt');

const userRepository = require('../../database/repositories/user-repo/user-repo-interface');
const tokenRepository = require('../../database/repositories/token-repo/token-repo-interface');
const db = require('../../database/index');

const logger = require('../../config/logger');

// TODO: Have a username and email check before trying to save to database and give user a better message back!

function registerUser(userData, callback) {
    // Generated random string for token. Will be used for e-mail confirmation of the user registration.
    const token = randomstring.generate();
    userData.password_hash = bcrypt.hashSync(userData.password, 10);

    db.getConnection(connection => {
        // Starting transaction.
        connection.client.query("BEGIN", err => {
            if (err) {
                logger.error("Could not start transaction to crate new user: ", err);
                connection.done();
                return callback(err);
            }

            userRepository.createNewUser(userData, (err, userID) => {
                if (err) {
                    logger.error("Unable to create new user.");
                    rollback(connection, "Could not do rollback for user registration: ");

                    return callback(err);
                }

                tokenRepository.saveNewToken(userID, token, (err, tokenID) => {
                    if (err) {
                        logger.error(`Unable to store token for new user with id: ${userID}`);
                        rollback(connection, "Could not do rollback for user registration: ");

                        return callback(err);
                    }

                    commit(connection, "Could not do commit for user registration: ");

                    callback(null, token);
                }, connection);
            }, connection);
        });
    });
};

function confirmRegistration(token, callback) {
    db.getConnection(connection => {
        connection.client.query("BEGIN", err => {
            if (err) {
                logger.error("Could not start transaction for confirming user registration.");
                return callback(err);
            }

            tokenRepository.activateTokenByTokenText(token, (err, userID) => {
                if (err) {
                    logger.error(`Unable to activate token with value [${token}].`);
                    rollback(connection, "Could not do rollback for user activation.");

                    return callback(err);
                }

                userRepository.activateUser(userID, (err, userID) => {
                    if (err) {
                        logger.error("Unable to activate user with id: " + userID);
                        rollback(connection, "Could not do rollback for user activation.");

                        return callback(err);
                    }

                    commit(connection, "Could not commit for user acivation.");
                    callback(null, userID);
                }, connection);
            }, connection);
        });
    });
}

async function updateUserLogin(userID) {
    userRepository.updateUserLastLogin(userID, () => {});
}



/**
 * Helper function that will try a 'ROLLBACK' query on a given connection and log and close the connection if that throws an error.
 */
function rollback(connection, errMsg) {
    connection.client.query("ROLLBACK", err => {
        if (err) {
            logger.error(errMsg, err);
        }

        connection.done();
    });
}

/**
 * Helper function that will try a 'COMMIT' query on a given connection and log and close the connection if that throws an error.
 */
function commit(connection, errMsg) {
    connection.client.query('COMMIT', err => {
        if (err) {
            logger.error(errMsg, err);
        }

        connection.done();
    });
}

module.exports = {
    registerUser,
    confirmRegistration,
    updateUserLogin
};
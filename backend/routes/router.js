const passport = require('passport');
const router = require('express').Router();

const authController = require('../controllers/auth-controller');
const trailController = require('../controllers/trails-controller');
const userController = require('../controllers/user-controller');
const antController = require('../controllers/ant-controller');

router.post("/auth/register", authController.register);
router.post("/auth/login", passport.authenticate('local'), authController.login);
router.get("/auth/logout", passport.loginCheck, authController.logout);
router.get("/auth/confirm/:token", authController.confirmRegistration);
router.get("/auth/myinfo", authController.getSelfInfo);
router.get("/auth/editInfo", passport.loginCheck, authController.getSelfEditInfo);
router.get("/auth/loginCheck", authController.isLoggedIn);

router.get("/auth/check/username/:username", userController.usernameCheck);
router.get("/auth/check/email/:email", userController.emailCheck);

router.post("/trails/upload", passport.loginCheck, trailController.saveNewTrail);
router.get("/trails/info/id/:trailID", trailController.getTrailInfoById);
router.get("/trails/info/code/:trailCode", trailController.getTrailInfoByCode);
router.get("/trails/details/:code", trailController.getTrailDetails);
router.get("/trails/thumbnail", trailController.getTrailThumbnail);
router.get("/trails/random", trailController.getRandomTrail);
router.get("/trails/myTrails", passport.loginCheck, trailController.getUserTrails);
router.get("/trails/search", trailController.searchTrails);

// FIXME: This method should not be POST!
router.post("/trails/info/all", trailController.getAllOffsetTrails);

router.get("/ant/id/:id", antController.getAntById);
router.get("/ant/code/:code", antController.getAntByCode);
router.post("/ant/save", passport.loginCheck, antController.saveNewAnt);
router.get("/ant/recent", passport.loginCheck, antController.getRecentAnts);
router.get("/ant/params/:antID", antController.getParamsForAnt);
router.get("/ant/search", antController.getAntsBySubQuery);
router.get("/ant/admin/:trailCode", antController.getRandomAdminAnt);
router.get("/ant/myAnts", passport.loginCheck, antController.getUserAnts);

module.exports = router;
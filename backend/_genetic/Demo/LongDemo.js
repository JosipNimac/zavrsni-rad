const {performance} = require('perf_hooks');

const GeneticRunner = require('../GeneticProgramming/GeneticRunner');

// All params that are defineable.
params = {
    mapParams: {
        antRow: 0,
        antColumn: 0,
        antRotation: "Right",
        fromFile: true,
        filePath: "Maps/Default/SantaFeTrail.txt"
    },
    algorithmParams: {
        popInitParams: {
            popSize: 800,
            desiredDepth: 6,
            maxNodes: 200,
            method: "Half_and_half"
        },
        operators: {
            selection: {
                sizeOfTournament: 7
            },
            mutation: {
                maxDepth: 8,
                maxNodes: 200
            },
            crossover: {
                maxNodes: 200,
                maxEffort: 2
            },
            evaluator: {
                plagirismFactor: 0.9
            }
        },
        generations: 250,
        reproductionRate: 0.01,
        mutationRate: 0.14,
        crossoverRate: 0.85,
        elitist: true,
        maxGensNoProgress: 80,
        mutationInjection: false
    }
}

let initStart = performance.now();
let runner = new GeneticRunner(params);
let initEnd = performance.now();

let trainingStart = performance.now();
runner.startTraining();
let trainingEnd = performance.now();

console.log();
console.log("-------Additional-------");
console.log(`Initial setup took ${Math.floor(initEnd - initStart)} milliseconds.`);
console.log(`Training took ${Number((trainingEnd - trainingStart) / 1000).toFixed(2)} seconds.`);
console.log();


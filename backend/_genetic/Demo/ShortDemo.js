const {performance} = require('perf_hooks');

const GeneticRunner = require('../GeneticProgramming/GeneticRunner');

// All params that are defineable.
params = {
    mapParams: {
        antRow: 0,
        antColumn: 0,
        antRotation: "Right",
        fromFile: true,
        filePath: "Maps/Default/SantaFeTrail.txt"
    },
    algorithmParams: {
        popInitParams: {
            popSize: 180,
            desiredDepth: 5,
            maxNodes: 200,
            method: "Half_and_half"
        },
        operators: {
            selection: {
                sizeOfTournament: 7
            },
            mutation: {
                maxNodes: 200,
                maxDepth: 8
            },
            crossover: {
                maxNodes: 250,
                maxEffort: 4
            },
            evaluator: {
                plagirismFactor: 0.85
            }
        },
        generations: 100,
        reproductionRate: 0.01,
        mutationRate: 0.24,
        crossoverRate: 0.75,
        elitist: true,
        maxGensNoProgress: 60,
        mutationInjection: true
    }
}

let initStart = performance.now();
let runner = new GeneticRunner(params);
let initEnd = performance.now();

let trainingStart = performance.now();
runner.startTraining();
let trainingEnd = performance.now();

console.log();
console.log("-------Additional-------");
console.log(`Initial setup took ${Math.floor(initEnd - initStart)} milliseconds.`);
console.log(`Training took ${Number((trainingEnd - trainingStart) / 1000).toFixed(2)} seconds.`);
console.log();

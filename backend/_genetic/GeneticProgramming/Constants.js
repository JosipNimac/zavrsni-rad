module.exports = {
    MOVES_PER_EXECUTION: 600,
    MAP_DATA: {
        FOOD: '1',
        TRAIL: '0',
        OTHER: '.'
    },
    ANT_ROTATIONS: {
        UP: "Up",
        RIGHT: "Right",
        DOWN: "Down",
        LEFT: "Left"
    },
    TREE_GENERATION_METHODS: {
        GROW: "Grow",
        FULL: "Full",
        HALF_AND_HALF: "Half_and_half"
    }
}
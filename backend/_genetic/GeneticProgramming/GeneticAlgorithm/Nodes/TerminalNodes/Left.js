const GenericTreeNode = require('../GenericTreeNode');
const ExecutionState = require('../../../ExecutionEngine/ExecutionState');
const Constants = require('../../../Constants');

module.exports = class Left extends GenericTreeNode {
    constructor(depth, parent, nodeCount) {
        super(depth, parent, nodeCount, 0);
    }

    /**
     * 
     * @param {ExecutionState} exe 
     */
    execute(exe) {
        if (exe.getNumberOfAvailableActions() < 1) {
            return;
        }

        switch(exe.getAntRotation()) {
            case Constants.ANT_ROTATIONS.LEFT:
                exe.setAntRotation(Constants.ANT_ROTATIONS.DOWN);
                break;
            case Constants.ANT_ROTATIONS.RIGHT:
                exe.setAntRotation(Constants.ANT_ROTATIONS.UP);
                break;
            case Constants.ANT_ROTATIONS.UP:
                exe.setAntRotation(Constants.ANT_ROTATIONS.LEFT);
                break;
            case Constants.ANT_ROTATIONS.DOWN:
                exe.setAntRotation(Constants.ANT_ROTATIONS.RIGHT);
                break;
            default:
                // TODO: Log that unknown rotation happened here.
        }

        exe.actionPerformed();
    }

    clone(parent) {
        return new Left(super.getDepth(), parent, super.getNodeCount(), super.getSupposedChildren());
    }

    getName() {
        return "Left";
    }
}
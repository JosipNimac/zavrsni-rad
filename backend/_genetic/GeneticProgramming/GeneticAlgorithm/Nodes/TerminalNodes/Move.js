const GenericTreeNode = require('../GenericTreeNode');
const ExecutionState = require('../../../ExecutionEngine/ExecutionState');

module.exports = class Move extends GenericTreeNode {
    constructor(depth, parent, nodeCount) {
        super(depth, parent, nodeCount, 0);
    }

    /**
     * 
     * @param {ExecutionState} exe 
     */
    execute(exe) {
        if (exe.getNumberOfAvailableActions() < 1) {
            // No actions available.
            return;
        }

        exe.moveAntForward();
        exe.actionPerformed();
    }

    clone(parent) {
        return new Move(super.getDepth(), parent, 1, 0);
    }

    getName() {
        return "Move";
    }
}
const GenericTreeNode = require('../GenericTreeNode');
const ExecutionState = require('../../../ExecutionEngine/ExecutionState');
const Constants = require('../../../Constants');

module.exports = class IfFoodAhead extends GenericTreeNode {
    constructor(depth, parent, nodeCount) {
        super(depth, parent, nodeCount, 2);
        this.children = [];
    }

    /**
     * 
     * @param {ExecutionState} exe 
     */
    execute(exe) {
        if (super.getSupposedChildren() !== this.children.length) {
            // TODO: Execute called before children were set up. Should do some error handling here.
            console.log("Error: execute called before children were set.");
            return;
        }

        if (exe.getNumberOfAvailableActions() < 1) {
            return;
        }

        if (exe.mapAheadOfAnt() === Constants.MAP_DATA.FOOD) {
            this.children[0].execute(exe);
        } else {
            this.children[1].execute(exe);
        }
    }

    clone(parent) {
        let clone = new IfFoodAhead(super.getDepth(), parent, super.getNodeCount(), super.getSupposedChildren());

        for (let i = 0; i < this.children.length; i++) {
            clone.addChild(this.children[i].clone(clone));
        }

        return clone;
    }

    getName() {
        return "IfFoodAhead";
    }
}
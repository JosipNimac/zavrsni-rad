const GenericTreeNode = require('./GenericTreeNode');
const Left = require('./TerminalNodes/Left');
const Right = require('./TerminalNodes/Right');
const Move = require('./TerminalNodes/Move');
const Prog2 = require('./FunctionalNodes/Prog2');
const Prog3 = require('./FunctionalNodes/Prog3');
const IfFoodAhead = require('./FunctionalNodes/IfFoodAhead');

const TerminalNodes = [new Move(0, null, 1), new Left(0, null, 1), new Right(0, null, 1)];
const FunctionalNodes = [new Prog2(0, null, 1), new Prog3(0, null, 1), new IfFoodAhead(0, null, 1)];
const AllNodes = [new Move(0, null, 1), new Left(0, null, 1), new Right(0, null, 1), new Prog2(0, null, 1), new Prog3(0, null, 1), new IfFoodAhead(0, null, 1)];

function GenerateTreeWithRoot(maxDepth, maxNodes, methodFull) {
    let root = RandomFunctionalNode();

    for (let i = 0; i < root.getSupposedChildren(); i++) {
        let child = GenerateTree(maxDepth - 1, maxNodes - 1, methodFull, root.getDepth() + 1);
        child.setParent(root);
        
        maxNodes -= child.getNodeCount();

        root.addChild(child);
        root.setNodeCount(root.getNodeCount() + child.getNodeCount());
    }

    return root;
}

/**
 * 
 * @param {*} maxDepth 
 * @param {*} maxNodes 
 * @param {*} methodFull 
 * @param {*} currentDepth
 * @returns {GenericTreeNode} root of the generated tree 
 */
function GenerateTree(maxDepth, maxNodes, methodFull, currentDepth) {
    let root;
    if (maxDepth === 1 || maxNodes <= 1) {
        // If we are out of depth or out of nodes, we only generate terminal nodes regardles of method.
        root = RandomTerminalNode();
    } else if (methodFull) {
        // If method is full and we are not out of nodes or depth we are getting another functional node.
        root = RandomFunctionalNode();
    } else {
        root = RandomNode();
    }
    root.setDepth(currentDepth);

    maxNodes--; // We just created a node.
    for (let i = 0, children = root.getSupposedChildren(); i < children; i++) {
        let child = GenerateTree(maxDepth - 1, maxNodes, methodFull, currentDepth + 1);
        child.setParent(root);

        maxNodes -= child.getNodeCount();

        root.addChild(child);
        root.setNodeCount(root.getNodeCount() + child.getNodeCount());
    }

    return root;
}

/**
 * 
 * @param {GenericTreeNode} node 
 */
function GetTreeAsString(node) {
    let result = node.getName();

    if (!node.isTerminal()) {
        result += "(";
    }
    
    for (let i = 0, children = node.getSupposedChildren(); i < children; i++) {
        result += GetTreeAsString(node.getChild(i));

        if (i != children - 1) {
            result += ",";
        }
    }

    if (!node.isTerminal()) {
        result += ")";
    }

    return result;
}

/**
 * 
 * @param {GenericTreeNode} root
 * @returns {GenericTreeNode} random node 
 */
function GetRadnomNodeInTree(root) {
    return GetNodeAtIndex(root, Math.floor(Math.random() * root.getNodeCount()));
}

/**
 * 
 * @param {GenericTreeNode} root of the tree
 * @param {number} index of the node 
 */
function GetNodeAtIndex(root, index) {
    if (index >= root.getNodeCount()) {
        throw `Index outside of allowed range. Node has node-count: ${root.getNodeCount()} and index given was: ${index}`;
    }
    
    if (index === 0) {
        return root;
    }

    index--;

    for (let i = 0, children = root.getSupposedChildren(); i < children; i++) {
        let child = root.getChild(i);

        if (child.getNodeCount() > index) {
            // If child has more nodes below itself than our index, the node we are searching is at or bellow child node.
            return GetNodeAtIndex(child, index);
        } else {
            // Else if child has less nodes than index, we reduce index by child size and continue to next child.
            index -= child.getNodeCount();
        }
    }
}

// Helper functions for better syntax in other functions.

/**
 * @returns {GenericTreeNode} random terminal node
 */
function RandomTerminalNode() {
    return TerminalNodes[Math.floor(Math.random() * TerminalNodes.length)].clone();
}

/**
 * @returns {GenericTreeNode} random functional node
 */
function RandomFunctionalNode() {
    return FunctionalNodes[Math.floor(Math.random() * FunctionalNodes.length)].clone();
}

/**
 * @returns {GenericTreeNode} random node
 */
function RandomNode() {
    return AllNodes[Math.floor(Math.random() * AllNodes.length)].clone();
}

module.exports =  {
    GenerateTreeWithRoot,
    GenerateTree,
    GetTreeAsString,
    GetRadnomNodeInTree,
    GetNodeAtIndex
}

// Class serves as a simple way to store infromation about an ant.
// It stores information about current position, rotation and whether ant ate food on this move.
module.exports = class AntState {
    /**
     * 
     * @param {number} row 
     * @param {number} column 
     * @param {string} rotation 
     * @param {boolean} ateFood
     */
    constructor(row, column, rotation, ateFood) {
        this.row = row;
        this.column = column;
        this.rotation = rotation;
        this.ateFood = ateFood;
    }

    getRow() {
        return this.row;
    }

    getColumn() {
        return this.column;
    }

    getRotation() {
        return this.rotation;
    }

    hasEatenFood() {
        return this.ateFood;
    }
}
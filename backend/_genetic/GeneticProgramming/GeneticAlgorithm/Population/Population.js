const PopulationInitializer = require('./PopulationInitializer');
const Ant = require('../Ant/Ant');

const ExecutionEngine = require('../../ExecutionEngine/ExecutionEngine');
const SimpleEvaluator = require('../Operators/Evaluators/SimpleEvaluator');
const nTournamentSelection = require('../Operators/Selections/nTournamentSelection');
const SimpleMutation = require('../Operators/Mutations/SimpleMutation');
const OnePointCrossover = require('../Operators/Crossovers/OnePointCrossover');

const checkParams = (populationParams) => {


}

module.exports = class Population {
    constructor(populationParams, engine) {
        this.engine = engine;

        this.population = PopulationInitializer(populationParams.popInitParams);
        this.generations = populationParams.generations;
        this.reproductionRate = populationParams.reproductionRate;
        this.mutationRate = populationParams.mutationRate;
        this.crossoverRate = populationParams.crossoverRate;

        this.elitist = populationParams.elitist;
        this.maxFitness = populationParams.maxFitness;
        this.maxGensNoProgress = populationParams.maxGensNoProgress;
        this.mutationInjection = populationParams.mutationInjection;

        // Function used to compare to ants for sorting the population.
        // Extracted here so that function object is only created once.
        this.compareFunction = (ant1, ant2) => {
            // Not using functions here to increase performance
            return ant2.fitness - ant1.fitness;
        }

        // TODO: It will maybe be possible to add different implementations of operators later.
        // TODO: Think about sending the approprate populationParams.operators - object so that implementations can make sure the parameters for them are valid.
        this.evaluator = new SimpleEvaluator(engine, populationParams.operators.evaluator.plagirismFactor);
        this.selection = new nTournamentSelection(populationParams.operators.selection.sizeOfTournament);
        this.mutator = new SimpleMutation(populationParams.operators.mutation.maxDepth, populationParams.operators.mutation.maxNodes);
        this.crossover = new OnePointCrossover(populationParams.operators.crossover.maxNodes, populationParams.operators.crossover.maxEffort);
    
        this.currentGeneration = 0;
    }

    runAllGenerations() {
        let gensWithNoProgress = 0;
        let maxFitness = 0;

        for (let i = 0; i < this.generations; i++) {
            this.evaluatePopulation();

            gensWithNoProgress = maxFitness >= this.population[0].getFitness() ? gensWithNoProgress + 1 : 0;
            maxFitness = this.population[0].getFitness();

            console.log(`Generation [${i+1} out of ${this.generations}] --> Best ant ate [${this.population[0].getFoodEaten()}] pieces of food. Maximum is: [${this.maxFitness}]`);

            if (this.mutationInjection && gensWithNoProgress === Math.floor(this.maxGensNoProgress / 2)) {
                // TODO: Add mutation injection here later.
                this.activateMutationInjection(20);
                continue;
            }

            if (gensWithNoProgress >= this.maxGensNoProgress || maxFitness === this.maxFitness) {
                break;
            }

            this.runNextGeneration();
        }

        this.evaluatePopulation();
        console.log(`Final generation : Best ant ate [${this.population[0].getFoodEaten()}] pieces of food.`);
    }

    runNextGeneration() {
        if (this.currentGeneration >= this.generations) {
            // TODO: Log that function was called after all generations were finished. No need to throw error, just return from function. 
            return;
        }

        let newGeneration = [];
        if (this.elitist) {
            // If algorithm is elitist we save the best ant from previous generation.
            newGeneration.push(this.population[0]);
        }

        while(newGeneration.length < this.population.length) {
            //this.putAntForNextGeneration(newGeneration);

            let randomNum = Math.random() * (this.reproductionRate + this.mutationRate + this.crossoverRate);

            if (randomNum < this.reproductionRate) {
                // Reproduction was selected.
                // We can use unchanged reference to parent because parent reference should not change during creating of new generation.
                newGeneration.push(this.selection.select(this.population));
            } else if (randomNum < this.reproductionRate + this.mutationRate) {
                // Mutation was selected.
                newGeneration.push(this.mutator.mutate(this.selection.select(this.population).clone()));
            } else if (randomNum < this.reproductionRate + this.mutationRate + this.crossoverRate) {
                // Crossover was selected.
                let parent1Clone = this.selection.select(this.population).clone();
                let parent2Clone = this.selection.select(this.population).clone();

                let err = this.crossover.cross(parent1Clone, parent2Clone);
                if (err) {
                    newGeneration.push(parent1Clone);
                    newGeneration.push(this.mutator.mutate(parent2Clone));
                    continue;
                }

                newGeneration.push(parent1Clone);
                newGeneration.push(parent2Clone);
            } else {
                throw `Random number generated in 'getAntForNextGeneration' is out of allowed range. 
                        Allowed:${this.reproductionRate + this.mutationRate + this.crossoverRate}, Generated: ${randomNum}`;
            }
        }

        this.population = newGeneration;
        this.currentGeneration++;
    }

    /**
     * 
     * @param {Array} newGeneration 
     */
    putAntForNextGeneration(newGeneration) {
        let randomNum = Math.random() * (this.reproductionRate + this.mutationRate + this.crossoverRate);

        if (randomNum < this.reproductionRate) {
            // Reproduction was selected.
            // We can use unchanged reference to parent because parent reference should not change during creating of new generation.
            newGeneration.push(this.selection.select(this.population));
        } else if (randomNum < this.reproductionRate + this.mutationRate) {
            // Mutation was selected.
            newGeneration.push(this.mutator.mutate(this.selection.select(this.population).clone()));
        } else if (randomNum < this.reproductionRate + this.mutationRate + this.crossoverRate) {
            // Crossover was selected.
            let parent1Clone = this.selection.select(this.population).clone();
            let parent2Clone = this.selection.select(this.population).clone();

            let err = this.crossover.cross(parent1Clone, parent2Clone);
            if (err) {
                newGeneration.push(parent1Clone);
                newGeneration.push(this.mutator.mutate(parent2Clone));
                return;
            }

            newGeneration.push(parent1Clone);
            newGeneration.push(parent2Clone);
        } else {
            throw `Random number generated in 'getAntForNextGeneration' is out of allowed range. 
                    Allowed:${this.reproductionRate + this.mutationRate + this.crossoverRate}, Generated: ${randomNum}`;
        }
    }

    activateMutationInjection(mutationIntensity) {
        console.log(" ---- ----- --- -- Activated mutation injection - --- -- -- --- --- - --");

        for (let i = 0; i < this.population.length; i++) {
            for (let j = 0; j < mutationIntensity; j++) {
                this.mutator.mutate(this.population[i]);
            }
        }
    }

    evaluatePopulation() {
        for (let i = 0; i < this.population.length; i++) {
            this.evaluator.evaluate(this.population[i]);
        }

        this.population.sort(this.compareFunction);
    }

    getEntirePopulation() {
        return this.population;
    }

    getBestAnt() {
        return this.population[0];
    }
}
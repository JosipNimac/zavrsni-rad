const Ant = require('../../Ant/Ant');
const ExecutionEngine = require('../../../ExecutionEngine/ExecutionEngine');

module.exports = class SimpleEvaluator {

    /**
     * 
     * @param {ExecutionEngine} engine 
     * @param {number} plagarismFactor 
     */
    constructor(engine, plagarismFactor) {
        this.engine = engine;
        this.plagarismFactor = plagarismFactor;
    }

    /**
     * Method will evaluate ant based on how much food it is able to eat. More food means better evaluation.
     * 
     * @param {Ant} ant that should be evaluated. 
     */
    evaluate(ant) {
        // Setting up a new ExecutionState so that each evaluation is independed of another.
        this.engine.refreshExecutionState();
        
        // While we have available moves ant will just repeat it's tree.
        do {
            ant.getRoot().execute(this.engine.getCurrentExecutionState());
        } while(this.engine.getCurrentExecutionState().getNumberOfAvailableActions() > 0);
        
        // After ant is done we check how much food it has eaten.
        ant.foodEaten = this.engine.getCurrentExecutionState().getFoodEaten();

        // If ant ate the same amount of food as it's parent w algorithm will claim that it is copying it.
        // Because of that those ants will lose their fitness based on plagarismFactor.
        if (ant.foodEaten === ant.ParentFoodEaten) {
            ant.fitness = ant.foodEaten * this.plagirismFactor;
        } else {
            ant.fitness = ant.foodEaten;
        }
    }
}
const Ant = require('../../Ant/Ant');

module.exports = class nTournamentSelection {
    constructor(tournamentSize) {
        this.tournamentSize = tournamentSize;
    }

    /**
     * Method will select the Ant from the array of ants using the n-tournament selection technique.
     * Given array must be sorted. 
     * 
     * @param {Array} population sorted population
     * @returns {Ant} ant from the population
     */
    select(population) {
        let minIndex;

        for (let i = 0; i < this.tournamentSize; i++) {
            const randomIndex = Math.floor(Math.random() * population.length);

            if (!minIndex || randomIndex < minIndex) {
                minIndex = randomIndex;
            }
        }

        return population[minIndex];
    }
}
const fs = require('fs'); 
const path = require('path');
const Constants = require('./Constants');
const ExecutionEngine = require('./ExecutionEngine/ExecutionEngine');
const Population = require('./GeneticAlgorithm/Population/Population');
const AntState = require('./GeneticAlgorithm/Ant/AntState');

module.exports = class GeneticRunner {

    constructor(params) {

        let mapParams = prepareMapParams(params.mapParams);
        this.engine = new ExecutionEngine(
            mapParams.antRow,
            mapParams.antColumn,
            mapParams.antRotation,
            mapParams.mapRows, 
            mapParams.mapColumns,
            mapParams.foodLocations
        );
        
        params.algorithmParams.maxFitness = countFoodOnMap(mapParams.foodLocations);

        this.population = new Population(params.algorithmParams, this.engine);
    }

    startTraining() {
        this.population.runAllGenerations();
    }

    /**
     * @returns {Array.<AntState>}
     */
    getBestAntStates() {
        return this.engine.getAntStates(this.population.getBestAnt());
    }
}

function prepareMapParams(mapParams) {
    const {antRow, antColumn, antRotation, fromFile} = mapParams;

    if (antRow === undefined || 
        antColumn === undefined || 
        !antRotation || 
        fromFile === undefined) {
        
            throw JSON.stringify(mapParams);
    }

    let preparedParams = {
        antRow: antRow,
        antColumn: antColumn,
        antRotation: antRotation
    }

    if (mapParams.fromFile) {
        const mapData = loadMapFromFile(mapParams.filePath);

        preparedParams.mapRows = mapData.mapRows;
        preparedParams.mapColumns = mapData.mapColumns;
        preparedParams.foodLocations = mapData.foodLocations;

    } else {
        if (!mapParams.foodLocations) {
            throw "No food locations provided";
        }
        
        preparedParams.mapRows = mapParams.foodLocations.length;
        preparedParams.mapColumns = mapParams.foodLocations[0].length;
        preparedParams.foodLocations = mapParams.foodLocations;
    }

    return preparedParams;
}

function loadMapFromFile(filePath) {
    if (typeof filePath !== "string" || !filePath.startsWith(`Maps/`)) {
        throw "Illegal path to file.";
    } else if (!fs.existsSync(__dirname + "/" + filePath)) {
        throw "File with given name does not exist.";
    }


    const data = fs.readFileSync(__dirname + "/" + filePath, "utf-8").split(/\r\n|\r|\n/);

    const firstLine = data[0].split("x");

    if (!Array.isArray(firstLine) || firstLine.length !== 2 || data.length !== parseInt(firstLine[0]) + 1) {
        throw "Illegal fromat of map file.";
    }

    const foodLocations = [];

    for (let i = 1; i < data.length; i++) {
        const line = [];

        for (let j = 0; j < data[i].length; j++) {
            line.push(data[i].charAt(j));
        }

        foodLocations.push(line);
    }

    return {
        mapRows: parseInt(firstLine[0]),
        mapColumns: parseInt(firstLine[1]),
        foodLocations: foodLocations
    };
}

/**
 * 
 * @param {Array[]} foodLocations 
 */
function countFoodOnMap(foodLocations) {
    let amountOfFood = 0;

    for (let i = 0; i < foodLocations.length; i++) {
        for (let j = 0; j < foodLocations[i].length; j++) {
            if (foodLocations[i][j] === Constants.MAP_DATA.FOOD) {
                amountOfFood++;
            }
        }
    }

    return amountOfFood;
}
require('dotenv').config();
const logger = require('./config/logger');

const express = require('express');
const app = express();

require('./config/express-server-config')(app);

// Start listening for requests.
const port = process.env.PORT || 3001;
app.listen(port, () => {
    logger.info('Server started on port: ' + port);
});
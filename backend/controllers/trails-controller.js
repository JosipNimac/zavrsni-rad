const logger = require('../config/logger');
const trailService = require('../services/trail-services/trail-service');

function saveNewTrail(req, res, next) {
    if (req.body.mapParams === undefined || req.body.thumbnailBase64 === undefined) {
        return res.status(400).send({message: "Incomplete data about map was provided."});
    }
    
    trailService.saveNewTrail(req.body.mapParams, req.body.thumbnailBase64, req.user, (err) => {
        if (err) {
            if (err.invalidParams) {
                res.status(400).send({invalidParams: true});
            } else {
                res.status(500).send({message: "Error on server. Check logs."});
            }

            return;
        }

        res.status(200).send({message: "Trail saved successfully."});
    });
}

function getTrailInfoById(req, res, next) {
    if (req.params.trailID === undefined) {
        return res.status(400).send({message: "No trail ID given!"});
    }

    trailService.getBasicTrailInfoById(req.params.trailID, (err, trailInfo) => {
        if (err && err.notFound) {
            return res.status(404).send({message: `Trail with given id [${req.params.trailID}] not found!`});
        } else if (err) {
            return res.status(500).send({message: "Server was unable to process your request."});
        }

        return res.status(200).json(trailInfo);
    });
}

function getTrailInfoByCode(req, res, next) {
    if (req.params.trailCode === undefined) {
        return res.status(400).send({message: "No trail code given!"});
    }

    trailService.getBasicTrailInfoByCode(req.params.trailCode, (err, trailInfo) => {
        if (err && err.notFound) {
            return res.status(404).send({message: `Trail with given code [${req.params.trailCode}] not found!`});
        } else if (err) {
            return res.status(500).send({message: "Server was unable to process your request."});
        }

        return res.status(200).json(trailInfo);
    });
}

function getTrailThumbnail(req, res, next) {
    if (req.query.path === undefined) {
        return res.status(400).send({message: "No path for thumbnail given!"});
    }

    trailService.getTrailThumbnail(req.query.path, (err, data) => {
        if (err && err.notFound) {
            return res.status(404).send({message: "Trail thumbnail was not found!"});
        } else if (err) {
            return res.status(500).send({message: "Server was unable to process your request."});
        }

        return res.status(200).send({image: data});
    });
}

function getAllOffsetTrails(req, res, next) {
    if (req.body.offsetDate === undefined || req.body.limit === undefined) {
        return res.status(400).send({message: "Not all data was provided. Please provide both 'offsetData' and 'limit'."});
    }

    trailService.getAllOffsetTrails(req.body.offsetDate, req.body.limit, (err, result) => {
        if (err) {
            return res.status(500).send("Server was unable to process your request.");
        }

        return res.status(200).json(result);
    });
}

function getTrailDetails(req, res, next) {
    if (req.params.code === undefined) {
        return res.status(400).send({message: "Trail code was not provided."});
    }

    trailService.getTrailDetailByCode(req.params.code, (err, mapParams) => {
        if (err && err.notFound) {
            return res.status(404).send({message: "Trail with given code was not found."});
        } else if (err) {
            return res.status(500).send({message: "Server was unable to process your request."});
        }

        return res.status(200).json(mapParams);
    });
}

function getRandomTrail(req, res, next) {
    trailService.getRandomDefaultTrail((err, trail) => {
        if (err) {
            if (err.notFound) {
                return res.status(404).send("Trail was not found.");
            }
        
            return res.status(500).send({message: "Unable to get a random trail."});
        }

        return res.status(200).json(trail);
    });
}

function getUserTrails(req, res, next) {
    if (req.query.page === undefined || req.query.pageSize === undefined) {
        return res.status(400).send({message: "Page or page size was not provided."});
    }

    trailService.getUserTrails(req.user.id, req.query.page, req.query.pageSize, (err, data) => {
        if (err) {
            if (err.illegalPage) {
                return res.status(400).send({message: "Page given was outside of allowed range."});
            }
            
            return res.status(500).send({message: "Server was unable to process your request."});
        }

        return res.status(200).json(data);
    });
}

function searchTrails(req, res, next) {
    if (req.query.q === undefined || req.query.q.trim().length === 0) {
        return res.status(400).send({message: "You must provide search query!"});
    }

    trailService.searchTrails(req.query.q.trim(), (err, trails) => {
        if (err) {
            return res.status(500).send({message: "Server was unable to process your request."});
        }

        return res.status(200).json(trails);
    });
}

module.exports = {
    saveNewTrail,
    getTrailInfoById, 
    getTrailInfoByCode,
    getTrailThumbnail,
    getAllOffsetTrails,
    getTrailDetails,
    getRandomTrail,
    getUserTrails,
    searchTrails
}
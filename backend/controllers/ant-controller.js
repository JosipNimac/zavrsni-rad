const logger = require('../config/logger');
const antService = require('../services/ant-services/ant-service');

function saveNewAnt(req, res, next) {
    if (req.body.name === undefined || req.body.data === undefined || req.body.algParams === undefined || req.body.trailID === undefined) {
        return res.status(400).send({message: "Incomplete data sent."});
    }

    antData = {
        name: req.body.name,
        data: req.body.data,
        userID: req.user.id,
        trailID: req.body.trailID
    }

    antService.saveNewAnt(antData, req.body.algParams, (err) => {
        if (err) {
            return res.status(500).send({message: "Server was unable to process your request."});
        }

        logger.info(`User [${req.user.username}] has saved a new ant named: ${req.body.name}`);
        return res.status(200).send({message: "Ant was successfully saved."});
    });
}

function getAntById(req, res, next) {
    if (req.params.id === undefined) {
        return res.status(200).send({message: "Ant ID was not provided."});
    }

    antService.getAntById(req.params.id, (err, ant) => {
        if (err) {
            if (err.notFound) {
                return res.status(404).send({message: `Ant with given id [${req.param.id}] was not found.`});
            } else {
                return res.status(500).send({message: "Server was unable to process your request."});;
            }
        }

        return res.status(200).json(ant);
    });
}

function getAntByCode(req, res, next) {
    if (req.params.code === undefined) {
        return res.status(200).send({message: "Ant code was not provided."});
    }

    antService.getAntByCode(req.params.code, (err, ant) => {
        if (err) {
            if (err.notFound) {
                return res.status(404).send({message: `Ant with given code [${req.param.code}] was not found.`});
            } else {
                return res.status(500).send({message: "Server was unable to process your request."});;
            }
        }

        return res.status(200).json(ant);
    });
}

function getRecentAnts(req, res, next) {
    if (req.user === undefined) {
        return res.status(401).send({message: "Unauthorized"});
    }

    antService.getRecentAnts(req.user.id, (err, ants) => {
        if (err) {
            return res.status(500).send({message: "Server was unable to process your request."});
        }

        return res.status(200).json(ants);
    });
}

function getParamsForAnt(req, res, next) {
    if (req.params.antID === undefined) {
        return res.status(400).send({message: "No antID was provided!"});
    }

    antService.getParamsForAnt(req.params.antID, (err, params) => {
        if (err) {
            if (err.notFound) {
                return res.status(404).send({message: "Params for given antID were not found."});
            }

            return res.status(500).send({message: "Server was unable to process your request."});
        }

        return res.status(200).json(params);
    });
}

function getAntsBySubQuery(req, res, next) {
    if (req.query.subQuery === undefined || req.query.limit === undefined) {
        return res.status(400).send({message: "Incomplete request! Please send your subquery, limit and whether or not you are searching by code or name."});
    }

    antService.getAntsBySubQuery(req.query.subQuery, req.query.limit, req.query.code === "true", (err, ants) => {
        if (err) {
            return res.status(500).send({message: "Server was unable to process your request."});
        }

        return res.status(200).json(ants);
    });
}

function getRandomAdminAnt(req, res, next) {
    if (req.params.trailCode === undefined) {
        return res.status(400).send({message: "No trail code was provided."});
    }

    antService.getRandomAdminAnt(req.params.trailCode, (err, ant) => {
        if (err) {
            return res.send(500).send({message: "Server was unable to process your request."});
        }

        return res.status(200).json(ant);
    });
}

function getUserAnts(req, res, next) {
    if (req.query.page === undefined || req.query.pageSize === undefined) {
        return res.status(400).send({message: "Page or page size was not provided."});
    }

    antService.getUserAnts(req.user.id, req.query.page, req.query.pageSize, (err, data) => {
        if (err) {
            if (err.illegalPage) {
                return res.status(400).send({message: "Page given was outside of allowed range."});
            }
            
            return res.status(500).send({message: "Server was unable to process your request."});
        }

        return res.status(200).json(data);
    });
}

module.exports = {
    saveNewAnt,
    getAntById,
    getAntByCode,
    getRecentAnts,
    getParamsForAnt,
    getAntsBySubQuery,
    getRandomAdminAnt,
    getUserAnts,
}
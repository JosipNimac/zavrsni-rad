const logger = require('../config/logger');
const authService = require('../services/user-services/auth-service');
const emailService = require('../services/user-services/email-token-service');

const register = function(req, res, next) {
    if (req.body.username == undefined || req.body.email == undefined || req.body.password == undefined) {
        return res.status(400).send({message: "Not all credentails were provided."});
    }

    authService.registerUser(req.body, (err, token) => {
        if (err) {
            return res.status(400).send({message: "Registration failed. Try again later."});
        }

        emailService.sendTokenViaMail(token, req.body.email);

        return res.status(200).send({message: "Registration completed. Please confirm your e-mail."});
    });
};

const confirmRegistration = (req, res, next) => {
    if (req.params.token === undefined) {
        return res.status(400).send({message: "Token was not provided"});
    }

    authService.confirmRegistration(req.params.token, (err, userID) => {
        if (err) {
            return res.status(400).send({message: "Server was unable to activate your account. Is it already activated?"});
        }

        return res.status(200).send({message: "Account activated successfully."});
    });
}

const login = function(req, res, next) {
    if (!req.user) {
        return res.status(401).json({message: 'Unauthorized'});
    }

    authService.updateUserLogin(req.user.id);

    logger.info(`User [id: ${req.user.id}, username: ${req.user.username}] has just logged in!`);
    res.status(200).json({message: 'Login successful'});
};

const logout = function(req, res, next) {
    logger.info(`User [id: ${req.user.id}, username: ${req.user.username}] has just logged out!`);
    req.logOut();

    req.session.destroy((err) => {
        if (err) {
            // TODO: Log that this error happened here.
        }

        // Remove cookie when user logouts.
        res.clearCookie('connect.sid');
        res.status(200).json({message: 'Logout successful'});
    });
};

const getSelfInfo = function(req, res, next) {
    if (!req.user) {
        return res.status(200).json({notLoggedIn: true});
    }

    // We are sending just some of the data back to user.
    res.status(200).json({
        id: req.user.id,
        username: req.user.username,
        email: req.user.email
    });
}

const getSelfEditInfo = function(req, res, next) {
    if (!req.user) {
        return res.status(401).json({message: "Unauthorized"});
    }

    res.status(200).json({
        username: req.user.username,
        email: req.user.email
    });
}

const isLoggedIn = function(req, res, next) {
    return res.status(200).send({loggedIn: req.user !== undefined});
}

module.exports = {
    register,
    confirmRegistration,
    login,
    logout,
    getSelfInfo,
    getSelfEditInfo,
    isLoggedIn
}
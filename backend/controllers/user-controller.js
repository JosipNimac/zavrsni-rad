const logger = require('../config/logger');
const userService = require('../services/user-services/user-service');

const usernameCheck = function(req, res, next) {
    userService.checkIfUsernameExists(req.params.username, (exists) => {
        res.status(200).send({exists: exists});
    });
}

const emailCheck = function(req, res, next) {
    userService.checkIfEmailExists(req.params.email, (exists) => {
        res.status(200).send({exists: exists});
    });
}

module.exports = {
    usernameCheck,
    emailCheck
}
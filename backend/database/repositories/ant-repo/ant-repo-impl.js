const logger = require('../../../config/logger');

function saveNewAnt(antData, callback, connection) {
    const query = `INSERT INTO ant(name, code, description, timestamp_created, timestamp_modified, user_created, user_modified) VALUES 
                    ($1, $2, $3, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP, $4, $4) returning id;`;

    const {name, code, data, user} = antData;

    connection.client.query(query, [name, code, data, user], (err, queryRes) => {
        if (err) {
            logger.error("Problem on database while saving a new ant: ", err);
            return callback(err);
        }

        return callback(null, queryRes.rows[0].id);
    });
}

function getAntById(id, callback, connection) {
    const query = 'SELECT ant.*, "user".username FROM ant JOIN "user" ON "user".id = ant.user_created WHERE ant.id = $1;';

    connection.client.query(query, [id], (err, queryRes) => {
        if (err) {
            logger.error(`Problem on database while getting ant with id [${id}]: `, err);
            return callback(err);
        }

        if (queryRes.rows.length === 0) {
            logger.warn(`Ant with requested id [${id}] does not exist.`);
            return callback({noAnt: true});
        }

        return callback(null, queryRes.rows[0]);
    });
}

function getAntByCode(code, callback, connection) {
    const query = 'SELECT ant.*, "user".username FROM ant JOIN "user" ON "user".id = ant.user_created WHERE ant.code = $1;';

    connection.client.query(query, [code], (err, queryRes) => {
        if (err) {
            logger.error(`Problem on database while getting ant with code [${code}]: `, err);
            return callback(err);
        }

        if (queryRes.rows.length === 0) {
            logger.warn(`Ant with requested code [${code}] does not exist.`);
            return callback({noAnt: true});
        }

        return callback(null, queryRes.rows[0]);
    });
}

function saveAntParams(algData, callback, connection) {
    const query = `INSERT INTO algorithm_params(params, ant_id, trail_id) VALUES 
                    ($1, $2, $3) returning id;`;
    const {algParams, antID, trailID} = algData;

    connection.client.query(query, [algParams, antID, trailID], (err, queryRes) => {
        if (err) {
            logger.error("Problem on database while saving algorithm params: ", err);
            return callback(err);
        }

        return callback(null, queryRes.rows[0].id);
    });
}

function getRecentAnts(userID, callback, connection) {
    const query = 'SELECT ant.*, "user".username FROM ant LEFT OUTER JOIN "user" on "user".id = ant.user_created WHERE ant.user_created = $1 ORDER BY ant.timestamp_modified DESC LIMIT 5;';

    connection.client.query(query, [userID], (err, queryRes) => {
        if (err) {
            logger.error(`Problem on database while retrieving recent ants for user with id [${userID}]: `, err);
            return callback(err);
        }

        return callback(null, queryRes.rows);
    });
}

function getParamsForAnt(antID, callback, connection) {
    const query = "SELECT * FROM algorithm_params WHERE ant_id=$1;";

    connection.client.query(query, [antID], (err, queryRes) => {
        if (err) {
            logger.error(`Problem on database while retrieving parameters used to train ant with id [${antID}]: `, err);
            return callback(err);
        }

        if (queryRes.rows.length === 0) {
            logger.warn(`Algorithm params for ant with id [${antID}] does not exist.`);
            return callback({noParams: true});
        }

        return callback(null, queryRes.rows[0]);
    });
}

function getAntsBySubQuery(antsParams, callback, connection) {
    let {subQuery, limit, code} = antsParams;

    subQuery = "%" + subQuery + "%";
    const query = `SELECT ant.*, "user".username FROM ant LEFT OUTER JOIN "user" on "user".id = ant.user_created WHERE ant.${code ? "code" : "name"} ILIKE $1 
                        ORDER BY ant.timestamp_created DESC LIMIT $2;`;

    connection.client.query(query, [subQuery, limit], (err, queryRes) => {
        if (err) {
            logger.error(`Problem on database while getting ants with subquery [${code ? "code" : "name"} = ${subQuery}]: `, err);
            return callback(err);
        }

        return callback(null, queryRes.rows);
    });
}

function getAdminAntsOnTrail(trailCode, callback, connection) {
    const query = `SELECT ant.* from ant JOIN algorithm_params on ant.id = algorithm_params.ant_id JOIN trail on trail.id = algorithm_params.trail_id 
                    JOIN "user" on "user".id = ant.user_created JOIN user_role on user_role.user_id = "user".id 
                    WHERE user_role.role_id = 1 AND trail.code=$1;`;

    connection.client.query(query, [trailCode], (err, queryRes) => {
        if (err) {
            logger.error(`Problem on database while getting random ant for trail with code [${trailCode}]: `, err);
            return callback(err);
        }

        return callback(null, queryRes.rows);
    });
}

function getUserAnts(userID, callback, connection) {
    const query = `SELECT * FROM ant WHERE ant.user_created = $1 ORDER BY ant.timestamp_created DESC;`;

    connection.client.query(query, [userID], (err, queryRes) => {
        if (err) {
            logger.error(`Problem on database while getting ants by user with id [${userID}]: `, err);
            return callback(err);
        }

        return callback(null, queryRes.rows);
    });
}

module.exports = {
    saveNewAnt,
    saveAntParams,
    getAntById,
    getAntByCode,
    getRecentAnts,
    getParamsForAnt,
    getAntsBySubQuery,
    getAdminAntsOnTrail,
    getUserAnts
}
const db = require('../../index');
const antRepoImpl = require('./ant-repo-impl');

function saveNewAnt(name, code, data, user, callback, connection) {
    if (connection === undefined) {
        return getConnection(antRepoImpl.saveNewAnt, {name, code, data, user}, callback);
    }

    antRepoImpl.saveNewAnt({name, code, data, user}, callback, connection);
}

function getAntById(id, callback, connection) {
    if (connection === undefined) {
        return getConnection(antRepoImpl.getAntById, id, callback);
    }

    antRepoImpl.getAntById(id, callback, connection);
}

function getAntByCode(code, callback, connection) {
    if (connection === undefined) {
        return getConnection(antRepoImpl.getAntByCode, code, callback);
    }

    antRepoImpl.getAntByCode(code, callback, connection);
}

function saveAlgParams(algParams, antID, trailID, callback, connection) {
    if (connection === undefined) {
        return getConnection(antRepoImpl.saveAntParams, {algParams, antID, trailID}, callback);
    }

    antRepoImpl.saveAntParams({algParams, antID, trailID}, callback, connection);
}

function getRecentAnts(userID, callback, connection) {
    if (connection === undefined) {
        return getConnection(antRepoImpl.getRecentAnts, userID, callback);
    }

    antRepoImpl.getRecentAnts(userID, callback, connection);
}

function getParamsForAnt(antID, callback, connection) {
    if (connection === undefined) {
        return getConnection(antRepoImpl.getParamsForAnt, antID, callback);
    }

    antRepoImpl.getParamsForAnt(antID, callback, connection);
}

function getAntsBySubQuery(subQuery, limit, code, callback, connection) {
    if (connection === undefined) {
        return getConnection(antRepoImpl.getAntsBySubQuery, {subQuery, limit, code}, callback);
    }

    antRepoImpl.getAntsBySubQuery({subQuery, limit, code}, callback, connection);
}

function getAdminAntsOnTrail(trailCode, callback, connection) {
    if (connection === undefined) {
        return getConnection(antRepoImpl.getAdminAntsOnTrail, trailCode, callback);
    }

    antRepoImpl.getAdminAntsOnTrail(trailCode, callback, connection);
}

function getUserAnts(userID, callback, connection) {
    if (connection === undefined) {
        return getConnection(antRepoImpl.getUserAnts, userID, callback);
    }

    antRepoImpl.getUserAnts(userID, callback, connection);
}

function getConnection(db_function, param, callback) {
    db.getConnection(connection => {
        return db_function(param, (err, data) => {
            connection.done();
            return callback(err, data);
        }, connection);
    });
}

module.exports = {
    saveNewAnt,
    saveAlgParams,
    getAntById,
    getAntByCode,
    getRecentAnts,
    getParamsForAnt,
    getAntsBySubQuery,
    getAdminAntsOnTrail,
    getUserAnts
}
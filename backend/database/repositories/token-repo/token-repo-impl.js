const logger = require('../../../config/logger');

function saveNewToken(tokenParams, callback, connection) {
    const query = "INSERT INTO user_token (token, user_id, is_claimed) VALUES ($1, $2, false) RETURNING id;";
    
    const {tokenText, userID} = tokenParams;

    connection.client.query(query, [tokenText, userID], (err, queryRes) => {
        if (err) {
            logger.error("Problem on database while saving new token: ", err);
            return callback(err);
        }

        return callback(null, queryRes.rows[0].id);
    });
}

function getTokenByTokenText(tokenText, callback, connection) {
    const query = "SELECT * from user_token WHERE token LIKE $1;"

    connection.client.query(query, [tokenText], (err, queryRes) => {
        if (err) {
            logger.error(`Problem on database while getting token with value [${tokenText}]: `, err);
            return callback(err);
        }

        return callback(null, queryRes.rows[0]);
    });
}

function activateTokenByTokenText(tokenText, callback, connection) {
    const query = "UPDATE user_token SET (is_claimed, timestamp_claimed) = (true, CURRENT_TIMESTAMP) WHERE token LIKE $1 AND timestamp_claimed IS NULL RETURNING user_id;";

    connection.client.query(query, [tokenText], (err, queryRes) => {
        if (err) {
            logger.error(`Problem on database while activating token with value [${tokenText}]: `, err);
            return callback(err);
        }

        if (queryRes.rows.length !== 1) {
            return callback("Token alredy claimed.");
        }

        return callback(null, queryRes.rows[0].user_id);
    });
}

module.exports = {
    saveNewToken,
    getTokenByTokenText,
    activateTokenByTokenText
}
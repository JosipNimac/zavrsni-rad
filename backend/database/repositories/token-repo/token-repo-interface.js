const db = require('../../index');
const tokenRepoImpl = require('./token-repo-impl');
const logger = require('../../../config/logger');

function saveNewToken(userID, tokenText, callback, connection) {
    if (connection === undefined) {
        return getConnection(tokenRepoImpl.saveNewToken, {userID, tokenText}, callback);
    }

    tokenRepoImpl.saveNewToken({tokenText, userID}, callback, connection);
}

function getTokenByTokenText(tokenText, callback, connection) {
    if (connection === undefined) {
        return getConnection(tokenRepoImpl.getTokenByTokenText, tokenText, callback);
    }

    tokenRepoImpl.getTokenByTokenText(tokenText, callback, connection);
}

function activateTokenByTokenText(tokenText, callback, connection) {
    if (connection === undefined) {
        return getConnection(tokenRepoImpl.activateTokenByTokenText, tokenText, callback);
    }

    tokenRepoImpl.activateTokenByTokenText(tokenText, callback, connection);
}

function getConnection(db_function, param, callback) {
    db.getConnection(connection => {
        return db_function(param, (err, data) => {
            connection.done();
            return callback(err, data);
        }, connection);
    });
}

module.exports = {
    saveNewToken,
    getTokenByTokenText,
    activateTokenByTokenText
};
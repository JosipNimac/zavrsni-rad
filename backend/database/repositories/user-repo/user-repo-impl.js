const logger = require('../../../config/logger');

function createNewUser(userData, callback, connection) {
    const query = `INSERT INTO "user" (username, password_hash, email, activated, timestamp_created, timestamp_modified) VALUES 
                        ($1, $2, $3, false, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP) RETURNING id;`;
    
    connection.client.query(query, [userData.username, userData.password_hash, userData.email], (err, queryRes) => {
        if (err) {
            logger.error("Problem on database while creating new user: ", err);
            return callback(err);
        }

        return callback(null, queryRes.rows[0].id);
    });
}

function activateUser(userID, callback, connection) {
    const query = 'UPDATE "user" SET (activated, timestamp_modified) = (true, CURRENT_TIMESTAMP) WHERE id = $1 RETURNING id;';

    connection.client.query(query, [userID], (err, queryRes) => {
        if (err) {
            logger.error(`Problem on database while activating user [${userID}]: `, err);
            return callback(err);
        }

        return callback(null, queryRes.rows[0].id);
    });
}

function getUserByUsername(username, callback, connection) {
    const query = 'SELECT * FROM "user" WHERE username = $1;';

    connection.client.query(query, [username], (err, queryRes) => {
        if (err) {
            logger.error(`Problem on database while getting user with username [${username}]: `, err);
            return callback(err);
        }

        if (queryRes.rows.length !== 1) {
            return callback({custom: "User with given username does not exist."});
        }

        return callback(null, queryRes.rows[0]);
    });
}

function getUserByID(userID, callback, connection) {
    const query = 'SELECT * FROM "user" WHERE id = $1;';

    connection.client.query(query, [userID], (err, queryRes) => {
        if (err) {
            logger.error(`Problem on database while getting user with id [${userID}].`);
            return callback(err);
        }

        return callback(null, queryRes.rows[0]);
    });
}

function updateUserLastLogin(userID, callback, connection) {
    const query = 'UPDATE "user" SET timestamp_last_login = CURRENT_TIMESTAMP WHERE id = $1 returning id;';

    connection.client.query(query, [userID], (err, queryRes) => {
        if (err) {
            logger.error(`Problem on database while updating last login for user with id [${userID}].`);
            return callback(err);
        }

        return callback(null, queryRes.rows[0]);
    });
}

function getUserByEmail(email, callback, connection) {
    const query = 'SELECT * from "user" WHERE email = $1;';

    connection.client.query(query, [email], (err, queryRes) => {
        if (err) {
            logger.error(`Problem on database while getting user with email [${email}].`);
            return callback(err);
        }

        if (queryRes.rows.length !== 1) {
            return callback({custom: "User with given username does not exist."});
        }

        return callback(null, queryRes.rows[0]);
    });
}

module.exports = {
    createNewUser,
    activateUser,
    getUserByUsername,
    getUserByID,
    getUserByEmail,
    updateUserLastLogin
};
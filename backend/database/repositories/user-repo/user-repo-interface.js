const db = require('../../index');
const userRepoImpl = require('./user-repo-impl');

function createNewUser(userData, callback, connection) {
    if (connection === undefined) {
        return getConnection(userRepoImpl.createNewUser, userData, callback);
    }

    userRepoImpl.createNewUser(userData, callback, connection);
}

function activateUser(userID, callback, connection) {
    if (connection === undefined) {
        return getConnection(userRepoImpl.activateUser, userID, callback);
    }

    userRepoImpl.activateUser(userID, callback, connection);
}

function getUserByUsername(username, callback, connection) {
    if (connection === undefined) {
        return getConnection(userRepoImpl.getUserByUsername, username, callback);
    }

    userRepoImpl.getUserByUsername(username, callback, connection);
}

function getUserByID(userID, callback, connection) {
    if (connection === undefined) {
        return getConnection(userRepoImpl.getUserByID, userID, callback);
    }

    userRepoImpl.getUserByID(userID, callback, connection);
}

function updateUserLastLogin(userID, callback, connection) {
    if (connection === undefined) {
        return getConnection(userRepoImpl.updateUserLastLogin, userID, callback);
    }

    userRepoImpl.updateUserLastLogin(userID, callback, connection);
}

function getUserByEmail(email, callback, connection) {
    if (connection === undefined) {
        return getConnection(userRepoImpl.getUserByEmail, email, callback);
    }

    userRepoImpl.getUserByEmail(email, callback, connection);
}

function getConnection(db_function, param, callback) {
    db.getConnection(connection => {
        return db_function(param, (err, data) => {
            connection.done();
            return callback(err, data);
        }, connection);
    });
}

module.exports = {
    createNewUser,
    activateUser,
    getUserByUsername,
    getUserByID,
    getUserByEmail,
    updateUserLastLogin
};
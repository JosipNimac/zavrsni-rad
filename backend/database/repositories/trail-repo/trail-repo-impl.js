const logger = require('../../../config/logger');

function saveNewTrail(trailData, callback, connection) {
    const query = `INSERT INTO "trail"(name, code, path_to_definition, path_to_thumbnail, timestamp_created, timestamp_modified, user_created, user_modified) VALUES
                    ($1, $2, $3, $4, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP, $5, $5) returning id;`;

    const {name, code, path_to_definition, path_to_thumbnail, userID} = trailData;

    connection.client.query(query, [name, code, path_to_definition, path_to_thumbnail, userID], (err, queryRes) => {
        if (err) {
            logger.error("Problem on database while saving new map: ", err);
            return callback(err);
        }

        return callback(null, queryRes.rows[0].id);
    });
}

function getTrailById(id, callback, connection) {
    const query = 'SELECT trail.*, "user".username FROM trail JOIN "user" ON trail.user_created = "user".id WHERE trail.id = $1;';

    connection.client.query(query, [id], (err, queryRes) => {
        if (err) {
            logger.error(`Problem on database while getting trail with id [${id}]: `, err);
            return callback(err);
        }

        if (queryRes.rows.length === 0) {
            logger.warn(`Trail with requested id [${id}] does not exist.`);
            return callback({noTrail: true});
        }

        return callback(null, queryRes.rows[0]);
    });
}

function getTrailByCode(code, callback, connection) {
    const query = `SELECT trail.*, "user".username FROM trail JOIN "user" ON trail.user_created = "user".id WHERE trail.code = $1;`;

    connection.client.query(query, [code], (err, queryRes) => {
        if (err) {
            logger.error(`Problem on database while getting trail with code [${code}]: `, err);
            return callback(err);
        }

        if (queryRes.rows.length < 1) {
            return callback({noTrail: true});
        }

        return callback(null, queryRes.rows[0]);
    });
}

function getAllOffsetTrails(offsetData, callback, connection) {
    const {offsetDate, limit} = offsetData

    const query = `SELECT trail.*, "user".username FROM trail JOIN "user" ON trail.user_created = "user".id
                     ${offsetDate != null ? `WHERE trail.timestamp_created < timestamp '${offsetDate}'` : ""} ORDER BY trail.timestamp_created DESC LIMIT ${limit};`;

    connection.client.query(query, [], (err, queryRes) => {
        if (err) {
            logger.error("Problem on database while getting trails: ", err);
            return callback(err);
        }

        return callback(null, queryRes.rows);
    });
}

function getTrailsCount(param, callback, connection) {
    query = "SELECT COUNT(*) FROM trail;";

    connection.client.query(query, [], (err, queryRes) => {
        if (err) {
            logger.error("Problem on database while counting trails: ", err);
            return callback(err);
        }

        return callback(null, queryRes.rows[0]);
    });
}

function getUserTrails(userID, callback, connection) {
    query = "SELECT * FROM trail WHERE trail.user_created = $1 ORDER BY trail.timestamp_created DESC;";

    connection.client.query(query, [userID], (err, queryRes) => {
        if (err) {
            logger.error(`Problem on database while getting trails for user [id = ${userID}]: `, err);
            return callback(err);
        }

        return callback(null, queryRes.rows);
    });
}

function searchTrailsByNameOrCode(searchQuery, callback, connection) {
    subQuery = "%" + searchQuery + "%";
    query = `SELECT trail.*, "user".username FROM trail JOIN "user" ON trail.user_created = "user".id 
            WHERE name ILIKE $1 OR code ILIKE $1 ORDER BY trail.timestamp_created DESC LIMIT 4;`;

    connection.client.query(query, [subQuery], (err, queryRes) => {
        if (err) {
            logger.error(`Problem on database while searching for trails with query [${searchQuery}]: `, err);
            return callback(err);
        }

        return callback(null, queryRes.rows);
    });
}

module.exports = {
    saveNewTrail,
    getTrailById,
    getTrailByCode,
    getAllOffsetTrails,
    getTrailsCount,
    getUserTrails,
    searchTrailsByNameOrCode
}
const logger = require('../../../config/logger');
const trailRepoImpl = require('./trail-repo-impl');
const db = require('../../index');

function saveNewTrail(name, code, path_to_definition, path_to_thumbnail, userID, callback, connection) {
    if (connection === undefined) {
        return getConnection(trailRepoImpl.saveNewTrail, {name, code, path_to_definition, path_to_thumbnail, userID}, callback);
    }

    trailRepoImpl.saveNewTrail({name, code, path_to_definition, path_to_thumbnail, userID}, callback, connection);
}

function getTrailById(id, callback, connection) {
    if (connection === undefined) {
        return getConnection(trailRepoImpl.getTrailById, id, callback);
    }

    trailRepoImpl.getTrailById(id, callback, connection);
}

function getTrailByCode(code, callback, connection) {
    if (connection === undefined) {
        return getConnection(trailRepoImpl.getTrailByCode, code, callback);
    }

    trailRepoImpl.getTrailByCode(id, callback, connection);
}

function getAllOffsetTrails(offsetDate, limit, callback, connection) {
    if (connection === undefined) {
        return getConnection(trailRepoImpl.getAllOffsetTrails, {offsetDate, limit}, callback);
    }

    trailRepoImpl.getAllOffsetTrails({offsetDate, limit}, callback, connection);
}

function getTrailsCount(callback, connection) {
    if (connection === undefined) {
        return getConnection(trailRepoImpl.getTrailsCount, null, callback);
    }

    trailRepoImpl.getTrailsCount(null, callback, connection);
}

function getUserTrails(userID, callback, connection) {
    if (connection === undefined) {
        return getConnection(trailRepoImpl.getUserTrails, userID, callback);
    }

    trailRepoImpl.getUserTrails(userID, callback, connection);
}

function searchTrailsByNameOrCode(searchQuery, callback, connection) {
    if (connection === undefined) {
        return getConnection(trailRepoImpl.searchTrailsByNameOrCode, searchQuery, callback);
    }

    trailRepoImpl.searchTrailsByNameOrCode(searchQuery, callback, connection);
}

function getConnection(db_function, param, callback) {
    db.getConnection(connection => {
        return db_function(param, (err, data) => {
            connection.done();
            return callback(err, data);
        }, connection);
    });
}


module.exports = {
    saveNewTrail,
    getTrailById,
    getTrailByCode,
    getAllOffsetTrails,
    getTrailsCount,
    getUserTrails,
    searchTrailsByNameOrCode
}
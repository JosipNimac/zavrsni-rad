const pg = require('pg');
const session = require('express-session');
const pgSession = require('connect-pg-simple')(session);
const logger = require('../config/logger');

const db_pool = new pg.Pool({
    host: process.env.DB_HOST,
    port: process.env.DB_PORT,
    database: process.env.DB_NAME,
    user: process.env.DB_USER,
    password: process.env.DB_PASS,
});

const pgSessionStore = new pgSession({
    pool: db_pool,
    tableName: "session"
});

// Function that will be avalilable outside of this file.
function query (queryText, params, callback) {
    // Actual call to node-postgres.
    return db_pool.query(queryText, params, callback);
};

/**
 * Function will return one connection to the database. This allows for multiple queries
 * to be executed as a transaction. User is responsible for starting the transaction and
 * at the end closing the connection.
 * TODO: Check if I should add automatic closing of the connection after significant time 
 * has passed!! 
 */
function getConnection (callback) {
    db_pool.connect((err, client, done) => {
        if (err) {
            logger.error('Problem with getting a single connection from the pool.', err);
            return callback();
        }
        
        const connection = {
            client: client,
            done: done
        };

        return callback(connection);
    });
};

function getSessionStore() {
    return pgSessionStore;
}

function rollback(connection, errMessage) {
    if (connection === undefined) {
        throw new Error("Connection must be defined in rollback function!");
    }

    connection.client.query("ROLLBACK", err => {
        if (err) {
            logger.error(errMsg, err);
        }

        connection.done();
    });
}

function commit(connection, errMessage) {
    if (connection === undefined) {
        throw new Error("Connection must be defined in commit function!");
    }

    connection.client.query("COMMIT", err => {
        if (err) {
            logger.error(errMsg, err);
        }

        connection.done();
    });
}

module.exports = {
    query,
    getConnection,
    getSessionStore,
    commit,
    rollback
};
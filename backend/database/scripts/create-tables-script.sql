CREATE TABLE "role" (
	id SERIAL primary key,
	description VARCHAR(20) NOT NULL
);

CREATE TABLE "user" (
	id SERIAL primary key,
	username VARCHAR(20) UNIQUE NOT NULL,
	password_hash TEXT NOT NULL,
	email VARCHAR(128) UNIQUE NOT NULL,
	activated boolean NOT NULL DEFAULT false,
	timestamp_created timestamp with time zone NOT NULL,
	timestamp_modified timestamp with time zone NOT NULL,
	timestamp_last_login timestamp with time zone
);

CREATE TABLE user_role (
	id SERIAL primary key,
	role_id INTEGER references "role"(id) NOT NULL,
	user_id INTEGER references "user"(id) NOT NULL
);

-- Prilikom registracije korisnik mora potvrditi mail. Ova tablica sprema podatke o potvrdi mailom.
CREATE TABLE user_token (
	id SERIAL primary key,
	token TEXT UNIQUE NOT NULL,
	user_id INTEGER references "user"(id) NOT NULL,
	is_claimed BOOLEAN NOT NULL,
	timestamp_claimed timestamp with time zone 
);

CREATE TABLE ant (
	id SERIAL primary key,
	name VARCHAR(20) NOT NULL,
	code VARCHAR(23) NOT NULL UNIQUE, -- Biti će omogućeno pretraživanje po ovome.
	representation TEXT NOT NULL,
	alg_params_id INTEGER references algorithm_params(id) NOT NULL,
	timestamp_created timestamp with time zone NOT NULL,
	timestamp_modified timestamp with time zone NOT NULL,
	user_created INTEGER references "user"(id) NOT NULL,
	user_modified INTEGER references "user"(id) NOT NULL
);

CREATE TABLE trail (
	id SERIAL primary key,
	name VARCHAR(20) NOT NULL,
	code VARCHAR(23) NOT NULL UNIQUE, -- Biti će omogućeno pretraživanje po ovome.
	path_to_definition TEXT NOT NULL, -- Put do mjesta na disku gdje je	 datoteka s cijelokupnim opisom staze
	path_to_thumbnail TEXT NOT NULL, -- Put do mjesta na disku gdje je slika staze jer bi korisnicima prilikom pretrage to moglo biti zanimljivo
	timestamp_created timestamp with time zone NOT NULL,
	timestamp_modified timestamp with time zone NOT NULL,
	user_created INTEGER references "user"(id) NOT NULL,
	user_modified INTEGER references "user"(id) NOT NULL
);

CREATE TABLE best_ant_trail (
	id SERIAL primary key,
	ant_id INTEGER references ant(id) NOT NULL,
	trail_id INTEGER references trail(id) NOT NULL,
	food_eaten INTEGER NOT NULL
);

CREATE TABLE algorithm_params (
	id SERIAL primary key,
	params json NOT NULL,
	ant_id INTEGER references ant(id) NOT NULL,  
	trail_id INTEGER references trail(id) NOT NULL -- Id staze na kojoj se mrav trenirao
);
import React from 'react';
import {Row, Col, Button} from 'reactstrap'; // Likely I will move the pagination into it's own component so some things here will be unnecessary!
import TrailCard from './TrailCard';
import axios from 'axios';
import constants from '../constants';
import Loading from '../Util/SimpleLoadingAnimation';

import './trails.css';

export default class Trails extends React.Component {

    constructor(props) {
        super(props);
        
        this.itemsPerPage = this.props.itemsPerPage || 12; // Default items per page!

        this.state = {
            trails: [],
            totalTrails: 6,
            lastSeenTrail: null,
            pageIndex: 1,
            loading: true
        }
    }

    componentDidMount() {
        axios({
            method: "POST",
            url: constants.backend + "api/trails/info/all",
            data: {
                offsetDate: null, // Because we don't have offset date of the last trail yet.
                limit: this.itemsPerPage 
            }
        }).then(res => {
            console.log(res.data);
            this.setState({
                trails: res.data.trails,
                totalTrails: parseInt(res.data.totalTrails.count),
                lastSeenTrail: res.data.trails[res.data.trails.length - 1],
                loading: false
            });
        }).catch(err => {
            // TODO: Decide how to handle this error.
        });
    }

    render() {
        return(
            <div className="content">
                <div hidden={!this.state.loading}><Loading spinnerColor="gray" spinnerType="grow"/></div>
                <div hidden={this.state.loading} className="container-full">

                    <h4 className="subtitle">Newest trails</h4>
                    
                    <Row className="row-no-margin">
                        {this.state.trails.map((trail, index)=> {
                            return getTrail(trail, index);
                        })}
                    </Row>
                    <Pagination pageIndex={this.state.pageIndex} maxPage={Math.ceil(this.state.totalTrails / this.itemsPerPage)}/>
                </div>
            </div>
        )
    }
}

function getTrail(trail, index) {
    return (
        <Col key={`trail-col-${index}`} className="col-no-padding" xs={12} sm={6} md={4} lg={3}>
            <TrailCard user={trail.username} name={trail.name} code={trail.code} thumbnailPath={trail.path_to_thumbnail} date={new Date(trail.timestamp_created).toLocaleString()}/>
        </Col>
    );
}

function Pagination(props) {
    return(
        <Row className="d-flex justify-content-center">
            <Col className={"d-flex justify-content-end"}>
                <div hidden={props.pageIndex === 1}><Button outline size="sm">Previous</Button></div>
            </Col>
            <Col xs="auto" className="d-flex align-self-center">
                <span className="framed">{props.pageIndex}/{props.maxPage}</span>
            </Col>
            <Col className="d-flex justify-content-start">
                <div hidden={props.maxPage === props.pageIndex}><Button outline size="sm">Next</Button></div>
            </Col>
        </Row>
    );
}
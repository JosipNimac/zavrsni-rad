import React from 'react';
import {Link} from 'react-router-dom';
import {Row, Col} from 'reactstrap';
import axios from 'axios';

import constants from '../constants';

import loadingPNG from '../Resources/GeneticDisplay/loadingPlaceholder.png';

// TODO: Here we can actually use alt property of img tag by a predetermined trail image.

export default class TrailCard extends React.Component {

    constructor(props) {
        super(props);

        this.state = {
            trailThumbnail: loadingPNG,
            trailImageReceived: false
        }
    }

    componentDidMount() {
        axios({
            method: "GET",
            url: constants.backend + "api/trails/thumbnail?path=" + this.props.thumbnailPath
        }).then(res => {
            this.setState({
                trailThumbnail: res.data.image,
                trailImageReceived: true
            });
        }).catch(err => {
            // TODO: Decide how to handle this.
        });
    }

    render() {
        const topRow = this.props.noUserName ? this.props.name : `${this.props.user} -- ${this.props.name}`

        return (
            <div className="trail-card">
                <Row className="card-text">
                    <Col className="text-center">{topRow}</Col>
                </Row>
                <Row className="card-text">
                    <Col className="col-no-padding">
                        <Link to={`/training/${this.props.code}`}>
                            <img className="trail-thumbnail" src={this.state.trailThumbnail} alt="Slika staze se nije uspijela učitati."/>
                        </Link>
                    </Col>
                </Row>
                <Row className="card-text">
                    <Col className="text-center">Code: {this.props.code}</Col>
                </Row>
                {this.props.date &&
                    <Row className="card-text">
                        <Col className="text-center">{this.props.date}</Col>
                    </Row>
                }
            </div>
        );
    }
}
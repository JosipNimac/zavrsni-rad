import React from 'react';
import {Row, Col, Button, Input, Collapse} from 'reactstrap';
import {FaTimes} from 'react-icons/fa';

import axios from 'axios';

import Navigation from '../Navigation/Navigation';
import Trails from './Trails';
import TrailCard from './TrailCard';
import constants from '../constants';

import "bootstrap/dist/css/bootstrap.min.css";
import './trails.css';

export default class TrailSelectIndex extends React.Component {
    
    constructor(props) {
        super(props);

        this.state = {
            searched: false,
            searchTrails: [],
            searchQuery: ""
        }
    }
    
    searchTrails = () => {
        if (this.state.searchQuery.trim().length > 0) {
            axios({
                method: "GET",
                url: constants.backend + "api/trails/search?q=" + this.state.searchQuery
            }).then(res => {
                this.setState({
                    searchTrails: res.data,
                    searched: true
                });
            }).catch(err => {
    
            });
        }
    }

    queryChanged = (event) => {
        this.setState({
            searchQuery: event.target.value
        });
    }

    onEnterSubmit = (event) => {
        if (event.charCode === 13) {
            // Char code 13 is new line (enter)
            this.searchTrails();
        }
    }

    cancelSearch = () => {
        this.setState({
            searchTrails: [],
            searched: false
        });
    }

    render() {
        return(
            <div className="container-full">
                <Navigation {...this.props}/>
                <div className="search-div">
                    <Row>
                        <Col className="col-no-padding" xs="auto">
                            <Button outline onClick={this.searchTrails}>Search</Button>
                        </Col>
                        <Col className="col-no-padding">
                            <Input className="search" placeholder="Type in trail's name or code" value={this.state.searchQuery} onChange={this.queryChanged} onKeyPress={this.onEnterSubmit}/>
                        </Col>
                    </Row>
                </div>
                <Collapse className="content searched-trails container-full" isOpen={this.state.searched}>
                    <Row className="row-no-margin top">
                        <Col xs={2}></Col>
                        <Col xs={8}>
                            <span className="subtitle"><h4>Search results</h4></span>
                        </Col>
                        <Col xs={2}><Button size="sm" onClick={this.cancelSearch}>Cancel search<FaTimes className="icon"/></Button></Col>
                    </Row>
                    <Row className="row-no-margin">
                        {this.state.searchTrails.map(trail => 
                            <Col className="col-no-padding" key={trail.code} lg={3}>
                                <TrailCard name={trail.name} user={trail.username} thumbnailPath={trail.path_to_thumbnail} code={trail.code} date={new Date(trail.timestamp_created).toLocaleString()}/>
                            </Col>
                        )}
                    </Row>
                    {this.state.searchTrails.length === 0 &&
                        <Row className="row-no-margin top">
                            <Col xs={12} className="col-no-padding">
                                <span className="subtitle"><h5>No trails found!</h5></span>
                            </Col>
                        </Row>
                    }
                </Collapse>
                <Trails/>
            </div>
        );
    }
}
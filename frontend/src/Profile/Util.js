import React from 'react';
import {Row, Col, Button} from 'reactstrap';

function Pagination(props) {
    return(
        <Row className="d-flex justify-content-center">
            <Col className={"d-flex justify-content-end"}>
                <div hidden={props.pageIndex === 1}><Button outline size="sm" onClick={props.previous}>Previous</Button></div>
            </Col>
            <Col xs="auto" className="d-flex align-self-center">
                {"Page: "}<span className="framed">{props.pageIndex}{" of "}{props.maxPage}</span>
            </Col>
            <Col className="d-flex justify-content-start">
                <div hidden={props.maxPage === props.pageIndex}><Button outline size="sm" onClick={props.next}>Next</Button></div>
            </Col>
        </Row>
    );
}

export default {
    Pagination
}
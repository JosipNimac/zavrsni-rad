import React from "react";
import {Row, Col, Button} from 'reactstrap';
import {FaPlus} from 'react-icons/fa';

import axios from 'axios';

import TrailCard from '../TrailSelection/TrailCard';
import Util from './Util';
import Loading from '../Util/SimpleLoadingAnimation';
import constants from '../constants';

export default class ProfileTrails extends React.Component {

    constructor(props) {
        super(props);

        this.state = {
            trails: [],
            page: 1,
            pageSize: 12,
            totalPages: undefined
        }
    }

    componentDidMount() {
        this.fetchTrails();
    }

    previousPage = () => {
        this.setState({
            page: this.state.page - 1
        }, this.fetchTrails);
    }

    nextPage = () => {
        this.setState({
            page: this.state.page + 1
        }, this.fetchTrails);
    }

    fetchTrails = () => {
        axios({
            method: "GET",
            url: constants.backend + `api/trails/myTrails?page=${this.state.page}&pageSize=${this.state.pageSize}`
        }).then(res => {
            this.setState({
                page: res.data.page,
                pageSize: res.data.pageSize,
                totalPages: res.data.totalPages,
                trails: res.data.trails
            });
        }).catch(err => {

        });
    }

    redirectToCreate = () => {
        this.props.history.push("/user/newMap");
    }

    render() {
        return(
            <div className="container-full trails-container">
                <Row className="d-flex justify-content-between top-row">
                    <Col xs={4} sm={3} lg={2}></Col>
                    <Col xs={4}><h5><span className="subtitle">My recent trails</span></h5></Col>
                    <Col xs={4} sm={3} lg={2}><span className="subtitle">Create new trail</span><Button className="add-btn" size="sm" color="primary" onClick={this.redirectToCreate}><FaPlus className="add-icon" /></Button></Col>
                </Row>
                <Row>
                    <Col xs={12}>
                        <div hidden={this.state.trails.length > 0}>
                            <Loading message="Loading trails"/>
                        </div>
                    </Col>
                    {this.state.trails.map(trail => 
                        <Col xs={6} sm={4} md={3} lg={2} key={trail.code}>
                            <TrailCard noUserName
                                        name={trail.name} 
                                        code={trail.code} 
                                        thumbnailPath={trail.path_to_thumbnail}/>
                        </Col>
                    )}
                </Row>
                <Row className="pagination-row">
                    <Col xs={12}><Util.Pagination pageIndex={this.state.page} maxPage={this.state.totalPages} previous={this.previousPage} next={this.nextPage}/></Col>
                </Row>
            </div>
        );
    }
}
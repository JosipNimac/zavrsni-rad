import React from "react";
import {Row, Col} from "reactstrap";

import Navigation from "../Navigation/Navigation";
import ProfileData from "./ProfileData";
import ProfileAnts from './ProfileAnts';
import ProfileTrails from './ProfileTrails';

import "./profile.css";

export default class ProfileIndex extends React.Component {

    render() {
        return (
            <div className="container-full">
                <Navigation />
                <Row>
                    <Col xs={12} md={6}><ProfileData/></Col>
                    <Col xs={12} md={6}><ProfileAnts/></Col>
                </Row>
                <Row>
                    <Col xs={12}>
                        <ProfileTrails {...this.props}/>
                    </Col>
                </Row>
            </div>
        );
    }
}
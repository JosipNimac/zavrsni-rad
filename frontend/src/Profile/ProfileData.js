import React from 'react';
import {Row, Col, Container, FormGroup, FormFeedback, Input, Button, Label, Collapse, Fade} from 'reactstrap';
import {FaEdit, FaCheck, FaTimes} from 'react-icons/fa';
import {Redirect} from 'react-router-dom';
import axios from 'axios';

import constants from "../constants";

export default class ProfilelData extends React.Component {

    constructor(props) {
        super(props);
        
        this.state = {
            username: {value: "", originalValue: "", valid:true, isEdit:false},
            email: {value: ""},
            password: {oldValue: "", value: "", repeatValue: "", isEdit: false},
            unauthorized: false
        }
    }

    componentDidMount() {
        axios({
            method: "GET",
            url: constants.backend + "api/auth/editInfo"
        }).then(res => {
            this.setState({
                username: {
                    ...this.state.username,
                    value: res.data.username,
                    originalValue: res.data.username
                },
                email: {
                    value: res.data.email
                }
            });
        }).catch(err => {
            if (!err.response) {
                return console.log("Unknown server error!");
            }

            if (err.response.status === 401) {
                this.setState({unauthorized: true});
            }

        });
    }

    usernameEdit = () => {
        this.setState({
            username: {
                ...this.state.username,
                value: this.state.username.originalValue,
                isEdit: !this.state.username.isEdit
            }
        });
    }

    usernameChanged = (event) => {
        this.setState({
            username: {
                ...this.state.username,
                value: event.target.value
            }
        });
    }

    changePassword = () => {
        this.setState({
            password: {
                ...this.state.password,
                isEdit: !this.state.password.isEdit,
            }
        });
    }

    render() {
        if (this.state.unauthorized) {
            return <Redirect to="/auth"/>
        }

        return(
            <div className="container-full data-container">
                <Container>
                    <h5><span className="subtitle">Profile data</span></h5>
                    <FormGroup row className="d-flex justify-content-start">
                        <Col xs={3}><Label className="form-text" for="username">Username</Label></Col>
                        <Col xs={4}><Input name="username" bsSize="sm"
                                value={this.state.username.value}
                                onChange={this.usernameChanged}
                                maxLength={20}
                                disabled={!this.state.username.isEdit}/></Col>
                        
                            <Col xs={5} className="col-no-padding">
                                <Fade in={!this.state.username.isEdit}>
                                    <Button hidden={this.state.username.isEdit} size="sm" onClick={this.usernameEdit}><FaEdit className="icon"/>Edit</Button>
                                </Fade>
                                <Fade in={this.state.username.isEdit}>
                                    <Button hidden={!this.state.username.isEdit} className="profile-btn" size="sm" onClick={this.confirmUsername}><FaCheck className="icon"/>Confirm</Button>
                                    <Button hidden={!this.state.username.isEdit} className="profile-btn" size="sm" onClick={this.usernameEdit}><FaTimes className="icon"/>Cancel</Button>
                                </Fade>
                            </Col>
                    </FormGroup>
                    <FormGroup row>
                        <Col xs={3}><Label className="form-text" for="email">E-mail</Label></Col>
                        <Col xs={4}><Input name="email" bsSize="sm" value={this.state.email.value} readOnly/></Col>
                    </FormGroup>
                    <Collapse isOpen={this.state.password.isEdit}>
                        <FormGroup row>
                            <Col xs={3}><Label className="form-text" for="oldPassword">Old password</Label></Col>
                            <Col xs={4}><Input id="oldPassword" name="oldPassword" bsSize="sm"/></Col>
                        </FormGroup>
                        <FormGroup row>
                            <Col xs={3}><Label className="form-text" for="password">New password</Label></Col>
                            <Col xs={4}><Input id="password" name="password" bsSize="sm"/></Col>
                        </FormGroup>
                        <FormGroup row>
                            <Col xs={3}><Label className="form-text" for="repeatPassword">Repeat password</Label></Col>
                            <Col xs={4}><Input id="repeatPassword" name="repeatPassword" bsSize="sm"/></Col>
                        </FormGroup>
                    </Collapse>
                    {!this.state.password.isEdit &&
                        <Fade in={!this.state.password.isEdit}>
                            <FormGroup className="pass-change-row" row>
                                <Col xs={12}><Button size="sm" onClick={this.changePassword}><FaEdit className="icon"/>Change Password</Button></Col>   
                            </FormGroup>
                        </Fade>
                    }
                    {this.state.password.isEdit &&
                        <Fade in={this.state.password.isEdit}>
                            <FormGroup row>
                                <Col xs={6}>
                                    <Button size="sm"><FaCheck className="icon"/>Save new password</Button>
                                </Col>
                                <Col xs={6}>
                                    <Button size="sm" onClick={this.changePassword}><FaTimes className="icon"/>Cancel</Button>
                                </Col>
                            </FormGroup>
                        </Fade>
                    }
                </Container>
            </div>
        );
    }
}
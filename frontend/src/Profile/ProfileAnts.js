import React from 'react';
import {Row, Col} from 'reactstrap';

import axios from 'axios';

import antPNG from '../Resources/GeneticDisplay/Ant_small.png';
import Loading from '../Util/SimpleLoadingAnimation';
import Util from './Util';
import constants from '../constants';

export default class ProfileAnts extends React.Component {

    constructor(props) {
        super(props);

        this.state = {
            ants: [],
            page: 1,
            pageSize: 6,
            totalPages: undefined
        }
    }

    componentDidMount() {
        this.fetchAnts();
    }

    previousPage = () => {
        this.setState({
            page: this.state.page - 1
        }, this.fetchAnts);
    }

    nextPage = () => {
        this.setState({
            page: this.state.page + 1
        }, this.fetchAnts);
    }

    fetchAnts = () => {
        axios({
            method: "GET",
            url: constants.backend + `api/ant/myAnts?page=${this.state.page}&pageSize=${this.state.pageSize}`
        }).then(res => {
            console.log(res.data);
            this.setState({
                page: res.data.page,
                pageSize: res.data.pageSize,
                totalPages: res.data.totalPages,
                ants: res.data.ants
            });
        }).catch(err => {
            console.log(err);
        });
    }

    render() {
        return (
            <div className="container-full ants-container">
                <Row>
                    <Col xs={12}>
                        <h5><span className="subtitle">My recent ants</span></h5>
                    </Col>
                </Row>
                <Row>
                    <Col xs={12}>
                        <div hidden={this.state.ants.length > 0}>
                            <Loading message="Loading ants"/>
                        </div>
                    </Col>
                    {this.state.ants.map(ant => 
                        <Col key={ant.code} xs={4}><AntCard ant={ant}/></Col>
                    )}
                </Row>
                <Row className="pagination-row">
                    <Col xs={12}><Util.Pagination pageIndex={this.state.page} maxPage={this.state.totalPages} previous={this.previousPage} next={this.nextPage}/></Col>
                </Row>
            </div>
        );
    }
}

function AntCard(props) {
    return (
        <div className="container-full ant-card">
            <Row className="row-no-margin">
                <Col className="col-no-padding">{props.ant.name}</Col>
            </Row>
            <Row className="row-no-margin">
                <Col className="col-no-padding"><img className="ant-image" src={antPNG} alt="ant"/></Col>
            </Row>
            <Row className="row-no-margin">
                <Col className="col-no-padding">{props.ant.code}</Col>
            </Row>
        </div>
    );
}

/*function AntCard(props) {
    return (
        <div className="container-full ant-card">
            <Row className="row-no-margin">
                <Col className="col-no-padding">{props.ant.name}</Col>
            </Row>
            <Row className="row-no-margin">
                <Col className="col-no-padding"><img onClick={() => props.antSelected(props.ant)} className="ant-image" src={antPNG} alt="ant"/></Col>
            </Row>
            <Row className="row-no-margin">
                <Col className="col-no-padding">{props.ant.code}</Col>
            </Row>
            <Row className="row-no-margin">
                <Col className="col-no-padding">By: {props.ant.username}</Col>
            </Row>
        </div>
    );
}*/
import React from "react";

import axios from 'axios';
import constants from '../constants';

import NodeUtils from '../GeneticProgramming/GeneticAlgorithm/Nodes/NodeUtils';
import ExecutionEngine from '../GeneticProgramming/ExecutionEngine/ExecutionEngine';

import AntAnimation from '../GeneticVisual/TrainedAnimation/AntAnimation';
import Loading from './SimpleLoadingAnimation';

export default class RandomVisualDemo extends React.Component {

    constructor(props) {
        super(props);

        this.state = {
            mapParams: null,
            antData: null,
            antLoaded: false
        }

        this.visuals = React.createRef();
    }

    componentDidMount() {
        axios({
            method: "GET",
            url: constants.backend + "api/trails/random"
        }).then(res => {
            this.setState({
                mapParams: res.data
            });

            this.engine = new ExecutionEngine(res.data.antRow,
                res.data.antColumn,
                res.data.antRotation,
                res.data.mapRows,
                res.data.mapColumns,
                res.data.foodLocations);

            this.fetchAnt(res.data.code);
        }).catch(err => {

        });
    }

    fetchAnt = (trailCode) => {
        axios({
            method: "GET",
            url: constants.backend + "api/ant/admin/" + trailCode
        }).then(res => {
            this.setState({
                antData: this.engine.getAntStates(NodeUtils.ParseShortRepresentation(res.data.description)),
                antLoaded: true
            }, this.visuals.current.resetAnimData);
        }).catch(err => {

        });
    }

    render() {
        return (
            <div>
                <div hidden={this.state.antLoaded}><Loading/></div>
                <div hidden={!this.state.antLoaded}><AntAnimation ref={this.visuals}
                              mapParams={this.state.mapParams}
                              resolution={1000}
                              solvedData={this.state.antData}
                              trainingFinished={this.state.antLoaded}/></div>
            </div>
        );
    }
}
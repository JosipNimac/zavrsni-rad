import React from 'react';
import {Spinner, Row} from 'reactstrap';

import './util.css';

export default function SimpleLoadingAnimation(props) {
    return (
        <div className="loading"> 
            <Row className="row-vertical-margin justify-content-center">
                {props.message}
            </Row>
            <Row className="row-vertifcal-margin justify-content-center">
                <Spinner color={props.spinnerColor || "gray"} type={props.spinnerType || "grow"}/>
                <Spinner color={props.spinnerColor || "gray"} type={props.spinnerType || "grow"}/>
                <Spinner color={props.spinnerColor || "gray"} type={props.spinnerType || "grow"}/>
            </Row>
        </div>
    );
}
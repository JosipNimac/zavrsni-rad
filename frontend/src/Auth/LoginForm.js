import React from 'react';
import {Alert, Col, Input, Button, FormGroup, FormFeedback, Label} from 'reactstrap';
import axios from 'axios';

import constants from '../constants';

import "bootstrap/dist/css/bootstrap.min.css";
import './auth-downloaded.scss';
import './auth.css';

export default class LoginForm extends React.Component {

    constructor(props) {
        super(props);

        this.state = {
            identificator: "",
            password: "",
            identificatorValid: true,
            passwordValid: true,
            loginFailed: false
        }
    }

    submit = () => {
        if (this.validate()) {
            axios({
                method: "POST",
                url: constants.backend + "api/auth/login",
                headers: {"Content-type": "application/json"},
                data: {
                    identificator: this.state.identificator,
                    password: this.state.password
                }
            }).then(res => {
                // TODO: redirect to trails index!
                localStorage.setItem("LoggedIn", true);
                this.props.history.push("trails");
            }).catch(err => {
                this.setState({
                    loginFailed: true
                });
            });
        }
    }

    onChange = (event) => {
        if (event.target.value.length <= 20) {
            this.setState({
                [event.target.id]: event.target.value,
                identificatorValid: true,
                passwordValid: true,
                loginFailed: false
            });
        }
    }

    onEnterSubmit = (event) => {
        if (event.charCode === 13) {
            // Char code 13 is new line (enter)
            this.submit();
        }
    }

    validate() {
        this.setState({
            identificatorValid: this.state.identificator.length > 0,
            passwordValid: this.state.password.length > 0
        });

        return this.state.identificator.length > 0 && this.state.password.length > 0;
    }

    render() {
        return (
            <div className="inner-container">
                <h3 className="auth-form-title">Login</h3>

                <Alert className="feedback-alert" color="danger" isOpen={this.state.loginFailed}>Login failed. Check your data and try again.</Alert>

                <FormGroup row className="from-row">
                    <Label for="identificator">Username or email:</Label>
                    <Input autoFocus id="identificator" type="text" bsSize="sm" value={this.state.identificator} onChange={this.onChange} invalid={!this.state.identificatorValid}/>
                    <FormFeedback className="feedback">You can't leave this field empty!</FormFeedback>
                </FormGroup>

                <FormGroup row className="from-row">
                    <Label for="password">Password:</Label>
                    <Input id="password" type="password" bsSize="sm" value={this.state.password} onChange={this.onChange} invalid={!this.state.passwordValid} onKeyPress={this.onEnterSubmit}/>
                    <FormFeedback className="feedback">You can't leave this field empty!</FormFeedback>
                </FormGroup>

                <FormGroup row className="from-row">
                    <Col><Button id="login" outline onClick={this.submit}>Login</Button></Col>
                </FormGroup>
            </div>        
        );
    }
}
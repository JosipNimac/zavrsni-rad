import React from 'react';
import {Row, Col} from 'reactstrap';
import Navigation from '../Navigation/Navigation';

import RandomAnimation from '../Util/RandomVisualDemo';

import AuthTabs from './AuthTabs';

import './auth.css';

export default class AuthAuthIndex extends React.Component {

    render() {
        return(
            <div className="container-full">
                <Navigation />
                <Row>
                    <Col className="col-no-padding test-borders" lg={5} md={6} sm={12} xs={12}>
                        <div className="visual-demo">
                            <p className="subtitle">Become a registered member and you will be able to:</p>
                            <ul>
                                <li>Make custom maps</li>
                                <li>Save your ants</li>
                            </ul>
                            <RandomAnimation />
                        </div>
                    </Col>
                    <Col className="col-no-padding test-borders" lg={7} md={6} sm={12} xs={12}>
                        <div className="auth-form-container">
                            <AuthTabs {...this.props}/>
                        </div>
                    </Col>
                </Row>
            </div>
        );
    }
}
import React from 'react';
import {Input, Button, FormGroup, Label, Col, FormFeedback, Alert} from 'reactstrap';
import axios from 'axios';
import lodash from 'lodash';
import constants from '../constants';

import "bootstrap/dist/css/bootstrap.min.css";
import './auth-downloaded.scss';
import './auth.css';

const minInputLength = 6;
const maxInputLength = 20;

export default class RegisterForm extends React.Component {

    constructor(props) {
        super(props);
        
        this.state = {
            email: {doesntExist: false, exist: false, value: "", valid: true, invalidMessage: "Email is already in use."},
            username: {doesntExist: false, exist: false, value: "", valid: true, invalidMessage: "Username is alredy in use."},
            password: {value: "", valid: true, invalidMessage: "Password and repeate password do not match."},
            repeatPassword: {value: "", valid: true},
            registerFailed: false
        }

        // Info on the last checked email and username values.
        this.lastCheck = {
            email: "",
            username: ""
        }

        this.emailRegex = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    }

    submit = (event) => {
        if (this.isValid()) {
            axios({
                method: "POST",
                url: constants.backend + "api/auth/register",
                headers: {"Content-Type": "application/json"},
                data: {
                    email: this.state.email.value,
                    username: this.state.username.value,
                    password: this.state.password.value
                }
            }).then(response => {
                this.props.registerSuccess();
            }).catch(err => {
                this.setState({
                    registerFailed: true
                });
            });
        }
    }  

    isValid() {
        let stateCopy = lodash.cloneDeep(this.state);
        let isValid = true;

        stateCopy.username.valid = stateCopy.username.value.length >= minInputLength && stateCopy.username.value.length <= maxInputLength;

        if (stateCopy.username.value.length <= minInputLength || stateCopy.username.value.length >= maxInputLength) {
            stateCopy.username.valid = false;
            stateCopy.username.invalidMessage = `Username length must be in interval [${minInputLength}-${maxInputLength}].`;
        }

        if (!this.emailRegex.test(stateCopy.email.value.toLowerCase())) {
            stateCopy.email.valid = false;
            stateCopy.email.invalidMessage = "This does not seem like an email address.";
        }

        if (stateCopy.password.value !== stateCopy.repeatPassword.value) {
            stateCopy.password.valid = false;
            stateCopy.password.invalidMessage = "Password and repeate password do not match.";
            stateCopy.repeatPassword.value = "";
        }

        if (stateCopy.password.value.length <= minInputLength || stateCopy.password.value.length >= maxInputLength) {
            stateCopy.password.valid = false;
            stateCopy.password.invalidMessage = `Password length must be in interval [${minInputLength}-${maxInputLength}].`;
            stateCopy.password.value = "";
            stateCopy.repeatPassword.value = "";
        }
        
        isValid = isValid && stateCopy.username.valid && stateCopy.email.valid && stateCopy.password.valid;

        this.setState(stateCopy);
        return isValid;
    }

    liveAvailabilityCheck = (event) => {
        // Storing event target id here because event could be released for performance reasons by the browser.
        const eventID = event.target.id;

        if (eventID === "username" && (this.state[eventID].value.length < minInputLength || this.state[eventID].value.length > maxInputLength)) {
            // Info that could be checked is not valid so there is no need to check on server.
            return;
        }

        if (eventID === "email" && !this.emailRegex.test(this.state[eventID].value.toLowerCase())) {
            // Email does not have a valid form so there is no need to check on server.
            return;
        }

        const preResponseData = this.state[eventID].value;
        if (preResponseData === this.lastCheck[eventID] || preResponseData === "") {
            // This is the same value that was already checked, so we don't need to check it again.
            // Or it is empty string, which we also don't need to check.
            return;
        }
        
        // TODO: Extract hardcoded link to backend server to constants.js
        const link = constants.backend + "api/auth/check/" + eventID + "/" + preResponseData;

        axios({
            method: "GET",
            url: link
        }).then(res => {
            // If the input was not changed while getting data from server.
            if (this.state[eventID].value === preResponseData) {
                // Store that we checked with this value.
                this.lastCheck[eventID] = preResponseData;

                this.setState({
                    [eventID]: {
                        value: this.state[eventID].value, // Value is unchanged.
                        doesntExist: !res.data.exists,
                        valid: !res.data.exists,
                        invalidMessage: "This value is already in use!"
                    },
                });
            }
        }).catch(err => {
            // If error happened we can't do live email and username availability check so we ignore it..
        });
    }

    onChange = (event) => {
        const stateCopy = lodash.cloneDeep(this.state);
        if (event.target.id === "email" || event.target.id === "username") {
            // If input changes we don't know if email or username are valid or not.
            stateCopy[event.target.id].doesntExist = false;
            stateCopy[event.target.id].exists = false;
            stateCopy[event.target.id].valid = true;
        }

        if (event.target.id === "password" || event.target.id === "username") {
            stateCopy[event.target.id].valid = true;
        }

        // Updating value stored in state.
        stateCopy[event.target.id].value = event.target.value;
        this.setState(stateCopy);
    }

    onEnterSubmit = (event) => {
        if (event.charCode === 13) {
            // Char code 13 is new line (enter)
            this.submit();
        }
    }

    render() {
        return (
            <div className="inner-container">
                <h3 className="auth-form-title">Registration</h3>

                <Col xs={12}><Alert className="feedback-alert" color="danger" isOpen={this.state.registerFailed}>Registration was not successfull</Alert></Col>
                
                <FormGroup row className="from-row">
                    <Label lg={3} className="label" for="email">Email:</Label>
                    <Col lg={9}>
                        <Input id="email" 
                               type="email" 
                               bsSize="sm"
                               autoFocus
                               value={this.state.email.value} 
                               onChange={this.onChange} 
                               onBlur={this.liveAvailabilityCheck}
                               valid={this.state.email.doesntExist}
                               invalid={!this.state.email.valid}/>
                        <FormFeedback valid className="feedback">Email is available for use!</FormFeedback>
                        <FormFeedback className="feedback">{this.state.email.invalidMessage}</FormFeedback>
                    </Col>
                </FormGroup>
                <FormGroup row className="from-row">
                    <Label lg={3} className="label" for="username">Username:</Label>
                    <Col lg={9}>
                        <Input id="username" 
                               type="text" 
                               bsSize="sm"
                               value={this.state.username.value} 
                               onChange={this.onChange} 
                               onBlur={this.liveAvailabilityCheck}
                               valid={this.state.username.doesntExist}
                               invalid={!this.state.username.valid}/>
                        <FormFeedback valid className="feedback">Username is available for use!</FormFeedback>
                        <FormFeedback className="feedback">{this.state.username.invalidMessage}</FormFeedback>
                    </Col>
                </FormGroup>
                <FormGroup row className="from-row">
                    <Label lg={3} className="label" for="password">Password:</Label>
                    <Col lg={9}>
                        <Input id="password" type="password" bsSize="sm" value={this.state.password.value} onChange={this.onChange} invalid={!this.state.password.valid}/>
                        <FormFeedback className="feedback">{this.state.password.invalidMessage}</FormFeedback>
                    </Col>
                </FormGroup>
                <FormGroup row className="from-row">
                    <Label lg={3} className="label" for="repeatPassword">Repeat password:</Label>
                    <Col lg={9}>
                        <Input id="repeatPassword" type="password" bsSize="sm" value={this.state.repeatPassword.value} onChange={this.onChange} onKeyPress={this.onEnterSubmit}/>
                    </Col>
                </FormGroup>
                <FormGroup row className="from-row">
                    <Col xs={12}>
                        <Button outline onClick={this.submit}>Register</Button>
                    </Col>
                </FormGroup>
            </div>        
        );
    }
}
import React from 'react';
import {Col, Alert} from 'reactstrap';

import "bootstrap/dist/css/bootstrap.min.css";

import LoginForm from './LoginForm';
import RegisterForm from './RegisterForm';
import FadeTransition from '../Util/FadeTransition';

import './auth-downloaded.scss';

export default class AuthTabs extends React.Component {

    constructor(props) {
        super(props);

        this.state = {
            loginOpened: true,
            registerOpened: false,
            isTransitioning: false,
            registerSuccess: false
        }

        this.tranistionTime = 180; // In milliseconds.
    }

    displayLogin = () => {
        if (this.state.isTransitioning) return;

        this.setState({
            registerOpened: false,
            isTransitioning: true
        });
        
        setTimeout(() => this.setState({
            loginOpened: true,
            isTransitioning: false
        }), this.tranistionTime * 1.4);
    }

    displayRegister = () => {
        if (this.state.isTransitioning) return;

        this.setState({
            loginOpened: false,
            isTransitioning: true
        });

        setTimeout(() => this.setState({
            registerOpened: true,
            isTransitioning: false,
            registerSuccess: false
        }), this.tranistionTime * 1.4);
    }

    registerSuccessDisplay = () => {
        this.setState({
            registerSuccess: true
        }, this.displayLogin);
    }

    render() {
        return (
            <div className="root-container">
                <div>
                    <Col xs={12}>
                        <Alert className="feedback-alert" color="success" isOpen={this.state.registerSuccess}>Registration was successfull. Please check your email and confirm your account.</Alert>
                    </Col>
                </div>

                <div className="auth-form-top">
                    <div className={"controller auth-subtitle " + (this.state.loginOpened
                        ? "selected-controller"
                        : "")} onClick={this.displayLogin}>
                        Login
                    </div>
                    <div className={"controller auth-subtitle " + (this.state.registerOpened
                        ? "selected-controller"
                        : "")} onClick={this.displayRegister}>
                        Registration
                    </div>
                </div>

                <FadeTransition isOpen={this.state.loginOpened} duration={this.tranistionTime}>
                    <div className="box-container">
                        <LoginForm {...this.props}/>
                    </div>
                </FadeTransition>

                <FadeTransition isOpen={this.state.registerOpened} duration={this.tranistionTime}>
                    <div className="box-container">
                        <RegisterForm registerSuccess={this.registerSuccessDisplay}/>
                    </div>
                </FadeTransition>
            </div>
        );
    }
}
import React from 'react';

import Constants from '../GeneticProgramming/Constants';
import {Row, Col, Button, ButtonGroup, Label} from 'reactstrap';

import "bootstrap/dist/css/bootstrap.min.css";

import './TrainedAnimation/animation-visuals.css';
import './TrailDesign/trail-design.css';

/**
 * 
 * @param {number} n number of canvas elements 
 * @param {number} resolution of the canvas elements 
 * @returns {Array.<HTMLCanvasElement>}
 */
function prepareVirtualCanvases(n, resolution) {
    let refs = [];
    for (let i = 0; i < n; i++) {
        let virtCanvas = document.createElement("canvas");
        virtCanvas.width = resolution;
        virtCanvas.height = resolution;
        refs.push(virtCanvas);
    }

    return refs;
}

function getRotationAngle(antRotation) {
    switch(antRotation) {
        case Constants.ANT_ROTATIONS.UP:
            return 0;
        case Constants.ANT_ROTATIONS.RIGHT:
            return 90 * Math.PI / 180; 
        case Constants.ANT_ROTATIONS.DOWN:
            return 180 * Math.PI / 180;
        case Constants.ANT_ROTATIONS.LEFT:
            return 270 * Math.PI / 180;
        default:
            // If we are here an error occured.
    }
}

function AnimationButtons(props) {
    const buttonText = props.paused ? "Start" : "Pause";

    return(
        <Row className="d-flex justify-content-between">
            <Col xs={2}>
                <Button size="sm" outline color="primary" onClick={props.pauseToggle}>{buttonText}</Button>
            </Col>
            <Col xs={10}>
                <div className="d-flex flex-row-reverse">
                    <ButtonGroup name="animation_buttons" className="">
                        <Button size="sm" id="1" color="primary" onClick={props.speedChanged} active={props.active === "1"}>Times 1</Button>
                        <Button size="sm" id="2" color="primary" onClick={props.speedChanged} active={props.active === "2"}>Times 2</Button>
                        <Button size="sm" id="4" color="primary" onClick={props.speedChanged} active={props.active === "4"}>Times 4</Button>
                    </ButtonGroup>
                    <Label for="animation_buttons" className="label-with-margin">
                        Animation speed:
                    </Label>
                </div>
            </Col>
        </Row>
    )
}

function ResetButton(props) {
    return (
        <Row>
            <Col>
                <Button outline color="primary" onClick={props.reset}>Reset animation</Button>
            </Col>
        </Row>
    )
}

function TrailDesignTools(props) {
    return (
        <div className="center">
            <Row className="row-no-margin padding justify-content-center flex-row">
                <Col xs={4}>
                    <ButtonGroup>
                        <Button id="Ant" color="primary" onClick={props.toolChanged} active={props.active === "Ant"}>Ant</Button>
                        <Button id="Food" color="primary" onClick={props.toolChanged} active={props.active === "Food"}>Food</Button>
                    </ButtonGroup>
                </Col>
                <Col xs={8} className="buttons-description">
                    Currently selected tool: {props.active}
                </Col>
            </Row>
        </div>
    )
}

export default {
    getRotationAngle,
    prepareVirtualCanvases,
    AnimationButtons,
    ResetButton,
    TrailDesignTools
}
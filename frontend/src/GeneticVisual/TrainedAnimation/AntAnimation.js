import React from 'react';
import {Row, Col} from 'reactstrap';
import loadash from 'lodash';
import Util from '../Util';
import AlgorithmConstants from '../../GeneticProgramming/Constants';
import Constants from '../../GeneticProgramming/Constants';

import foodPNG from '../../Resources/GeneticDisplay/Food.png';
import antPNG from '../../Resources/GeneticDisplay/Ant.png';
import backgroundPNG from '../../Resources/GeneticDisplay/woodBackground.png';

import "bootstrap/dist/css/bootstrap.min.css";

import './animation-visuals.css';

const defaulAnimInterval = 100; // Miliseconds between frames when animations speed is set to 1.

export default class AntAnimation extends React.Component {

    constructor(props) {
        super(props);

        this.canvas = React.createRef();

        const canvasRefs = Util.prepareVirtualCanvases(3, (this.props.resolution || 500));
        // Virtual canvas used for background and grid!
        this.virtualCanvas1 = canvasRefs[0];
        // Virtual canvas used for food!
        this.virtualCanvas2 = canvasRefs[1];
        // Virtual canvas used for ant!
        this.virtualCanvas3 = canvasRefs[2];

        // Setting up ant state data that should be animated.
        this.antStates = this.props.solvedData;
        this.currentDataIndex = 0;

        // Speed of the animation.
        this.state = {
            animSpeed: "1",
            animFinished: false,
            paused: true
        };
    }
    
    /*paramsSet = () => {
        // Cloning original params to mapParams to store even after currentMapParams are changed.
        // This is done to keep track of original places of food so that the user can play animation again
        // without any additional loading.
        this.mapParams = loadash.cloneDeep(this.props.mapParams);
        this.currentMapParams = loadash.cloneDeep(this.props.mapParams);

        this.drawFoods();
        
        //this.antImage.onload = () => this.drawAnt(this.mapParams.antRow, this.mapParams.antColumn, Util.getRotationAngle(this.mapParams.antRotation));
        this.drawAnt(this.mapParams.antRow, this.mapParams.antColumn, Util.getRotationAngle(this.mapParams.antRotation));
    }

    solutionSet = () => {
        this.antStates = this.props.solvedData;
    }*/

    resetAnimData = () => {
        clearInterval(this.handle);

        /*this.paramsSet();
        this.solutionSet();*/
        this.mapParams = loadash.cloneDeep(this.props.mapParams);
        this.currentMapParams = loadash.cloneDeep(this.props.mapParams);
        this.antStates = this.props.solvedData;

        this.drawFoods();
        this.drawAnt(this.mapParams.antRow, this.mapParams.antColumn, Util.getRotationAngle(this.mapParams.antRotation));

        // Here this.props.solvedData should be defined because this is on button click and button only shows up if training is done.
        this.antStates = this.props.solvedData;
        this.currentDataIndex = 0;
        this.setState({
            animFinished: false,
            paused: true
        });
    }

    componentDidMount() {
        this.backgroundImage = new Image();
        this.backgroundImage.src = backgroundPNG;
        this.backgroundImage.onload = () => this.drawBackround();
        
        this.foodImage = new Image();
        this.foodImage.src = foodPNG;
        this.foodImage.onload = () => {
            if (this.mapParams != null) this.drawFoods(); 
        };

        this.antImage = new Image();
        this.antImage.src = antPNG;
        this.antImage.onload = () => {
            if (this.mapParams != null) this.drawAnt(this.mapParams.antRow, this.mapParams.antColumn, Util.getRotationAngle(this.mapParams.antRotation));
        };
    }

    componentWillUpdate() {
        clearInterval(this.handle);
    }

    drawBackround() {
        let ctx = this.virtualCanvas1.getContext("2d");
        ctx.drawImage(this.backgroundImage, 0, 0, ctx.canvas.width, ctx.canvas.height);

        this.drawScene();
    }

    drawFoods() {
        let ctx = this.virtualCanvas2.getContext("2d");
        ctx.clearRect(0, 0, ctx.canvas.width, ctx.canvas.height);

        let rowStep = ctx.canvas.height / this.currentMapParams.mapRows;
        let colStep = ctx.canvas.width / this.currentMapParams.mapColumns;

        for (let i = 0; i < this.currentMapParams.mapRows; i++) {
            for (let j = 0; j < this.currentMapParams.mapColumns; j++) {
                if (this.currentMapParams.foodLocations[i][j] === AlgorithmConstants.MAP_DATA.FOOD) {
                    
                    ctx.drawImage(
                        this.foodImage,
                        (colStep * j) + colStep * (1 - (this.props.foodPercentage || 0.9)) / 2,
                        (rowStep * i) + rowStep * (1 - (this.props.foodPercentage || 0.9)) / 2,
                        colStep * (this.props.foodPercentage || 0.9),
                        rowStep * (this.props.foodPercentage || 0.9)
                    );
                }
            }
        }

        this.drawScene();
    }

    drawAnt(antRow, antCol, antAngle) {
        let ctx = this.virtualCanvas3.getContext("2d");
        ctx.clearRect(0, 0, this.virtualCanvas3.width, this.virtualCanvas3.height);

        let rowStep = this.virtualCanvas3.height / this.mapParams.mapRows;
        let colStep = this.virtualCanvas3.width / this.mapParams.mapColumns;
        
        ctx.save();

        ctx.translate((antCol + 0.5) * colStep, (antRow + 0.5) * rowStep);
        ctx.rotate(antAngle);

        ctx.drawImage(
            this.antImage,
            -(colStep * (this.props.antPercentage || 0.9) / 2),
            -(rowStep * (this.props.antPercentage || 0.9) / 2),
            colStep * (this.props.antPercentage || 0.9),
            rowStep * (this.props.antPercentage || 0.9)
        );

        ctx.restore();

        this.drawScene();
    }

    drawScene() {
        let ctx = this.canvas.current.getContext("2d");
        ctx.clearRect(0, 0, ctx.canvas.width, ctx.canvas.height);

        // Draw virtual canvas 1 (background and grid) to displayed canvas.
        ctx.drawImage(this.virtualCanvas1, 0, 0);
        // Draw virtual canvas 2 (food positions) to displayed canvas.
        ctx.drawImage(this.virtualCanvas2, 0, 0);
        // Draw virtual canvas 3 (ant) to displayed canvas.
        ctx.drawImage(this.virtualCanvas3, 0, 0);  
    }

    nextAnimationFrame = () => {
        if (this.currentDataIndex >= this.props.solvedData.length) {
            clearInterval(this.handle);
            this.pauseToggle();
            this.setState({
                animFinished: true
            });

            return;
        }

        let antState = this.antStates[this.currentDataIndex];
        this.drawAnt(antState.getRow(), antState.getColumn(), Util.getRotationAngle(antState.getRotation()));

        if (antState.hasEatenFood()) {
            this.currentMapParams.foodLocations[antState.getRow()][antState.getColumn()] = Constants.MAP_DATA.OTHER;
            this.drawFoods();
        }

        this.currentDataIndex++;
    }

    changeAnimationSpeed = (event) => {
        const animSpeed = event.target.id;

        this.setState({
            animSpeed: animSpeed,
        }, this.setupAnimations);
    }

    pauseToggle = () => {
        if (this.currentDataIndex === 0) this.resetAnimData();

        this.setState({
            paused: !this.state.paused
        }, this.setupAnimations);
    }

    setupAnimations() {
        clearInterval(this.handle);  
        
        if (!this.state.paused) {
            this.handle = setInterval(this.nextAnimationFrame, defaulAnimInterval / parseInt(this.state.animSpeed));
        }
    }

    render() {
        return(
            <div hidden={this.props.hidden}> 
                <Row>
                    <Col xs={12}>
                        <canvas className="my-canvas" ref={this.canvas} width={this.props.resolution || 500} height={this.props.resolution || 500}/>
                    </Col>
                </Row>
                <div hidden={!this.props.trainingFinished}>
                    {!this.state.animFinished ? (<Util.AnimationButtons 
                        active={this.state.animSpeed} 
                        paused={this.state.paused} 
                        speedChanged={this.changeAnimationSpeed} 
                        pauseToggle={this.pauseToggle}/>) : (<Util.ResetButton reset={this.resetAnimData}/>)}
                </div>
            </div>
        )
    }
}
import React from 'react';
import {Row, Col, Container} from 'reactstrap';
import { cloneDeep } from "lodash";

import "bootstrap/dist/css/bootstrap.min.css";
import "./trail-design.css";

import Constants from '../../GeneticProgramming/Constants';

import foodPNG from '../../Resources/GeneticDisplay/Food.png';
import antPNG from '../../Resources/GeneticDisplay/Ant.png';
import backgroundPNG from '../../Resources/GeneticDisplay/woodBackground.png';

import Util from '../Util';
import DesignUtil from './DesignUtil';

export default class TrailDesignVisual extends React.Component {

    constructor(props) {
        super(props);
        this.canvas = React.createRef();

        const canvasRefs = Util.prepareVirtualCanvases(3, (this.props.resolution || 500));
        // Virtual canvas used for background.
        this.virtualCanvas1 = canvasRefs[0];

        // Virtual canvas used for food and ants.
        this.virtualCanvas2 = canvasRefs[1];

        // Special virtual grid is needed because when we are generating thumbnail we don't want the gird there.
        this.virtualCanvasGrid = canvasRefs[2];

        // Currently active tool.
        this.state = {
            active: "Ant"
        }

        /*this.mapParams = {
            mapRows: this.props.mapParams.mapRows,
            mapColumns: this.props.mapParams.mapColumns
        }

        this.mapParams.foodLocations = [];
        for (let i = 0; i < this.props.mapParams.mapRows; i++) {
            let row = [];

            for(let j = 0; j < this.props.mapParams.mapColumns; j++) {
                // By default there is nothing on the map.
                row.push(Constants.MAP_DATA.OTHER);
            }

            this.mapParams.foodLocations.push(row);
        }*/
    }

    componentDidMount() {
        this.backgroundImage = new Image();
        this.backgroundImage.src = backgroundPNG;
        this.backgroundImage.onload = () => this.drawBackround();

        this.foodImage = new Image();
        this.foodImage.src = foodPNG;

        this.antImage = new Image();
        this.antImage.src = antPNG;
    }

    toolChanged = (event) => {
        this.setState({
            active: event.target.id
        });
    }

    action = (event) => {
        let mapParams = cloneDeep(this.props.mapParams);
        let pos = DesignUtil.getMousePos(this.canvas.current, event, mapParams);

        if (this.state.active === "Ant") {
            DesignUtil.handleMousePressedAntTool(event, mapParams, pos);
        } else if (this.state.active === "Food") {
            DesignUtil.handleMousePressedFoodTool(mapParams, pos);
        } else {
            // TODO: Maybe we will have somthing other than food and ant. Still thinking about it.
        }

        this.props.mapParamsChanged(mapParams, this.redrawMap);
    }

    drawBackround = () => {
        if (this.canvas.current === null) {
            return;
        }

        let ctx = this.virtualCanvas1.getContext("2d");
        ctx.drawImage(this.backgroundImage, 0, 0, ctx.canvas.width, ctx.canvas.height);
        this.drawGrid();
    }

    drawGrid = () => {
        let ctx = this.virtualCanvasGrid.getContext("2d");
        
        // Drawing rows!
        let rowStep = ctx.canvas.height / this.props.mapParams.mapRows;
        for (let i = 1; i < this.props.mapParams.mapRows; i++) {
            let height = rowStep * i;
            let start = {x: 0, y: height}
            let end = {x: this.canvas.current.width, y: height}

            ctx.beginPath();
            ctx.moveTo(start.x, start.y);
            ctx.lineTo(end.x, end.y);
            ctx.stroke();            
        }

        // Drawing cols!
        let colStep = ctx.canvas.width / this.props.mapParams.mapColumns;
        for (let j = 0; j < this.props.mapParams.mapColumns; j++) {
            let width = colStep * j;
            let start = {x: width, y: 0}
            let end = {x: width, y: this.canvas.current.height}

            ctx.beginPath();
            ctx.moveTo(start.x, start.y);
            ctx.lineTo(end.x, end.y);
            ctx.stroke();
        }

        this.drawScene();
    }

    redrawMap = () => {
        let ctx = this.virtualCanvas2.getContext("2d");
        ctx.clearRect(0, 0, ctx.canvas.width, ctx.canvas.height);

        let rowStep = ctx.canvas.height / this.props.mapParams.mapRows;
        let colStep = ctx.canvas.width / this.props.mapParams.mapColumns;

        for (let i = 0; i < this.props.mapParams.mapRows; i++) {
            for (let j = 0; j < this.props.mapParams.mapColumns; j++) {
                if (this.props.mapParams.foodLocations[i][j] === Constants.MAP_DATA.FOOD) {

                    ctx.drawImage(
                        this.foodImage,
                        (colStep * j) + colStep * (1 - (this.props.foodPercentage || 0.9)) / 2,
                        (rowStep * i) + rowStep * (1 - (this.props.foodPercentage || 0.9)) / 2,
                        colStep * (this.props.foodPercentage || 0.9),
                        rowStep * (this.props.foodPercentage || 0.9)
                    );
                }
            }
        }

        this.drawAnt(this.props.mapParams.antRow, this.props.mapParams.antColumn, Util.getRotationAngle(this.props.mapParams.antRotation));
    }

    drawAnt = (antRow, antCol, antAngle) => {
        if (antRow === undefined || antCol === undefined|| antAngle === undefined) {
            this.drawScene();
            return;
        }

        let ctx = this.virtualCanvas2.getContext("2d");

        let rowStep = ctx.canvas.height / this.props.mapParams.mapRows;
        let colStep = ctx.canvas.width / this.props.mapParams.mapColumns;
        
        ctx.save();

        ctx.translate((antCol + 0.5) * colStep, (antRow + 0.5) * rowStep);
        ctx.rotate(antAngle);

        ctx.drawImage(
            this.antImage,
            -(colStep * (this.props.antPercentage || 0.9) / 2),
            -(rowStep * (this.props.antPercentage || 0.9) / 2),
            colStep * (this.props.antPercentage || 0.9),
            rowStep * (this.props.antPercentage || 0.9)
        );
        
        ctx.restore();

        this.drawScene();
    }

    drawScene = () => {
        // Clear last frame.
        let ctx = this.canvas.current.getContext("2d");
        ctx.clearRect(0, 0, ctx.canvas.width, ctx.canvas.height);

        // Draw virtual canvases (background and grid) to displayed canvas.
        ctx.drawImage(this.virtualCanvas1, 0, 0);
        ctx.drawImage(this.virtualCanvasGrid, 0, 0);

        // Draw virtual canvas 2 (food and ant) to displayed canvas.
        ctx.drawImage(this.virtualCanvas2, 0, 0);
    }

    generateThumbnail = (width, height, format) => {
        var resizedCanvas = document.createElement("canvas");
        var resizedContext = resizedCanvas.getContext("2d");

        resizedCanvas.height = width;
        resizedCanvas.width = height;

        resizedContext.drawImage(this.virtualCanvas1, 0, 0, width, height);
        resizedContext.drawImage(this.virtualCanvas2, 0, 0, width, height);
        
        let imageObject = new Image();
        imageObject.src = resizedCanvas.toDataURL(format || "image/png");

        return imageObject;
    }

    render() {
        return(
            <Container className="container-full">
                <Row>
                    <Col xs={12} className="center">
                        <canvas className="canvas"
                            ref={this.canvas} 
                            width={this.props.resolution || 500} 
                            height={this.props.resolution || 500}
                            onClick={this.action}/>
                    </Col>
                </Row>
                <Row>
                    <Col xs={12} className="center">
                        <Util.TrailDesignTools className="my-text" toolChanged={this.toolChanged} active={this.state.active}/>
                    </Col>
                </Row>
            </Container>
        )
    }
}
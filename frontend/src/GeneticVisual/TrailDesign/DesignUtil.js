import Constants from '../../GeneticProgramming/Constants';

function getMousePos(canvas, evt, mapParams) {
    let rect = canvas.getBoundingClientRect(); // abs. size of element
    let scaleX = canvas.width / rect.width;  // relationship bitmap vs. element for X
    let scaleY = canvas.height / rect.height;  // relationship bitmap vs. element for Y
    
    // NOTE: I don't know where the '1.025' comes from, but it fixes the math!
    // scaleX and scaleY seem to be a bit off. That makes x and y sometimes one less then they should be.
    return {
        y: Math.floor(((evt.clientX - rect.left) * scaleX * 1.025) / mapParams.mapColumns),   // scale mouse coordinates after they have
        x: Math.floor(((evt.clientY - rect.top) * scaleY * 1.025) / mapParams.mapRows)   // been adjusted to be relative to element and turining it to grid coordinates
    }
}

function handleMousePressedAntTool(evt, mapParams, mousePos) {
    if (!evt.shiftKey) {
        if (mapParams.antRow === mousePos.x && mapParams.antColumn === mousePos.y) {
            // Rotate the ant!
            if (mapParams.antRotation === Constants.ANT_ROTATIONS.UP) {
                mapParams.antRotation = Constants.ANT_ROTATIONS.RIGHT;
            } else if (mapParams.antRotation === Constants.ANT_ROTATIONS.RIGHT) {
                mapParams.antRotation = Constants.ANT_ROTATIONS.DOWN;
            } else if (mapParams.antRotation === Constants.ANT_ROTATIONS.DOWN) {
                mapParams.antRotation = Constants.ANT_ROTATIONS.LEFT;
            } else if (mapParams.antRotation === Constants.ANT_ROTATIONS.LEFT) {
                mapParams.antRotation = Constants.ANT_ROTATIONS.UP;
            }
        } else {
            mapParams.antRow = mousePos.x;
            mapParams.antColumn = mousePos.y;
            mapParams.antRotation = Constants.ANT_ROTATIONS.UP;
            if (mapParams.foodLocations[mousePos.x][mousePos.y] === Constants.MAP_DATA.FOOD) {
                mapParams.foodPieces--;
                mapParams.foodLocations[mousePos.x][mousePos.y] = Constants.MAP_DATA.OTHER;
            }
        }

    } else {
        if (mapParams.antRow === mousePos.x && mapParams.antColumn === mousePos.y) {
            // User held shift and pressed cell that the ant is already on! That means remove the ant!
            mapParams.antRow = null;
            mapParams.antColumn = null;
            mapParams.antRotation = null;
        }
    }
}

function handleMousePressedFoodTool(mapParams, mousePos) {
    if (mapParams.foodLocations[mousePos.x][mousePos.y] !== Constants.MAP_DATA.FOOD) {
        if (mapParams.antRow === mousePos.x && mapParams.antColumn === mousePos.y) {
            mapParams.antRow = null;
            mapParams.antColumn = null;
            mapParams.antRotation = null;
        }

        mapParams.foodLocations[mousePos.x][mousePos.y] = Constants.MAP_DATA.FOOD;
        mapParams.foodPieces++;
    } else {
        mapParams.foodLocations[mousePos.x][mousePos.y] = Constants.MAP_DATA.OTHER;
        mapParams.foodPieces--;
    }
}

export default {
    getMousePos,
    handleMousePressedAntTool,
    handleMousePressedFoodTool
}
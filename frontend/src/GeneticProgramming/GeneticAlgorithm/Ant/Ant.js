import NodeUtils from '../Nodes/NodeUtils';

export default class Ant {
    constructor(rootNode, parentFoodEaten) {
        this.rootNode = rootNode;
        this.parentFoodEaten = parentFoodEaten; // This way it will be possible to 'punish' ants that copy their parents.

        this.fitness = -1; // Negative value so it is easier to spot possible errors later.
    }

    /**
     * @returns {GenericTreeNode} root node of this ant
     */
    getRoot() {
        return this.rootNode;
    }

    setRoot(root) {
        this.rootNode = root;
    }

    getParentFoodEaten() {
        return this.parentFoodEaten;
    }

    getFoodEaten() {
        return this.foodEaten;
    }

    setFoodEaten(foodEaten) {
        this.foodEaten = foodEaten;
    }

    getFitness() {
        return this.fitness;
    }

    setFitness(fitness) {
        this.fitness = fitness;
    }

    clone() {
        return new Ant(this.rootNode.clone(), this.foodEaten);
    }

    toString() {
        return NodeUtils.GetTreeAsString(this.rootNode);
    }

    toShortString() {
        return NodeUtils.GetShortRepresentation(this.rootNode);
    }
}
import NodeUtils from '../Nodes/NodeUtils';
import Ant from '../Ant/Ant';
import Constants from '../../Constants';

/**
 * Function will check if parameters given were valid.
 * 
 * @param {any} popInitParams 
 * @returns {booelan} true if parameters are valid, false otherwise
 */
function paramsValid(popInitParams) {
    popInitParams.popSize = typeof popInitParams.popSize === 'string' ? parseInt(popInitParams.popSize) : popInitParams.popSize;
    popInitParams.desiredDepth = typeof popInitParams.desiredDepth === 'string' ? parseInt(popInitParams.desiredDepth) : popInitParams.desiredDepth;
    popInitParams.maxNodes = typeof popInitParams.maxNodes === 'string' ? parseInt(popInitParams.maxNodes) : popInitParams.maxNodes;

    let {popSize, method, desiredDepth, maxNodes} = popInitParams;

    if (typeof popSize === 'string' || typeof desiredDepth === 'string' || typeof maxNodes === 'string') {
        return false;
    }

    if (!popSize || !method || !desiredDepth || !maxNodes) {
        // Not all necessary parameters were given.
        return false;
    }

    if (popSize < 10 || popSize > 6000) { 
        // Unrealistic populations size.
        return false;
    }

    if (desiredDepth < 2 || desiredDepth > 200) {
        // Unrealistic desired depth.
        return false;
    }

    if (maxNodes < 1 || maxNodes > 4000) {
        // Unrealistic max number of nodes.
        return false;
    }

    if (method !== Constants.TREE_GENERATION_METHODS.FULL && method !== Constants.TREE_GENERATION_METHODS.GROW && method !== Constants.TREE_GENERATION_METHODS.HALF_AND_HALF) {
        // Unknown method requested.
        return false;
    }

    return true;
}

/**
 * @returns {Array.<Ant>} population of ants
 */
export default function Initialize(popInitParams) {

    if (!paramsValid(popInitParams)) {
        // TODO: Log an error here.
        throw new Error("Invalid parameters for population initialization given.");
    }

    let {popSize, method, desiredDepth, maxNodes} = popInitParams;
    const minDepth = 2;

    let population = [];

    for (let i = minDepth; i <= desiredDepth; i++) {
        for (let j = 0; j < popSize / (desiredDepth - 1); j++) {
            
            let methodFull;
            if (method === Constants.TREE_GENERATION_METHODS.FULL) {
                methodFull = true;
            } else if (method === Constants.TREE_GENERATION_METHODS.GROW) {
                methodFull = false;
            } else if (method === Constants.TREE_GENERATION_METHODS.HALF_AND_HALF) {
                methodFull = j > (popSize / (desiredDepth - 1)) / 2;
            } else {
                // Should not happen because parameters check exists but...
                throw new Error(`Error with the given method: ${method}`);
            }

            let rootNode = NodeUtils.GenerateTreeWithRoot(desiredDepth, maxNodes, methodFull);
            population.push(new Ant(rootNode));
        }
    }

    return population;
}
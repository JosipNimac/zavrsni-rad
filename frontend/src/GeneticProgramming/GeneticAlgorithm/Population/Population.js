import PopulationInitializer from './PopulationInitializer';
import SimpleEvaluator from '../Operators/Evaluators/SimpleEvaluator';
import nTournamentSelection from '../Operators/Selections/nTournamentSelection';
import SimpleMutation from '../Operators/Mutations/SimpleMutation';
import OnePointCrossover from '../Operators/Crossovers/OnePointCrossover';

export default class Population {
    constructor(populationParams, engine) {
        this.engine = engine;

        this.population = PopulationInitializer(populationParams.popInitParams);
        this.popSize = this.population.length;
        this.generations = typeof populationParams.generations === 'string' ? parseFloat(populationParams.generations): populationParams.generations;
        this.reproductionRate = typeof populationParams.reproductionRate === 'string' ? parseFloat(populationParams.reproductionRate) : populationParams.reproductionRate;
        this.mutationRate = typeof populationParams.mutationRate === 'string' ? parseFloat(populationParams.mutationRate) : populationParams.mutationRate;
        this.crossoverRate = typeof populationParams.crossoverRate === 'string' ? parseFloat(populationParams.crossoverRate) : populationParams.crossoverRate;

        this.elitist = populationParams.elitist;
        this.maxFitness = typeof populationParams.maxFitness === 'string' ? parseFloat(populationParams.maxFitness) : populationParams.maxFitness;
        this.maxGensNoProgress = typeof populationParams.maxGensNoProgress === 'string' ? parseInt(populationParams.maxGensNoProgress) : populationParams.maxGensNoProgress;
        this.mutationInjection = populationParams.mutationInjection;

        // Function used to compare to ants for sorting the population.
        // Extracted here so that function object is only created once.
        this.compareFunction = (ant1, ant2) => {
            // Not using functions here to increase performance
            return ant2.fitness - ant1.fitness;
        }

        // TODO: It will maybe be possible to add different implementations of operators later.
        // TODO: Think about sending the approprate populationParams.operators - object so that implementations can make sure the parameters for them are valid.
        this.evaluator = new SimpleEvaluator(engine, populationParams.operators.evaluator.plagirismFactor);
        this.selection = new nTournamentSelection(populationParams.operators.selection.sizeOfTournament);
        this.mutator = new SimpleMutation(populationParams.operators.mutation.maxDepth, populationParams.operators.mutation.maxNodes);
        this.crossover = new OnePointCrossover(populationParams.operators.crossover.maxNodes, populationParams.operators.crossover.maxEffort || 5);
    
        this.currentGeneration = 0;

        this.gensWithNoProgress = 0;
        this.maxCurrentFitness = 0;
        this.bestAnt = this.population[0];
        this.evaluatePopulation();
    }

    runAllGenerations(callback) {
        let gensWithNoProgress = 0;
        let maxFitness = 0;

        this.bestAnt = this.population[0];

        for (let i = 0; i < this.generations; i++) {
            this.evaluatePopulation();

            if (!this.bestAnt.parentFoodEaten || this.population[0].foodEaten > this.bestAnt.parentFoodEaten) {
                this.bestAnt = this.population[0].clone();
            }

            gensWithNoProgress = maxFitness >= this.population[0].getFitness() ? gensWithNoProgress + 1 : 0;
            maxFitness = this.population[0].getFitness();

            if (this.mutationInjection && gensWithNoProgress === Math.floor(this.maxGensNoProgress * 3 / 4)) {
                this.activateMutationInjection(8);
                continue;
            }

            if (gensWithNoProgress >= this.maxGensNoProgress || maxFitness === this.maxFitness) {
                break;
            }

            this.runNextGeneration();
        }

        this.evaluatePopulation();
    }

    runNextGeneration() {
        if (this.currentGeneration >= this.generations) {
            // TODO: Log that function was called after all generations were finished. No need to throw error, just return from function. 
            return;
        }

        let newGeneration = [];
        if (this.elitist) {
            // If algorithm is elitist we save the best ant from previous generation.
            newGeneration.push(this.population[0]);
        }

        while(newGeneration.length < this.popSize) {
            //this.putAntForNextGeneration(newGeneration);

            let randomNum = Math.random() * (this.reproductionRate + this.mutationRate + this.crossoverRate);

            if (randomNum < this.reproductionRate) {
                // Reproduction was selected.
                // We can use unchanged reference to parent because parent reference should not change during creating of new generation.
                newGeneration.push(this.selection.select(this.population));
            } else if (randomNum < this.reproductionRate + this.mutationRate) {
                // Mutation was selected.
                newGeneration.push(this.mutator.mutate(this.selection.select(this.population).clone()));
            } else if (randomNum < this.reproductionRate + this.mutationRate + this.crossoverRate) {
                // Crossover was selected.
                let parent1Clone = this.selection.select(this.population).clone();
                let parent2Clone = this.selection.select(this.population).clone();

                let err = this.crossover.cross(parent1Clone, parent2Clone);
                if (err) {
                    newGeneration.push(parent1Clone);
                    newGeneration.push(parent2Clone);
                    continue;
                }

                newGeneration.push(parent1Clone);
                newGeneration.push(parent2Clone);
            } else {
                throw new Error(`Random number generated in 'getAntForNextGeneration' is out of allowed range. 
                        Allowed:${this.reproductionRate + this.mutationRate + this.crossoverRate}, Generated: ${randomNum}`);
            }
        }

        this.population = newGeneration;
        this.evaluatePopulation();
        this.currentGeneration++;
    }

    getCurrentStateData() {
        return {
            currentGen: this.currentGeneration, 
            totalGens: this.generations, 
            bestFood: this.population[0].foodEaten,
            popSize: this.population.length
        };
    }

    /**
     * 
     * @param {*} callback
     * @returns {Promise}
     */
    runNextGeneration2() {
        if (!this.bestAnt.parentFoodEaten || this.population[0].foodEaten > this.bestAnt.parentFoodEaten) {
            this.bestAnt = this.population[0].clone();
        }

        this.gensWithNoProgress = this.maxCurrentFitness >= this.population[0].getFitness() ? this.gensWithNoProgress + 1 : 0;
        this.maxCurrentFitness = this.population[0].getFitness();
        
        if (this.mutationInjection && this.gensWithNoProgress === Math.floor(this.maxGensNoProgress * 3 / 4)) {
            this.activateMutationInjection(8);
            this.evaluatePopulation();
            return {currentGen: this.currentGeneration + 1, totalGens: this.generations, bestFood: this.population[0].getFoodEaten()};
        }

        if (this.gensWithNoProgress >= this.maxGensNoProgress || this.maxCurrentFitness === this.maxFitness) {
            this.currentGeneration++;
            return {currentGen: this.currentGeneration + 1, totalGens: this.generations, bestFood: this.population[0].getFoodEaten()};
        }

        this.runNextGeneration();
        this.evaluatePopulation();

        return {currentGen: this.currentGeneration + 1, totalGens: this.generations, bestFood: this.population[0].getFoodEaten()};
    }

    hasNextGen() {
        return this.currentGeneration <= this.generations - 1 && this.gensWithNoProgress < this.maxGensNoProgress && this.maxCurrentFitness < this.maxFitness;
    }

    /**
     * 
     * @param {Array} newGeneration 
     */
    putAntForNextGeneration(newGeneration) {
        let randomNum = Math.random() * (this.reproductionRate + this.mutationRate + this.crossoverRate);

        if (randomNum < this.reproductionRate) {
            // Reproduction was selected.
            // We can use unchanged reference to parent because parent reference should not change during creating of new generation.
            newGeneration.push(this.selection.select(this.population));
        } else if (randomNum < this.reproductionRate + this.mutationRate) {
            // Mutation was selected.
            newGeneration.push(this.mutator.mutate(this.selection.select(this.population).clone()));
        } else if (randomNum < this.reproductionRate + this.mutationRate + this.crossoverRate) {
            // Crossover was selected.
            let parent1Clone = this.selection.select(this.population).clone();
            let parent2Clone = this.selection.select(this.population).clone();

            let err = this.crossover.cross(parent1Clone, parent2Clone);
            if (err) {
                newGeneration.push(parent1Clone);
                newGeneration.push(this.mutator.mutate(parent2Clone));
                return;
            }

            newGeneration.push(parent1Clone);
            newGeneration.push(parent2Clone);
        } else {
            throw new Error(`Random number generated in 'getAntForNextGeneration' is out of allowed range. 
                    Allowed:${this.reproductionRate + this.mutationRate + this.crossoverRate}, Generated: ${randomNum}`);
        }
    }

    activateMutationInjection(mutationIntensity) {
        for (let i = 0; i < this.population.length; i++) {
            for (let j = 0; j < mutationIntensity; j++) {
                this.mutator.mutate(this.population[i]);
            }
        }
    }

    evaluatePopulation() {
        for (let i = 0; i < this.population.length; i++) {
            this.evaluator.evaluate(this.population[i]);
        }

        this.population.sort(this.compareFunction);
    }

    getEntirePopulation() {
        return this.population;
    }

    getBestAnt() {
        return this.bestAnt;
    }
}
export default class GenericTreeNode {
    constructor(depth, parent, nodeCount, numOfChildren) {
        this.depth = depth;
        this.parent = parent;
        this.nodeCount = nodeCount ? nodeCount : 1;
        this.numOfChildren = numOfChildren ? numOfChildren : 0;
    }

    /**
     * 
     * @param {ExecutionEngine} exe engine that can be used to execute nodes.
     */
    execute(exe) {
        throw new Error("Cannot call 'execute' on GenericTreeNode");
    }

    clone() {
        throw new Error("Cannot call 'clone' on GenericTreeNode");
    }

    getSupposedChildren() {
        return this.numOfChildren;
    }

    getChild(index) {
        if (index >= this.numOfChildren || !this.children || index >= this.children.length) {
            throw new Error(`Illegal index [${index}] in 'getChild' for node: [${this.getName()}]`);
        }

        return this.children[index];
    }

    addChild(child) {
        if (!this.children || this.numOfChildren <= this.children.length) {
            throw new Error(`Adding children to node that cannot have another child. Node: ${this.getName()}, children: ${this.numOfChildren}.`);
        }

        this.children[this.children.length] = child;
    }

    setChild(child, index) {
        if (index >= this.numOfChildren || !this.children || index >= this.children.length) {
            throw new Error(`Illegal index [${index}] in 'setChild' for node: [${this.getName()}]`);
        }

        this.children[index] = child;
    }

    isTerminal() {
        // Only and all terminal nodes have children.  
        return this.numOfChildren === 0;
    }

    recalculateNodeCount() {
        if (this.isTerminal()) {
            this.nodeCount = 1;
        } else {
            // We count ourselves.
            let nodeCount = 1;

            for (let i = 0; i < this.children.length; i++) {
                this.children[i].recalculateNodeCount();
                nodeCount += this.children[i].nodeCount;
            }

            this.nodeCount = nodeCount;
        }
    }

    recalculateDepth() {
        // If parent exists this node is one depth more than parent, otherwise this node is root with depth 0.
        this.depth = this.parent ? this.parent.depth + 1 : 0;

        // If this node is not terminal, it's children should also recalculate depth.
        if (!this.isTerminal()) {
            for (let i = 0; i < this.children.length; i++) {
                this.children[i].recalculateDepth();
            }
        }
    }

    getDepth() {
        return this.depth;
    }

    setDepth(depth) {
        this.depth = depth;
    }

    /**
     * @returns {GenericTreeNode} parent of this node
     */
    getParent() {
        return this.parent;
    }

    setParent(parent) {
        this.parent = parent;
    }

    getNodeCount() {
        return this.nodeCount;
    }

    setNodeCount(nodeCount) {
        this.nodeCount = nodeCount;
    }

    getName() {
        return "GenericTreeNode";
    }

    getShortName() {
        return "G";
    }
}
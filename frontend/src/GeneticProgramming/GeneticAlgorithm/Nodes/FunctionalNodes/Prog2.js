import GenericTreeNode from '../GenericTreeNode';

export default class Prog2 extends GenericTreeNode {
    constructor(depth, parent, nodeCount) {
        super(depth, parent, nodeCount, 2);
        this.children = [];
    }

    /**
     * 
     * @param {ExecutionState} exe 
     */
    execute(exe) {
        if (super.getSupposedChildren() !== this.children.length) {
            // TODO: Execute called before children were set up. Should do some error handling here.
            console.log("Error: execute called before children were set.");
            return;
        }

        if (exe.getNumberOfAvailableActions() < 1) {
            return;
        }

        this.children[0].execute(exe);
        this.children[1].execute(exe);
    }

    clone(parent) {
        let clone = new Prog2(super.getDepth(), parent, super.getNodeCount(), super.getSupposedChildren());

        for (let i = 0; i < this.children.length; i++) {
            clone.addChild(this.children[i].clone(clone));
        }

        return clone;
    }

    getName() {
        return "Prog2";
    }

    getShortName() {
        return "2";
    }
}
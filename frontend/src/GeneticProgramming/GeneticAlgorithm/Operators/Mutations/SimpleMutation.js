import NodeUtils from '../../Nodes/NodeUtils';

export default class SimpleMutation {
    constructor(maxDepth, maxNodes) {
        this.maxDepth = typeof maxDepth === 'string' ? parseInt(maxDepth) : maxDepth;
        this.maxNodes = typeof maxNodes === 'string' ? parseInt(maxNodes) : maxNodes;
    }

    /**
     * Method will mutate the node tree of the ant given. Method will not do any cloning of the ant, developer should
     * make sure that reference to an ant given can be changed.
     * 
     * @param {Ant} ant 
     */
    mutate(ant) {

        let rootNode = ant.getRoot();

        let randomNode;
        do {
            // We want to select functional node because that is more likely to generate different subtree.
            randomNode = NodeUtils.GetRadnomNodeInTree(rootNode);
        } while(randomNode.nodeCount < 5 && rootNode.nodeCount > 20);

        if (randomNode === rootNode) {
            const newRoot = NodeUtils.GenerateTreeWithRoot(this.maxDepth, this.maxNodes, false);
            ant.setRoot(newRoot);
            return ant;
        }

        const randomNodeParent = randomNode.getParent();
        if (!randomNodeParent) {
            throw new Error("Error while mutating ant: Node selected is not root but has no parent.");
        }

        let childIndex = -1; // We are searching our position is parents children.
        for (let i = 0, children = randomNodeParent.getSupposedChildren(); i < children; i++) {
            if (randomNode === randomNodeParent.getChild(i)) {
                childIndex = i;
            }
        }

        if (childIndex === -1) {
            throw new Error("Error while mutating ant: Node selected is not a child of it's parent.");
        }

        const mutatedSubtree = NodeUtils.GenerateTree(this.maxDepth - randomNodeParent.getDepth(), this.maxNodes - randomNode.getNodeCount(), false, randomNode.getDepth());
        mutatedSubtree.setParent(randomNodeParent);
        
        // Here we are changing the reference of nodes so we do not need to create a new ant.
        randomNodeParent.setChild(mutatedSubtree, childIndex);
        // We need to recalculate node counts because our mutation could have changed some of those values.
        ant.getRoot().recalculateNodeCount();

        return ant;
    }
}
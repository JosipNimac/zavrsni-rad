import NodeUtils from '../../Nodes/NodeUtils';


export default class OnePointCrossover {
    constructor(maxNodes, maxEffort, maxDepth) {
        this.maxNodes = typeof maxNodes === 'string' ? parseInt(maxNodes) : maxNodes;
        this.maxEffort = typeof maxEffort === 'string' ? parseInt(maxEffort) : maxEffort;
        this.maxDepth = typeof maxDepth === 'string' ? parseInt(maxDepth) : maxDepth;
    }

    /**
     * Mehod will perform one point crossover on given parents. Method will change trees that parent1 and parent2 objects refernce
     * so the developer should be prepared for referenced objects to change.
     * 
     * @param {Ant} parent1 
     * @param {Ant} parent2 
     */
    cross(parent1, parent2) {
        let effort = 0;

        do {
            effort++;
            let numEffort = 0;
            
            // Random indexes of subtrees different than root. TODO: Try to find a better way to do this. Focus on getting subtrees of same size.
            let subTreeIndex1 = Math.floor(Math.random() * (parent1.getRoot().nodeCount - 1) + 1);
            let subTree1 = NodeUtils.GetNodeAtIndex(parent1.getRoot(), subTreeIndex1);
            let subTreeIndex2;
            let subTree2;
            do {
                numEffort++;
                subTreeIndex2 = Math.floor(Math.random() * (parent2.getRoot().nodeCount - 1) + 1);
                subTree2 = NodeUtils.GetNodeAtIndex(parent2.getRoot(), subTreeIndex2);
            } while(Math.abs(subTree2.getNodeCount() - subTree1.nodeCount) > 10 && numEffort < this.maxEffort);

            if (numEffort >= this.maxEffort) {
                continue;
            }

            // TODO: Allow for small changes of max nodes!
            if (parent1.getRoot().nodeCount - subTree1.nodeCount + subTree2.nodeCount > this.maxNodes || 
                    parent2.getRoot().nodeCount - subTree2.nodeCount + subTree1.nodeCount > this.maxNodes) {
    
                //if (effort > this.maxEffort) {
                //    break;
                //}
                // Crossover would be over max nodes.
                continue;
            }

            for (let i = 0, size = subTree1.getParent().getSupposedChildren(); i < size; i++) {
                if (subTree1 === subTree1.getParent().getChild(i)) {
                    let subTree2Copy = subTree2.clone();
                    subTree2Copy.setParent(subTree1.getParent());
                    subTree1.getParent().setChild(subTree2Copy, i);
                }
            }

            for (let i = 0, size = subTree2.getParent().getSupposedChildren(); i < size; i++) {
                if (subTree2 === subTree2.getParent().getChild(i)) {
                    let subTree1Copy = subTree1.clone();
                    subTree1Copy.setParent(subTree2.getParent());
                    subTree2.getParent().setChild(subTree1Copy, i);
                }
            }

            parent1.getRoot().recalculateDepth();
            parent1.getRoot().recalculateNodeCount();
            
            parent2.getRoot().recalculateDepth();
            parent2.getRoot().recalculateNodeCount();
            
            return;
        } while(effort < this.maxEffort);
    }
}
import Constants from './Constants';
import ExecutionEngine from './ExecutionEngine/ExecutionEngine';
import Population from './GeneticAlgorithm/Population/Population';

export default class GeneticRunner {

    constructor(params) {

        let mapParams = prepareMapParams(params.mapParams);
        this.engine = new ExecutionEngine(
            mapParams.antRow,
            mapParams.antColumn,
            mapParams.antRotation,
            mapParams.mapRows, 
            mapParams.mapColumns,
            mapParams.foodLocations
        );
        
        params.algorithmParams.maxFitness = countFoodOnMap(mapParams.foodLocations);

        this.population = new Population(params.algorithmParams, this.engine);
    }

    async startTraining(callback) {
        this.population.runAllGenerations(callback);
    }
    
    async startTraining2(callback) {
        let val = null;
        for (let i = 0; i < this.population.generations; i++) {
            if (val != null) {
                callback(val);
            }
            val = await this.population.runNextGeneration2();
            //console.log(JSON.stringify(val));
        }
        
        return Promise.resolve({done: true});
    }

    getNextGen(callback) {
        if (this.population.hasNextGen()) {
            callback(this.population.runNextGeneration2());
        } else {
            console.log("ERROR!!!");
        }
    }

    hasNextGen() {
        return this.population.hasNextGen();
    }
    
    startTraining3(callback) {
        for (let i = 0; i < this.population.generations; i++) {
            this.population.runNextGeneration2().then(data => {
                callback(data);
            });
        }
    }
    

    /**
     * @returns {Array.<AntState>}
     */
    getBestAntStates() {
        return this.engine.getAntStates(this.population.getBestAnt());
    }

    getBestAntRepresentation() {
        return this.population.getBestAnt().toShortString();
    }
}

function prepareMapParams(mapParams) {
    const {antRow, antColumn, antRotation} = mapParams;

    if (antRow === undefined || 
        antColumn === undefined || 
        !antRotation) {
        
            throw new Error(JSON.stringify(mapParams));
    }

    let preparedParams = {
        antRow: antRow,
        antColumn: antColumn,
        antRotation: antRotation
    }

    if (mapParams.fromFile) {
        throw new Error("No food locations provided");
        

    } else {
        if (!mapParams.foodLocations) {
            throw new Error("No food locations provided");
        }
        
        preparedParams.mapRows = mapParams.foodLocations.length;
        preparedParams.mapColumns = mapParams.foodLocations[0].length;
        preparedParams.foodLocations = mapParams.foodLocations;
    }

    return preparedParams;
}

/**
 * 
 * @param {Array[]} foodLocations 
 */
function countFoodOnMap(foodLocations) {
    let amountOfFood = 0;

    for (let i = 0; i < foodLocations.length; i++) {
        for (let j = 0; j < foodLocations[i].length; j++) {
            if (foodLocations[i][j] === Constants.MAP_DATA.FOOD) {
                amountOfFood++;
            }
        }
    }

    return amountOfFood;
}
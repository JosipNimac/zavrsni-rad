import ExecutionState from './ExecutionState';
import Constants from '../Constants';

export default class ExecutionEngine {
    constructor(startingAntRow, startingAntColumn, startingAntRotation, mapRows, mapColumns, foodLocations) {
        this.startingAntRow = startingAntRow;
        this.startingAntColumn = startingAntColumn;
        this.startingAntRotation = startingAntRotation;
        this.mapRows = mapRows;
        this.mapColumns = mapColumns;
        this.foodLocations = foodLocations;
    }

    /**
     * 
     * @param {boolean} saveStates should engine remeber positions that the ant takes in next execution. 
     */
    refreshExecutionState(saveStates) {
        // Using copy of food locations here because ExecutionState might write on the foodLocations given to it.
        let foodLocationsCopy = [];
        for (let i = 0; i < this.foodLocations.length; i++) {
            foodLocationsCopy.push([]);
            for (let j = 0; j < this.foodLocations[i].length; j++) {
                foodLocationsCopy[i][j] = this.foodLocations[i][j];
            }
        }

        this.executionState = new ExecutionState(
            this.startingAntRow,
            this.startingAntColumn,
            this.startingAntRotation,
            this.mapRows,
            this.mapColumns,
            foodLocationsCopy,
            Constants.MOVES_PER_EXECUTION,
            saveStates
        );
    }

    getCurrentExecutionState() {
        return this.executionState;
    }


    getAntStates(ant) {
        this.refreshExecutionState(true);

        do {
            ant.getRoot().execute(this.executionState);
        } while(this.executionState.getNumberOfAvailableActions() > 0);
 
        return this.executionState.getAllAntStates();
    }
}


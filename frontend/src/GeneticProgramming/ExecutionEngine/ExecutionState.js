import Constants from '../Constants';
import AntState from '../GeneticAlgorithm/Ant/AntState';

export default class ExecutionState {
    constructor(antRowPosition, antColumnPosition, antRotation, mapRows, mapColumns, foodLocations, movesAllowed, saveAntStates) {
        this.foodCollected = 0; // Initialy ant ate 0 food pieces.
        this.movesAllowed = movesAllowed;

        this.antRowPosition = antRowPosition;
        this.antColumnPosition = antColumnPosition;
        this.antRotation = antRotation;

        this.shouldSaveStates = saveAntStates;
        if (saveAntStates) {
            this.antStates = [];
            this.antStates.push(new AntState(this.antRowPosition, this.antColumnPosition, this.antRotation, false));
        }

        this.mapRows = mapRows;
        this.mapColumns = mapColumns;
        this.foodLocations = foodLocations;
    }

    moveAntForward() {
        if (this.movesAllowed < 1) {
            return;
        }

        if (this.antRotation === Constants.ANT_ROTATIONS.LEFT) {
            this.antColumnPosition--;
            if (this.antColumnPosition < 0) {
                this.antColumnPosition = this.mapColumns - 1;
            }

        } else if (this.antRotation === Constants.ANT_ROTATIONS.RIGHT) {
            this.antColumnPosition = (this.antColumnPosition + 1) % this.mapColumns;

        } else if (this.antRotation === Constants.ANT_ROTATIONS.UP) {
            this.antRowPosition--;
            if (this.antRowPosition < 0) {
                this.antRowPosition = this.mapRows - 1;
            }

        } else if (this.antRotation === Constants.ANT_ROTATIONS.DOWN) {
            this.antRowPosition = (this.antRowPosition + 1) % this.mapRows;

        } else {
            // TODO: Log an error here.
        }

        if (this.shouldSaveStates) {
            this.antStates.push(new AntState(
                this.antRowPosition, 
                this.antColumnPosition, 
                this.antRotation, 
                this.foodLocations[this.antRowPosition][this.antColumnPosition] === Constants.MAP_DATA.FOOD));
        }

        if (this.foodLocations[this.antRowPosition][this.antColumnPosition] === Constants.MAP_DATA.FOOD) {
            this.foodCollected++;
            this.foodLocations[this.antRowPosition][this.antColumnPosition] = Constants.MAP_DATA.OTHER;
        }
    }

    setAntRotation(newRotation) {
        if (this.movesAllowed < 1) {
            return;
        }

        this.antRotation = newRotation;

        if (this.shouldSaveStates) {
            this.antStates.push(new AntState(this.antRowPosition, this.antColumnPosition, this.antRotation, false));
        }
    }

    getAntRotation() {
        return this.antRotation;
    }

    mapAheadOfAnt() {
        if (this.antRotation === Constants.ANT_ROTATIONS.LEFT) {
            if (this.antColumnPosition === 0) {
                return this.foodLocations[this.antRowPosition][this.mapColumns - 1];
            }

            return this.foodLocations[this.antRowPosition][this.antColumnPosition - 1];

        } else if (this.antRotation === Constants.ANT_ROTATIONS.RIGHT) {
            return this.foodLocations[this.antRowPosition][(this.antColumnPosition + 1) % this.mapColumns];

        } else if (this.antRotation === Constants.ANT_ROTATIONS.UP) {
            if (this.antRowPosition === 0) {
                return this.foodLocations[this.mapRows - 1][this.antColumnPosition];
            }

            return this.foodLocations[this.antRowPosition - 1][this.antColumnPosition];

        } else if (this.antRotation === Constants.ANT_ROTATIONS.DOWN) {
            return this.foodLocations[(this.antRowPosition + 1) % this.mapRows][this.antColumnPosition];

        } else {
            // Error occured. Should be logged and the user notified.
            console.error("Checking if there is food ahead error! Rotation: " + this.antRotation);
        }
    }

    actionPerformed() {
        if (this.movesAllowed <= 0) {
            // TODO: Maybe Log this. But doesn't need to throw error.
        }

        this.movesAllowed--;
    }

    getNumberOfAvailableActions() {
        return this.movesAllowed;
    }

    getFoodEaten() {
        return this.foodCollected;
    }

    getAllAntStates() {
        return this.antStates;
    }
}
import React from 'react';
import {Button, Modal, ModalHeader, ModalBody, ModalFooter, Input, Label, FormGroup, FormFeedback} from 'reactstrap';

export default class MapSaveModal extends React.Component {
    
    constructor(props) {
        super(props);

        this.state = {
            nameValid: true
        }

        this.nameRegex = /^([a-zA-Z0-9])([a-zA-Z0-9_\s])+$/;
    }

    onSubmit = () => {
        if (!this.nameRegex.test(this.props.nameValue)) {
            this.setState({nameValid: false});
            return;
        }
        
        this.props.submitTrail();
    }

    render() {
        return(
            <Modal isOpen={this.props.modalOpened} toggle={this.props.modalToggle}>
                <ModalHeader className="" toggle={this.props.modalToggle}>Saving trail</ModalHeader>
                <ModalBody>
                    <FormGroup>
                        <Label for="map-name">Name your trail: </Label>
                        <Input id="map-name" 
                               type="text" 
                               placeholder="Koristite samo slova, brojke i _" 
                               onChange={this.props.onNameChange} 
                               value={this.props.nameValue} 
                               invalid={!this.state.nameValid}/>
                        <FormFeedback className="warn">Naziv nije ispravan!</FormFeedback>
                    </FormGroup>
                </ModalBody>
                <ModalFooter>
                    <Button color="primary" onClick={this.onSubmit}>Save</Button>
                    <Button onClick={this.props.modalToggle}>Cancel</Button>
                </ModalFooter>
            </Modal>
        );
    }
}


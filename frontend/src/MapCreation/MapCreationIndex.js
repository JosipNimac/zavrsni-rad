import React from 'react';
import {Redirect} from 'react-router-dom';

import Navigation from '../Navigation/Navigation';
import Loading from '../Util/SimpleLoadingAnimation';
import MapCreationContent from './MapCreationContent';

import axios from 'axios';

import constants from '../constants';

import './map-creation.css';

export default class MapCreationIndex extends React.Component {

    constructor(props) {
        super(props);

        this.state = {
            userLoggedIn: undefined,
            illegalUser: false,
            user: null
        }
    }

    componentDidMount() {
        this.checkLogin();
    }

    componentDidUpdate(prevProps) {
        if (prevProps.match.params.username !== this.props.match.params.username) {
            this.checkLogin();
        }
    }

    checkLogin() {
        this.setState({
            userLoggedIn: undefined,
            illegalUser: false,
            user: null
        }, () => {
            axios({
                method: "GET",
                url: constants.backend + "api/auth/myinfo"
            }).then(req => {
                this.setState({
                    userLoggedIn: req.data.notLoggedIn === undefined,
                    illegalUser: !req.data.notLoggedIn && req.data.username !== this.props.match.params.username,
                    user: !req.data.notLoggedIn ? req.data : null
                });
            }).catch(err => {
                // TODO: Decide what I want to happen if there is an error here.
            });
        });
    }

    render() {
        return (
            <div key={this.props.match.params.username}>
                <Navigation {...this.props}/>
                {this.state.illegalUser &&
                    <Redirect to={`/${this.state.user.username}/newMap`}/>
                }
                {this.state.userLoggedIn === undefined &&
                    <div className="loading-container"><Loading message="Loading..." spinnerColor="secondary" spinnerType="grow"/></div>
                }
                {this.state.userLoggedIn === true &&
                    <MapCreationContent {...this.props}/>
                }
                {this.state.userLoggedIn === false &&
                    <Redirect to="/auth"/>
                }
            </div>
        );
    }
}
import React from 'react';
import {Row, Col, Button} from 'reactstrap';
import TrailDesignMap from '../GeneticVisual/TrailDesign/TrailDesignVisual';
import MapSaveModal from './MapSaveModal';
import TrailConstants from '../GeneticProgramming/Constants';
import InfoModal from '../Util/InfoModal';
import axios from 'axios';
import constants from '../constants';

export default class MapCreationContent extends React.Component {
    
    constructor(props) {
        super(props);

        this.visuals = React.createRef();

        let mapParams = {
            mapRows: 32,
            mapColumns: 32,
            foodPieces: 0,
            antRow: null,
            antColumn: null,
            antRotation: null
        }
        
        mapParams.foodLocations = [];
        for (let i = 0; i < mapParams.mapRows; i++) {
            let row = [];

            for(let j = 0; j < mapParams.mapColumns; j++) {
                // By default there is nothing on the map.
                row.push(TrailConstants.MAP_DATA.OTHER);
            }

            mapParams.foodLocations.push(row);
        }

        this.state = {
            mapParams: mapParams,
            mapName: "",
            modalOpened: false,
            infoModal: {
                isOpen: false,
                toggle: this.infoToggle,
                title: "Incomplete data",
                message: ""
            }
        }

        
    }

    infoToggle = () => {
        this.setState({
            infoModal: {
                ...this.state.infoModal,
                isOpen: !this.state.infoModal.isOpen
            }
        });
    }

    mapParamsChanged = (newParams, callback) => {
        this.setState({
            mapParams: newParams
        }, callback);
    }

    onMapNameChange = event => {
        let name = event.target.value;
        this.setState({
            mapName: name,
            nameInvalid: false
        });
    }

    modalToggle = () => {
        if (this.state.mapParams.antRow === null || this.state.mapParams.antColumn === null || this.state.mapParams.antRotation === null) {
            this.setState({
                infoModal: {
                    ...this.state.infoModal,
                    message: "Ant is not set. There must be a starting position for the ant."
                }
            }, this.infoToggle);

            return;
        }

        if (this.state.mapParams.foodPieces < 10) {
            this.setState({
                infoModal: {
                    ...this.state.infoModal,
                    message: "You need to put at least 10 pieces of food on the trail."
                }
            }, this.infoToggle);

            return;
        }
        
        this.setState({
            modalOpened: !this.state.modalOpened
        });
    }

    submitTrail = () => {
        let thumbnail = this.visuals.current.generateThumbnail(380, 380);

        // TODO: Here look into giving a loadbar to show how much data we sent!!
        axios({
            method: "POST",
            url: constants.backend + "api/trails/upload",
            data: {
                mapParams: {
                    ...this.state.mapParams,
                    mapName: this.state.mapName
                },
                thumbnailBase64: thumbnail.src
            }
        }).then(res => {
            console.log(res);
            this.props.history.push("/myProfile");
        }).catch(err => {
            console.log(err);
        });
    }

    render() {
        let antData = adjustAntData(this.state.mapParams);

        return(
            <div className="container-full">
                <InfoModal isOpen={this.state.infoModal.isOpen} toggle={this.state.infoModal.toggle} title={this.state.infoModal.title} message={this.state.infoModal.message}></InfoModal>
                <MapSaveModal modalOpened={this.state.modalOpened} modalToggle={this.modalToggle} submitTrail={this.submitTrail} onNameChange={this.onMapNameChange} nameValue={this.state.mapName}/>
                <Row className="row-no-margin">
                    <Col lg={5} md={7} sm={12} xs={12}>
                        <h4 className="subtitle">Design your own trail!</h4>
                        <div className="trail-container"><TrailDesignMap ref={this.visuals} mapParams={this.state.mapParams} mapParamsChanged={this.mapParamsChanged} resolution={1000}/></div>
                    </Col>
                    <Col lg={7} md={5} sm={12} xs={12}>
                        <div className="trail-design-info">
                            <h5 className="subtitle">Short introduction:</h5>
                            <p className="my-text">On this page you will be able to design and save your own trails that other users will then use in their ant's training.
                            Because ants will have a limited amount of allowed moves per run, it is recommended that your trail has between 80 and 100 pieces of food. 
                            The trail should also make sense, because if you place the food randomly, ants will likely not learn any intelligent behaviour. 
                            And finally, at the start ant should be close to the trail - ideally it should be in a cell next to the start of the trail.
                            So with that in mind, try to design an interesting trail! <span role="img" aria-label="smiley_face">🙂</span></p>
                        </div>

                        <div className="trail-info-block">
                            <p className="info">Food pieces placed: <span className="framed">{this.state.mapParams.foodPieces}</span></p>
                            <p className="info">
                                Ant row number: <span className="framed">{antData.antRow}</span> <br/> 
                                Ant column number: <span className="framed">{antData.antColumn}</span> <br/>
                                Ant rotation: <span className="framed">{antData.antRotation}</span>
                            </p>
                        </div>

                        <div className="justify-content-center">
                            <Button color="primary" onClick={this.modalToggle}>Save trail</Button>
                        </div>
                    </Col>
                </Row>
            </div>
        )
    }
}

function adjustAntData(mapParams) {
    let antData = {};

    antData.antRow = mapParams.antRow !== null ? mapParams.antRow + 1 : "Ant not placed";
    antData.antColumn = mapParams.antColumn !== null ? mapParams.antColumn + 1 : "Ant not placed";

    if (mapParams.antRotation === TrailConstants.ANT_ROTATIONS.RIGHT) {
        antData.antRotation = "Right";
    } else if (mapParams.antRotation === TrailConstants.ANT_ROTATIONS.DOWN) {
        antData.antRotation = "Down";
    } else if (mapParams.antRotation === TrailConstants.ANT_ROTATIONS.LEFT) {
        antData.antRotation = "Left";
    } else if (mapParams.antRotation === TrailConstants.ANT_ROTATIONS.UP) {
        antData.antRotation = "Up";
    } else {
        // Unknown rotation. Decide how I want to handle this.
        antData.antRotation = "Ant not placed";
    }

    return antData;
}
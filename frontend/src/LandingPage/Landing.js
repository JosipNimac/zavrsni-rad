import React from 'react';
import Navigation from '../Navigation/Navigation';
import {Col} from 'reactstrap';

import './landing-page.css';
//import TrainingStarter from '../GeneticVisual/TrainedAnimation/TrainingStarter';
import RandomAnimation from '../Util/RandomVisualDemo';

export default class Landing extends React.Component {

    render() {
        return (
            <div>
                <Navigation {...this.props}/> 
                <div className="content">
                    <div className="title-header"><h1>Genetic programming demonstration</h1></div>
                    <Col style={{padding: 0}} xs={12} sm={12} md={7} lg={5}>
                        <div className="genetic-visual">
                            <div className="animation-desc">Check out this ant that was trained on this web-app:</div>
                            <RandomAnimation />
                        </div>
                    </Col>
                    <div className="lorem-ipsum">
                        <h3>Motivation</h3>
                        <p>
                            When I was just starting to learn about things like evolutionary algorithms and genetic programming I understood the basic concepts and ideas, 
                            but I definitely was not able to code the entire algorithm and graphical interface to see the results of training on some problem. 
                            And so, as I could not find any interactive examples of a project that would let me play with some parameters of the algorithm so I could see their effect 
                            for myself, I was just looking at tutorials and I was limited to the examples that were used there.
                        </p>
                        <p>
                            That is why now I developed this web-app. It will let you do just that. You will be able to pick the parameters of the algorithm and with the click of a button 
                            you will be able to see the results of your choice. The problem that can be solved on this site is the problem of training a virtual ant some food trail with
                            the goal of getting an ant that eats as much food as possible. You can read about the problem on <a href="https://en.wikipedia.org/wiki/Santa_Fe_Trail_problem" target="_blank">Wikipedia</a> or
                            you can check the summary on this link: <a href="/">Details</a>
                        </p>
                        <p>
                            Animation on the left is an example of what this application is capable of. You can even set the animation speed. If you register your account,
                            you will be able to design your own trails, but even without registering you can explore trails <a href="/trails">here</a>. Just click on the trail 
                            you think is interesting, select your parameters, click train and soon animation will be ready to show you the results of the training.
                        </p>
                    </div>
                </div>
            </div>
        )
    }
}

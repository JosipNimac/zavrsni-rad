import React from 'react';
import {Collapse, NavbarToggler, Navbar, Nav, NavItem, NavLink} from 'reactstrap';
import {Redirect} from 'react-router-dom';
import antImage from '../Resources/GeneticDisplay/Ant.png';
import axios from 'axios';
import constants from '../constants';

import "bootstrap/dist/css/bootstrap.min.css";
import './navbar.css';

export default class Navigation extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            isOpen: false,
            user: null,
            logoutPressed: false
        }
    }

    componentDidMount() {
        // Getting info about the user so it can display additional buttons (profile and logout).
        axios({
            method: "GET",
            url: constants.backend + "api/auth/myinfo"
        }).then(res => {
            if (!res.data.notLoggedIn) {
                this.setState({
                    user: res.data
                });
            }
        }).catch(err => {
            // If we are here we cannot get user info, probably because user is not logged in.
        });
    }

    toggle = () => {
        this.setState({
            isOpen: !this.state.isOpen
        });
    }

    logout = () => {
        if (!this.props) {
            alert("Navigation does not have props. Check where the error is!");
            return;
        }

        axios({
            method: "GET",
            url: constants.backend + "api/auth/logout",
        }).then(res => {
            this.setState({
                logoutPressed: true
            });
            localStorage.removeItem("LoggedIn");
        }).catch(err => {
            if (err.response && err.response.status === 401) {
                // Unauthorized request for logout. This means user is logged out but logout button is still diplayed.
                // We will just claim that the button was pressed again. This should return user to loanding and log them out. 
                this.setState({
                    logoutPressed: true
                });
            }
        });
    }

    render() {
        if (this.state.logoutPressed) {
            return <Redirect to="/"/>
        }

        return (
            <Navbar dark className="sticky-top nav" expand="sm">
                <Nav navbar>
                    <NavItem active>
                        <NavLink id="homepage-link" href="/">
                            <img id="homepage-image" src={antImage} alt="Ant logo" width={42} height={42}/>
                            Home page
                        </NavLink>
                    </NavItem>
                </Nav>

                <NavbarToggler className="mr-2" onClick={this.toggle}/>
                <Collapse isOpen={this.state.isOpen} navbar>
                    <Nav className="ml-auto" navbar>

                        <NavItem active hidden={this.state.user === null} className="my-nav-item">
                            <NavLink href="/myProfile">
                                {this.state.user ? this.state.user.username : ""}
                            </NavLink>
                        </NavItem>

                        <NavItem active className="my-nav-item">
                            <NavLink href="/">
                                About algorithm
                            </NavLink>
                        </NavItem>

                        <NavItem active className="my-nav-item">
                            <NavLink href="/trails">
                                Explore trails
                            </NavLink>
                        </NavItem>

                        <NavItem active hidden={this.state.user !== null} className="my-nav-item">
                            <NavLink href="/auth">
                                Log in / Sign up
                            </NavLink>
                        </NavItem>

                        <NavItem active hidden={this.state.user === null} className="my-nav-item" onClick={this.logout}>
                            <NavLink>
                                Logout
                            </NavLink>
                        </NavItem>
                    </Nav>
                </Collapse>
            </Navbar>
        );
    }
}

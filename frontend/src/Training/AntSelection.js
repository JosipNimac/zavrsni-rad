import React from 'react';
import axios from 'axios';
import {Row, Col, Input, Label, Button} from 'reactstrap';

import constants from '../constants';
import ConfirmSelectionModal from './ConfirmSelectionModal';
import AntSearch from './AntSearch';

export default class AntSelection extends React.Component {

    constructor(props) {
        super(props);

        this.state = {
            recentAnts: [],
            selectedAnt: null,
            modalOpened: false,
        }
    }

    componentDidMount() {
        if (this.props.loggedIn) {
            axios({
                method: "GET",
                url: constants.backend + "api/ant/recent"
            }).then(res => {
                this.setState({recentAnts: res.data, selectedAnt: 0});
            }).catch(err => {
    
            });
        }
    }

    modalToggle = () => {
        this.setState({modalOpened: !this.state.modalOpened});
    }

    antChanged = (event) => {
        this.setState({
            selectedAnt: parseInt(event.target.value)
        });
    }

    antSelected = () => {
        if (this.props.unsavedAnt) {
            return this.setState({modalOpened: true});
        } else {
            this.antChoosen();
        }
    }
    
    antChoosen = () => {
        this.setState({
            modalOpened: false
        });

        this.props.antSelected(this.state.recentAnts[this.state.selectedAnt]);
    }

    render() {
        return (
            <div className="container-ant-selection">
                <ConfirmSelectionModal isOpen={this.state.modalOpened} toggle={this.modalToggle} confirmSelection={this.antChoosen}/>
                {(this.props.loggedIn && this.state.recentAnts.length > 0) &&  
                    <div className="bottom-border row-container">
                        <Row className="form-text">
                            <Col xs={12}><Label for="antSelect">Check one of your own recent ants on this track!</Label></Col>
                        </Row>
                        <Row className="d-flex">
                            <Col>
                                <Input id="antSelect" type="select" bsSize="sm" value={this.state.selectedAnt} onChange={this.antChanged}>
                                    {this.state.recentAnts.map((ant, index) => {
                                        return (<option key={ant.code} value={index} onClick={this.antChanged}>{ant.name} --- {new Date(ant.timestamp_created).toLocaleString()}</option>)   
                                    })}
                                </Input>
                            </Col>
                            <Col xs="auto">
                                <Button outline size="sm" onClick={this.antSelected} disabled={this.props.selectionDisabled}>Select ant</Button>
                            </Col>
                        </Row>
                    </div>
                }

                <AntSearch unsavedAnt={this.props.unsavedAnt} antSelected={this.props.antSelected}/>
            </div>
        );
    }
}
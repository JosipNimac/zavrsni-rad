import React from 'react';
import {Modal, ModalHeader, ModalBody, ModalFooter, Label, Input, FormFeedback, FormGroup, Button} from 'reactstrap';

export default class AntSaveModal extends React.Component {

    constructor(props) {
        super(props);

        this.state = {
            antName: "",
            nameValid: true
        }

        // TODO: Add a regex here!!
        this.nameRegex = null;
    }

    onNameChange = (event) => {
        this.setState({
            antName: event.target.value
        });
    }

    onSubmit = (event) => {
        /*if (!this.nameRegex.test(this.state.antName)) {
            this.setState({nameValid: false});
            return;
        }*/

        this.props.onSubmit(this.state.antName);
    }

    render() {
        return (
            <Modal isOpen={this.props.opened} toggle={this.props.toggle}>
                <ModalHeader toggle={this.props.toggle}>Saving ant</ModalHeader>
                <ModalBody>
                    <FormGroup>
                        <Label for="ant-name">Give your ant a name:</Label>
                        <Input id="ant-name"
                               placeholder="Name can contain numbers, letters, spaces and -"
                               value={this.state.antName}
                               onChange={this.onNameChange}
                               invalid={!this.state.nameValid}/>
                        <FormFeedback className="warn">Given name is not valid!</FormFeedback>
                    </FormGroup>
                </ModalBody>
                <ModalFooter>
                    <Button color="primary" onClick={this.onSubmit}>Save</Button>
                    <Button onClick={this.props.toggle}>Cancel</Button>
                </ModalFooter>
            </Modal>
        );
    }
}
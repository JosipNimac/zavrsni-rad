import React from 'react';
import {Row, Col, Input, Button, Label, Fade, FormFeedback} from 'reactstrap';
import ToggleButton from 'react-toggle';

import constants from '../constants';
import axios from 'axios';
import Loading from '../Util/SimpleLoadingAnimation';

import antPNG from '../Resources/GeneticDisplay/Ant_small.png';
import ConfirmSelectionModal from './ConfirmSelectionModal';

export default class AntSearch extends React.Component {

    constructor(props) {
        super(props);

        this.state = {
            searchByName: true,
            query: {
                search: "",
                valid: true
            },
            ants: [],
            antsLoading: false,
            antsLoaded: false,
            confirmModalOpened: false,
            selectedAnt: null
        }
    }

    searchAnts = () => {
        if (this.state.query.search.length === 0) {
            return this.setState({
                query: {
                    ...this.state.query,
                    valid: false
                }
            });
        }
        
        this.setState({antsLoading: true, antsLoaded: false}, this.fetchAnts);
    }

    fetchAnts = () => {
        axios({
            method: "GET",
            url: constants.backend + "api/ant/search?limit=10&subQuery=" +  this.state.query.search + "&code=" + !this.state.searchByName
        }).then(res => {
            console.log(res.data);
            this.setState({ants: res.data, antsLoading: false, antsLoaded: true});
        }).catch(err => {

        });
    }

    searchParamChanged = () => {
        this.setState({searchByName: !this.state.searchByName});
    }

    searchQueryChanged = (event) => {
        this.setState({
            query: {
                ...this.state.query,
                search: event.target.value,
                valid: true
            }
        });
    }

    antSelected = (ant) => {
        if (this.props.unsavedAnt) {
            this.setState({confirmModalOpened: true});
        } else {   
            this.props.antSelected(ant);
        }
    }

    confirmedSelection = (ant) => {
        if (this.state.selectedAnt === null && this.props.unsavedAnt) {
            this.setState({
                confirmModalOpened: true,
                selectedAnt: ant
            });
        } else {
            this.setState({confirmModalOpened: false, selectedAnt: null});
            this.props.antSelected(this.state.selectedAnt === null ? ant : this.state.selectedAnt);
        }
    }

    modalToggle = () => {
        this.setState({confirmModalOpened: !this.state.confirmModalOpened});
    }

    render() {
        return (
            <div className="row-container">
                <ConfirmSelectionModal isOpen={this.state.confirmModalOpened} toggle={this.modalToggle} confirmSelection={this.confirmedSelection}/>
                <Row className="form-text">
                    <Col xs={12}>
                        <Label for="ant-search-box">Search ants by name or code!</Label>
                    </Col>
                </Row>
                <Row className="d-flex justify-content-center row-vertical-margin">
                    <Col xs={5}><Fade in={this.state.searchByName}><span className="regular-text">Searching by name</span></Fade></Col>
                    <Col xs={2}><ToggleButton className="name-code-select" icons={false} onChange={this.searchParamChanged}/></Col>
                    <Col xs={5}><Fade in={!this.state.searchByName}><span className="regular-text">Searching by code</span></Fade></Col>
                </Row>
                <Row className="d-flex">
                    <Col>
                        <Input name="ant-search-box" 
                               value={this.state.query.search}
                               onChange={this.searchQueryChanged}
                               placeholder={`Type in ant's ${this.state.searchByName ? "name" : "code"} here`} 
                               bsSize="sm"
                               invalid={!this.state.query.valid}/>
                            <FormFeedback className="warn">This field can't be empty.</FormFeedback>
                    </Col>
                    <Col xs="auto">
                        <Button outline onClick={this.searchAnts} size="sm">Search</Button>
                    </Col>
                </Row>
                <Row className="row-no-margin margin-top" hidden={!this.state.antsLoaded}>
                    <Col xs={12}><h4 className="strong-info col-no-padding">Search results: </h4></Col>
                    {this.state.ants.map((ant, index) => 
                        <Col className="col-no-padding" xs={4} sm={3} md={4} lg={4} key={ant.code}><AntCard ant={ant} antSelected={this.confirmedSelection}/></Col>
                    )}
                </Row>
                <div hidden={!this.state.antsLoading}>
                    {this.state.antsLoading &&
                        <Loading spinnerColor="gray" spinnerType="grow"/>
                    }
                </div>
            </div>
        );
    }
}

function AntCard(props) {
    return (
        <div className="container-full ant-card">
            <Row className="row-no-margin">
                <Col className="col-no-padding">{props.ant.name}</Col>
            </Row>
            <Row className="row-no-margin">
                <Col className="col-no-padding"><img onClick={() => props.antSelected(props.ant)} className="ant-image" src={antPNG} alt="ant"/></Col>
            </Row>
            <Row className="row-no-margin">
                <Col className="col-no-padding">{props.ant.code}</Col>
            </Row>
            <Row className="row-no-margin">
                <Col className="col-no-padding">By: {props.ant.username}</Col>
            </Row>
        </div>
    );
}
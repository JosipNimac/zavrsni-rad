const initialValidation = {
    popInitParams: {
        popSize: true,
        desiredDepth: true,
        maxNodes: true,
        method: true
    },
    operators: {
        selection: {
            sizeOfTournament: true
        },
        mutation: {
            maxDepth: true,
            maxNodes: true
        },
        crossover: {
            maxNodes: true
        },
        evaluator: {
            plagirismFactor: true
        }
    },
    generations: true,
    reproductionRate: true,
    mutationRate: true,
    crossoverRate: true,
    maxGensNoProgress: true
}

const initialParams = {
    popInitParams: {
        popSize: 10,
        desiredDepth: 2,
        maxNodes: 1,
        method: "Full"
    },
    operators: {
        selection: {
            sizeOfTournament: 1
        },
        mutation: {
            maxDepth: 2,
            maxNodes: 1
        },
        crossover: {
            maxNodes: 1
        },
        evaluator: {
            plagirismFactor: 1.
        }
    },
    generations: 1,
    reproductionRate: 0.01,
    mutationRate: 0.14,
    crossoverRate: 0.85,
    elitist: false,
    maxGensNoProgress: 100,
    mutationInjection: false
}

export default {
    initialValidation,
    initialParams
}
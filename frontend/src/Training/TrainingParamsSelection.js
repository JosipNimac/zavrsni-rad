import React from 'react';
import {Button, Row, Col} from 'reactstrap';

import TrainingUtils from './TrainingUtilComponents';
import AntSaveModal from './AntSaveModal';
import CancelResultsModal from './CancelResultsModal';
import Loading from '../Util/SimpleLoadingAnimation';
import TrainingConstants from './training-constants';

import lodash from 'lodash';

import './training.css';

// TODO: Extract theese to a sensible constanst file.
const minPopSize = 10;
const maxPopSize = 2000;
const minDepth = 2;
const maxDepth = 200;
const minAllowedNodes = 1;
const maxAllowedNodes = 5000;
const minGenerations = 1;
const maxGenerations = 1000;
const gensNoProgressAllowed = 10;

export default class TrainingParamsSelection extends React.Component {
    
    constructor(props) {
        super(props);

        this.state = {
            algorithmParams: TrainingConstants.initialParams,
            validation: TrainingConstants.initialValidation,
            inputsDisabled: false,
            antSaveModalOpened: false,
            cancelResultModalOpened: false
        }
    }

    popInitParamChanged = (event) => {
        this.setState({
            algorithmParams: {
                ...this.state.algorithmParams,
                popInitParams: {
                    ...this.state.algorithmParams.popInitParams,
                    [event.target.id]: event.target.value
                }
            }
        });
    }

    operatorParamChanged = (event, operator) => {
        const oper = this.state.algorithmParams.operators[operator];

        this.setState({
            algorithmParams: {
                ...this.state.algorithmParams,
                operators: {
                    ...this.state.algorithmParams.operators,
                    [operator]: {
                        ...oper,
                        [event.target.id]: event.target.value
                    }
                }
            }
        });
    }

    generalParamChanged = (event) => {
        if (typeof this.state.algorithmParams[event.target.id] === "boolean") {
            const currentToggle = this.state.algorithmParams[event.target.id];
            this.setState({
                algorithmParams: {
                    ...this.state.algorithmParams,
                    [event.target.id]: !currentToggle
                }
            });
        } else {
            this.setState({
                algorithmParams: {
                    ...this.state.algorithmParams,
                    [event.target.id]: event.target.value
                }
            });
        }
    }

    trainingStart = () => {
        if (!this.validate()) {
            window.scrollTo(0, 0);
            return;
        }

        this.setState({
            inputsDisabled: true
        });

        this.props.trainingStarted(this.state.algorithmParams);
    }

    validate() {
        const validationCopy = lodash.clone(this.state.validation);
        let isValid = true;
        
        // Pop init params
        const {popSize, desiredDepth, maxNodes} = this.state.algorithmParams.popInitParams;
        validationCopy.popInitParams.popSize = (popSize >= minPopSize && popSize <= maxPopSize);
        validationCopy.popInitParams.desiredDepth = (desiredDepth >= minDepth && desiredDepth <= maxDepth);
        validationCopy.popInitParams.maxNodes = (maxNodes >= minAllowedNodes && maxNodes <= maxAllowedNodes);
        isValid = isValid && validationCopy.popInitParams.popSize && validationCopy.popInitParams.desiredDepth && validationCopy.popInitParams.maxNodes;

        // Operators
        const {selection, mutation, crossover, evaluator} = this.state.algorithmParams.operators;
        validationCopy.operators.selection.sizeOfTournament = selection.sizeOfTournament > 0;
        validationCopy.operators.mutation.maxDepth = (mutation.maxDepth >= minDepth && mutation.maxDepth <= maxDepth);
        validationCopy.operators.mutation.maxNodes = (mutation.maxNodes >= minAllowedNodes && mutation.maxNodes <= maxAllowedNodes);
        validationCopy.operators.crossover.maxNodes = (crossover.maxNodes >= minAllowedNodes && crossover.maxNodes <= maxAllowedNodes);
        validationCopy.operators.evaluator.plagirismFactor = (evaluator.plagirismFactor >= 0 && evaluator.plagirismFactor <= 1)
        isValid = isValid && validationCopy.operators.selection.sizeOfTournament && validationCopy.operators.mutation.maxDepth && validationCopy.operators.mutation.maxNodes &&
            validationCopy.operators.crossover.maxNodes && validationCopy.operators.evaluator.plagirismFactor;

        // General
        const {generations, reproductionRate, mutationRate, crossoverRate, maxGensNoProgress} = this.state.algorithmParams;
        validationCopy.generations = generations >= minGenerations && generations <= maxGenerations;
        validationCopy.reproductionRate = reproductionRate >= 0;
        validationCopy.mutationRate = mutationRate >= 0;
        validationCopy.crossoverRate = crossoverRate >= 0;
        validationCopy.maxGensNoProgress = maxGensNoProgress >= gensNoProgressAllowed;
        isValid = isValid && validationCopy.generations && validationCopy.reproductionRate && validationCopy.mutationRate && validationCopy.crossoverRate && validationCopy.maxGensNoProgress;

        this.setState({
            validation: validationCopy
        });

        return isValid;
    }

    antSaveModalToggle = () => {
        this.setState({
            antSaveModalOpened: !this.state.antSaveModalOpened
        });
    }

    cancelResultModalToggle = () => {
        this.setState({
            cancelResultModalOpened: !this.state.cancelResultModalOpened
        });
    }

    resultsDeleted = () => {
        this.setState({
            inputsDisabled: false,
            antSaveModalOpened: false,
            cancelResultModalOpened: false
        }, this.props.resultsDeleted);
    }

    onSubmit = (antName) => {
        this.antSaveModalToggle()
        this.props.submitAnt(this.state.algorithmParams, antName);
    }

    antParamsLoaded = () => {
        this.setState({
            algorithmParams: this.props.paramsReceived,
            validation: TrainingConstants.initialValidation,
            inputsDisabled: true
        });
    }

    cancelTraining = () => {
        if (this.props.antLoaded) {
            return this.resultsDeleted();
        } else {
            this.cancelResultModalToggle();
        }
    }

    render() {
        if (this.state === null || this.state.algorithmParams === undefined) {
            return (
                <Loading spinnerColor="gray" spinnerType="grow"/>
            );
        }

        return (
            <div className="training-content">
                <AntSaveModal opened={this.state.antSaveModalOpened} 
                              toggle={this.antSaveModalToggle}
                              onSubmit={this.onSubmit}/>
                <CancelResultsModal opened={this.state.cancelResultModalOpened} 
                                    toggle={this.cancelResultModalToggle}
                                    newParamsSelection={this.resultsDeleted}/>

                <TrainingUtils.InitialPopulation data={this.state.algorithmParams.popInitParams} onParamChanged={this.popInitParamChanged} validation={this.state.validation.popInitParams} disabled={this.state.inputsDisabled}/>
                <TrainingUtils.Selection data={this.state.algorithmParams.operators.selection} onParamChanged={this.operatorParamChanged} validation={this.state.validation.operators} disabled={this.state.inputsDisabled}/>
                <TrainingUtils.Mutation data={this.state.algorithmParams.operators.mutation} onParamChanged={this.operatorParamChanged} validation={this.state.validation.operators} disabled={this.state.inputsDisabled}/>
                <TrainingUtils.Crossover data={this.state.algorithmParams.operators.crossover} onParamChanged={this.operatorParamChanged} validation={this.state.validation.operators} disabled={this.state.inputsDisabled}/>
                <TrainingUtils.Evaluator data={this.state.algorithmParams.operators.evaluator} onParamChanged={this.operatorParamChanged} validation={this.state.validation.operators} disabled={this.state.inputsDisabled}/>
                <TrainingUtils.General data={this.state.algorithmParams} onParamChanged={this.generalParamChanged} validation={this.state.validation} disabled={this.state.inputsDisabled}/>

                <Button className="training-button" outline onClick={this.trainingStart} hidden={this.state.inputsDisabled}>
                    Start training!
                </Button>
                <div className="container-full training-button" hidden={!this.state.inputsDisabled}>
                    <Row className="d-flex justify-content-around">
                        <Col xs="auto">
                            <Button outline onClick={this.antSaveModalToggle} disabled={this.props.savingDisabled}>Save ant</Button>
                        </Col>
                        <Col xs="auto">
                            <Button outline onClick={this.cancelTraining}>New training</Button>
                        </Col>
                    </Row>
                </div>
            </div>
        );
    }
}

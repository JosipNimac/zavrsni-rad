import React from 'react';
import {FormGroup, Label, Col, Input, FormFeedback} from 'reactstrap';

import Toggle from 'react-toggle';
import "react-toggle/style.css";

import './training.css';

// TODO: Extract theese to a sensible constanst file.
const minPopSize = 10;
const maxPopSize = 2000;
const minDepth = 2;
const maxDepth = 200;
const minAllowedNodes = 1;
const maxAllowedNodes = 5000;
const minGenerations = 1;
const maxGenerations = 1000;
const gensNoProgressAllowed = 10;

function InitialPopulation(props) {
    return (
        <fieldset className="with-border" disabled={props.disabled}>
            <legend className="strong-info">Initial population parameters</legend>
            <FormGroup row>
                <Col sm={7} md={6} lg={4}><Label className="label" for="popSize">Population size:</Label></Col>
                <Col sm={5} md={6} lg={8}>
                    <Input bsSize="sm" invalid={!props.validation.popSize} value={props.data.popSize} onChange={props.onParamChanged} id="popSize" type="number" disabled={props.disabled}/>
                    <FormFeedback className="warn">Population size must be in interval [{minPopSize}-{maxPopSize}]!</FormFeedback>
                </Col>
            </FormGroup>
            <FormGroup row>
                <Col sm={7} md={6} lg={4}><Label className="label" for="desiredDepth">Desired tree depth:</Label></Col>
                <Col sm={5} md={6} lg={8}>
                    <Input bsSize="sm" invalid={!props.validation.desiredDepth} value={props.data.desiredDepth} onChange={props.onParamChanged} id="desiredDepth" type="number" disabled={props.disabled}/>
                    <FormFeedback className="warn">Desired tree depth must be in interval [{minDepth}-{maxDepth}]!</FormFeedback>
                </Col>
            </FormGroup>
            <FormGroup row>
                <Col sm={7} md={6} lg={4}><Label className="label" for="maxNodes">Maximum nodes:</Label></Col>
                <Col sm={5} md={6} lg={8}>
                    <Input bsSize="sm" invalid={!props.validation.maxNodes} value={props.data.maxNodes} onChange={props.onParamChanged} id="maxNodes" type="number" disabled={props.disabled} step={10}/>
                    <FormFeedback className="warn">Maximum number of nodes must be in interval [{minAllowedNodes}-{maxAllowedNodes}]!</FormFeedback>
                </Col>
            </FormGroup>
            <FormGroup row >
            <Col sm={7} md={6} lg={4}><Label className="label" for="method">Tree creation method:</Label></Col>
                <Col sm={5} md={6} lg={8}>
                    <Input bsSize="sm" className="col-no-margin" onChange={props.onParamChanged} id="method" type="select" disabled={props.disabled}>
                        <option>Full</option>
                        <option>Grow</option>
                        <option>Half_and_half</option>
                    </Input>
                </Col>
            </FormGroup>
        </fieldset>
    );
}

function Selection(props) {
    return(
        <fieldset className="with-border" disabled={props.disabled}>
            <legend className="strong-info">Selection</legend>
            <FormGroup row>
                <Col sm={7} md={6} lg={4}><Label className="label" for="sizeOfTournament">Tournament size:</Label></Col>
                <Col sm={5} md={6} lg={8}>
                    <Input bsSize="sm" invalid={!props.validation.selection.sizeOfTournament} value={props.data.sizeOfTournament} onChange={(e) => props.onParamChanged(e, "selection")} id="sizeOfTournament" type="number" disabled={props.disabled}/>
                    <FormFeedback className="warn">Tournament size must be a positive number!</FormFeedback>
                </Col>
            </FormGroup>
        </fieldset>
    );
}

function Mutation(props) {
    return (
        <fieldset className="with-border" disabled={props.disabled}>
            <legend className="strong-info">Mutation</legend>
            <FormGroup row>
                <Col sm={7} md={6} lg={4}><Label className="label" for="maxDepth">Maximum tree depth:</Label></Col>
                <Col sm={5} md={6} lg={8}>
                    <Input bsSize="sm" invalid={!props.validation.mutation.maxDepth} value={props.data.maxDepth} onChange={(e) => props.onParamChanged(e, "mutation")} id="maxDepth" type="number" disabled={props.disabled}/>
                    <FormFeedback className="warn">Maximum tree depth must be in interval [{minDepth}-{maxDepth}]!</FormFeedback>
                </Col>
            </FormGroup>
            <FormGroup row>
                <Col sm={7} md={6} lg={4}><Label className="label" for="maxNodes">Maximum nodes:</Label></Col>
                <Col sm={5} md={6} lg={8}>
                    <Input bsSize="sm" invalid={!props.validation.mutation.maxNodes} value={props.data.maxNodes} onChange={(e) => props.onParamChanged(e, "mutation")} id="maxNodes" type="number" disabled={props.disabled} step={10}/>
                    <FormFeedback className="warn">Maximum number of nodes must be in interval [{minAllowedNodes}-{maxAllowedNodes}]!</FormFeedback>
                </Col>
            </FormGroup>
        </fieldset>
    );
}

function Crossover(props) {
    return (
        <fieldset className="with-border" disabled={props.disabled}>
            <legend className="strong-info">Crossover</legend>
            <FormGroup row>
                <Col sm={7} md={6} lg={4}><Label className="label" for="maxNodes">Maximum nodes:</Label></Col>
                <Col sm={5} md={6} lg={8}>
                    <Input bsSize="sm" invalid={!props.validation.crossover.maxNodes} value={props.data.maxNodes} onChange={(e) => props.onParamChanged(e, "crossover")} id="maxNodes" type="number" disabled={props.disabled} step={10}/>
                    <FormFeedback className="warn">Maximum number of nodes must be in interval [{minAllowedNodes}-{maxAllowedNodes}]!</FormFeedback>
                </Col>
            </FormGroup>
        </fieldset>
    );
}

function Evaluator(props) {
    return (
        <fieldset className="with-border" disabled={props.disabled}>
            <legend className="strong-info">Evaluator</legend>
            <FormGroup row>
                <Col sm={7} md={6} lg={4}><Label className="label" for="plagirismFactor">Plagirism factor:</Label></Col>
                <Col sm={5} md={6} lg={8}>
                    <Input bsSize="sm" invalid={!props.validation.evaluator.plagirismFactor} value={props.data.plagirismFactor} onChange={(e) => props.onParamChanged(e, "evaluator")} id="plagirismFactor" type="number" step={0.01} disabled={props.disabled}/>
                    <FormFeedback className="warn">Plagirism factor must be in interval [0-1]!</FormFeedback>
                </Col>
            </FormGroup>
        </fieldset>
    );
}

function General(props) {
    return (
        <fieldset className="with-border" disabled={props.disabled}>
            <legend className="strong-info">General</legend>
            <FormGroup row>
            <Col sm={7} md={6} lg={4}><Label className="label" for="generations">Number of generations:</Label></Col>
                <Col sm={5} md={6} lg={8}>
                    <Input bsSize="sm" invalid={!props.validation.generations} value={props.data.generations} onChange={props.onParamChanged} id="generations" type="number" disabled={props.disabled}/>
                    <FormFeedback className="warn">Number of generations must be in interval[{minGenerations}-{maxGenerations}]!</FormFeedback>
                </Col>
            </FormGroup>
            <FormGroup row>
                <Col sm={7} md={6} lg={4}><Label className="label" for="reproductionRate">Reproduction ratio:</Label></Col>
                <Col sm={5} md={6} lg={8}>
                    <Input bsSize="sm" invalid={!props.validation.reproductionRate} value={props.data.reproductionRate} onChange={props.onParamChanged} id="reproductionRate" type="number" step={0.01} disabled={props.disabled}/>
                    <FormFeedback className="warn">Reproduction ratio must be positive!</FormFeedback>
                </Col>
            </FormGroup>
            <FormGroup row>
                <Col sm={7} md={6} lg={4}><Label className="label" for="mutationRate">Mutation ratio:</Label></Col>
                <Col sm={5} md={6} lg={8}>
                    <Input bsSize="sm" invalid={!props.validation.mutationRate} value={props.data.mutationRate} onChange={props.onParamChanged} id="mutationRate" type="number" step={0.01} disabled={props.disabled}/>
                    <FormFeedback className="warn">Mutation ratio must be positive!</FormFeedback>
                </Col>
            </FormGroup>
            <FormGroup row>
                <Col sm={7} md={6} lg={4}><Label className="label" for="crossoverRate">Crossover ratio:</Label></Col>
                <Col sm={5} md={6} lg={8}>
                    <Input bsSize="sm" invalid={!props.validation.crossoverRate} value={props.data.crossoverRate} onChange={props.onParamChanged} id="crossoverRate" type="number" step={0.01} disabled={props.disabled}/>
                    <FormFeedback className="warn">Crossover ratio must be positive!</FormFeedback>
                </Col>
            </FormGroup>
            <FormGroup row>
                <Col sm={7} md={6} lg={4}><Label className="label" for="maxGensNoProgress">Maximum stagnation generations:</Label></Col>
                <Col sm={5} md={6} lg={8}>
                    <Input bsSize="sm" invalid={!props.validation.maxGensNoProgress} value={props.data.maxGensNoProgress} onChange={props.onParamChanged} id="maxGensNoProgress" type="number" disabled={props.disabled}/>
                    <FormFeedback className="warn">Minimum value for this field is [{gensNoProgressAllowed}]!</FormFeedback>
                </Col>
            </FormGroup>
            <FormGroup row>
                <Col sm={7} md={6} lg={4}><Label className="label" for="elitist">Elitist:</Label></Col>
                <Col sm={5} md={6} lg={8}>
                    <Toggle checked={props.data.elitist} onChange={props.onParamChanged} className="col-no-margin" id="elitist" disabled={props.disabled}/>
                </Col>
            </FormGroup>
            <FormGroup row>
                <Col sm={7} md={6} lg={4}><Label className="label" for="mutationInjection">Mutation injection:</Label></Col>
                <Col sm={5} md={6} lg={8}>
                    <Toggle checked={props.data.mutationInjection} onChange={props.onParamChanged} className="col-no-margin" id="mutationInjection" disabled={props.disabled}/>
                </Col>
            </FormGroup>
        </fieldset>
    );
}

export default {
    InitialPopulation,
    Selection, 
    Mutation, 
    Crossover, 
    Evaluator, 
    General
}
import React from 'react';
import {Row, Col, Alert} from 'reactstrap';
import axios from 'axios';
import Navigation from '../Navigation/Navigation';
import TrainingParamsSelection from './TrainingParamsSelection';
import GeneticRunner from '../GeneticProgramming/GeneticRunner';
import ExecutionEngine from '../GeneticProgramming/ExecutionEngine/ExecutionEngine';

import constants from '../constants';
import TrainingVisuals from './TrainingVisuals';
import './training.css';
import NodeUtils from '../GeneticProgramming/GeneticAlgorithm/Nodes/NodeUtils';

export default class TrainIndex extends React.Component {

    constructor(props) {
        super(props);

        this.visuals = React.createRef();
        this.params = React.createRef();

        this.state = {
            mapCode: this.props.match.params.mapCode,
            mapParams: null,
            trainingInfo: {
                percentageDone: 0,
                isTraining: false,
                isTrainingDone: false,
                bestAntData: null,
                bestAnt: null,
                unsavedAnt: false
            },
            antSaved: null,
            antLoaded: false,
            ant: {}
        }
    }

    componentDidMount() {
        axios({
            method: "GET",
            url: constants.backend + "api/trails/details/" + this.props.match.params.mapCode
        }).then(res => {
            this.engine = new ExecutionEngine(res.data.antRow,
                                                res.data.antColumn,
                                                res.data.antRotation,
                                                res.data.mapRows,
                                                res.data.mapColumns,
                                                res.data.foodLocations);
            this.setState({
                mapParams: res.data
            }, this.visuals.current.resetAnimation);
        }).catch(err => {

        });
    }

    runAlg = (callback) => {
        if (this.myRunner.hasNextGen()) {
            this.myRunner.getNextGen(callback);
        } else {
            console.log(this.myRunner.getBestAntRepresentation());
            this.setState({
                trainingInfo: {
                    percentageDone: this.state.trainingInfo.percentageDone,
                    isTraining: false,
                    isTrainingDone: true,
                    bestAntData: this.myRunner.getBestAntStates(),
                    bestAnt: this.myRunner.getBestAntRepresentation(),
                    unsavedAnt: true
                }
            }, this.visuals.current.resetAnimation);
        }
    }

    call = (data) => {
        //console.log(JSON.stringify(data));
        this.setState({
            trainingInfo: {
                ...this.state.trainingInfo,
                percentageDone: (data.currentGen / (data.totalGens - 1)) * 100,
                unsavedAnt: true
            }
        }, () => {
            setTimeout(() => this.runAlg(this.call), 50);
        });
    }
    
    trainingStarted = (algParams) => {
        this.myRunner = new GeneticRunner({
            mapParams: this.state.mapParams,
            algorithmParams: algParams
        });

        window.scrollTo(0, this.visuals.current.offsetTop);
        
        this.setState({
            trainingInfo: {
                ...this.state.trainingInfo,
                isTraining: true
            }
        },  () => this.runAlg(this.call));
    }

    resultsDeleted = () => {
        this.setState({
            trainingInfo: {
                percentageDone: 0,
                isTraining: false,
                isTrainingDone: false,
                bestAntData: null,
                bestAnt: null,
                unsavedAnt: false
            },
            ant: {},
            antSaved: null,
            antLoaded: false
        }, this.visuals.current.resetAnimation);
    }

    submitAnt = (algParams, antName) => {
        const name = antName;

        axios({
            method: "POST",
            url: constants.backend + "api/ant/save",
            data: {
                trailID: this.state.trailID,
                algParams: algParams,
                name: antName,
                data: this.myRunner.getBestAntRepresentation()
            }
        }).then(res => {

            this.setState({
                antSaved: true,
                antName: name
            }, () => window.scrollTo(0, 0));


        }).catch(err => {
            this.setState({
                antSaved: false,
            }, () => window.scrollTo(0, 0));
        });
    }

    antSelected = (ant) => {
        axios({
            method: "GET",
            url: constants.backend + "api/ant/params/" + ant.id
        }).then(res => {
            this.setState({
                antLoaded: true,
                antParams: res.data.params,
                ant: ant,
                trainingInfo: {
                    percentageDone: 100,
                    isTraining: false,
                    isTrainingDone: true,
                    bestAntData: this.engine.getAntStates(NodeUtils.ParseShortRepresentation(ant.description)),
                    bestAnt: null,
                    unsavedAnt: false
                }
            }, () => {this.params.current.antParamsLoaded(); this.visuals.current.resetAnimation(); });

            window.scrollTo(0, 0);
        }).catch({

        });
    }

    render() {
        return(
            <div className="container-full">
                <Navigation />
                <Row className="row-vertical-margin">
                    <Col sm="12">
                        <Alert color="success" isOpen={this.state.ant.name !== undefined}>{`Loaded ant '${this.state.ant.name}' created by user: ${this.state.ant.username}`}</Alert>
                        <Alert color="success" isOpen={this.state.antSaved}>{`Your ant '${this.state.antName}' has been successfully saved!`}</Alert>
                        <Alert color="danger" isOpen={this.state.antSaved === false}>There was a problem with saving your ant. Please try again later.</Alert>
                    </Col>
                </Row>
                <Row className="row-vertical-margin"> 
                    <Col xs={12} sm={12} md={12} lg={6}>
                        <TrainingParamsSelection 
                            ref={this.params}
                            trainingStarted={this.trainingStarted}
                            resultsDeleted={this.resultsDeleted}
                            submitAnt={this.submitAnt}
                            antSaved={this.state.antSaved}
                            antLoaded={this.state.antLoaded}
                            savingDisabled={this.state.antSaved || this.state.antLoaded}
                            paramsReceived={this.state.antParams}/>
                    </Col>
                    <Col xs={12} sm={12} md={12} lg={6}>
                        <TrainingVisuals 
                            ref={this.visuals}
                            trailCode={this.props.match.params.mapCode}
                            mapParams={this.state.mapParams}
                            trainingInfo={this.state.trainingInfo}
                            gotTrailID={id => this.setState({trailID: id})}
                            antSelected={this.antSelected}/>
                    </Col>
                </Row>
            </div>
        );
    }
}
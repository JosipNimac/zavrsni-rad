import React from 'react';
import {Modal, ModalHeader, ModalBody, ModalFooter, Button} from 'reactstrap';

export default class CancelResultModal extends React.Component {

    render() {
        return (
            <Modal isOpen={this.props.opened} toggle={this.props.toggle}>
                <ModalHeader toggle={this.props.toggle}><div className="title">Canceling results</div></ModalHeader>
                <ModalBody>
                    <div className="regular-text">
                        <p>
                            Are you sure you want to cancel results of training?
                        </p>
                        <p>
                            You be able to train another ant but you will lose the data about your current ant permanently.
                        </p>
                        <p>
                            Consider saving your current ant before training another.
                        </p>
                    </div>
                </ModalBody>
                <ModalFooter>
                    <Button color="danger" onClick={this.props.newParamsSelection}>Delete</Button>
                    <Button outline onClick={this.props.toggle}>Let me think</Button>
                </ModalFooter>
            </Modal>
        );
    }
}
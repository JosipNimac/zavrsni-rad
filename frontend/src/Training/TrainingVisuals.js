import React from 'react';
import {Row, Col, Progress} from 'reactstrap';
import axios from 'axios';

import constants from '../constants';

import AntAnimation from '../GeneticVisual/TrainedAnimation/AntAnimation';
import AntSelection from './AntSelection';
import './training.css';

export default class TrainingVisuals extends React.Component {
    constructor(props) {
        super(props);

        this.visuals = React.createRef();

        this.state = {
            mapParams: null,
            trailBasicData: {
                name: "",
                user: "",
                created: ""
            },
            recentAnts: [],
            // eslint-disable-next-line eqeqeq
            userLoggedIn: localStorage.getItem("LoggedIn") === "true"
        }
    }

    componentDidMount() {
        axios({
            method: "GET",
            url: constants.backend + "api/trails/info/code/" + this.props.trailCode
        }).then(res => {
            let data = res.data;
            data.created = new Date(data.created).toLocaleString();
            this.setState({
                trailBasicData: {
                    name: data.trailName,
                    user: data.user,
                    created: data.created
                }
            });

            this.props.gotTrailID(data.trailID);
        }).catch(err => {

        });
    }

    resetAnimation = () => {
        this.visuals.current.resetAnimData();
    }

    render() {
        return (
            <div className="container-padded">
                <div className="container-full">
                    <Row className="trail-title-row">
                        <Col xs={12}>
                            <h4 className="subtitle">{this.state.trailBasicData.name}</h4>
                        </Col>
                    </Row>
                    <Row className="d-flex justify-content-between trail-details-row">
                        <Col xs="auto">
                            <div className="image-descriptor">Created by: {this.state.trailBasicData.user}</div>
                        </Col>
                        <Col xs="auto">
                            <div className="image-descriptor">Created at: {this.state.trailBasicData.created}</div>
                        </Col>
                    </Row>
                </div>
                
                <AntAnimation ref={this.visuals} 
                              mapParams={this.props.mapParams} 
                              resolution={1000} 
                              solvedData={this.props.trainingInfo.bestAntData} 
                              trainingFinished={this.props.trainingInfo.isTrainingDone}/>
                
                <div hidden={!(this.props.trainingInfo.isTraining && !this.props.trainingInfo.isTrainingDone)}>
                    <Progress striped value={this.props.trainingInfo.percentageDone}/>
                </div>

                <AntSelection loggedIn={this.state.userLoggedIn}
                              antSelected={this.props.antSelected}
                              unsavedAnt={this.props.trainingInfo.unsavedAnt}
                              selectionDisabeld={this.props.trainingInfo.isTraining}/>
            </div>
        );
    }
}
import React from 'react';
import {Modal, ModalHeader, ModalBody, ModalFooter, Button} from 'reactstrap';

export default function ConfirmSelectionModal(props) {
    return (
        <Modal isOpen={props.isOpen} toggle={props.toggle}>
            <ModalHeader toggle={props.toggle}><div className="title">Ant selection</div></ModalHeader>
            <ModalBody className="regular-text">
                <p>
                    It looks like you have an unsaved ant...
                </p>
                <p>
                    If you select another ant you will lose current ant data.
                </p>
                <p>
                    Are you sure you want to select another ant?
                </p>
            </ModalBody>
            <ModalFooter>
                <Button color="info" onClick={props.confirmSelection}>
                    Yes, I am sure
                </Button>
                <Button outline onClick={props.toggle}>
                    Cancel
                </Button>
            </ModalFooter>
        </Modal>
    );
}

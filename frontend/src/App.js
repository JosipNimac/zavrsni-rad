import React, { Component } from 'react';
import {BrowserRouter, Switch, Route} from 'react-router-dom';

import './App.css';

import Landing from './LandingPage/Landing';
import TrailSelection from './TrailSelection/TrailSelectIndex';
import TrainIndex from './Training/TrainIndex';
import AuthIndex from './Auth/AuthIndex';
import MapCreationIndex from './MapCreation/MapCreationIndex';
import ProfileIndex from './Profile/ProfileIndex';
import axios from 'axios';

axios.defaults.withCredentials = true;

class App extends Component {
  render() {
    return (
      <div className="App">
        <BrowserRouter>
          <Switch>
            <Route exact path="/" component={Landing}/>
            <Route exact path="/home" component={Landing}/>
            <Route exact path="/trails" component={TrailSelection}/>
            <Route exact path="/training/:mapCode" component={TrainIndex}/>
            <Route exact path="/auth" component={AuthIndex}/>
            <Route exact path="/:username/newMap" component={MapCreationIndex}/>
            <Route exact path="/myProfile" component={ProfileIndex} />
          </Switch>
        </BrowserRouter>
      </div>
    );
  }
}

export default App;

//<TrailDesignVisual mapParams={mapParams} resolution={1000}/>
//<TrainingStarter />